package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.eclipse.emf.ecore.EObject;

import printer.SeleniumPrinter;
import IFML.Core.CorePackage;
import IFML.Core.IFMLModel;
import IFML.Core.impl.CorePackageImpl;
import IFML.DataTypes.DataTypesPackage;
import IFML.DataTypes.impl.DataTypesPackageImpl;
import IFML.Extensions.ExtensionsPackage;
import IFML.Extensions.impl.ExtensionsPackageImpl;
import ifmlmutator.Crossover;
import util.FitnessFunc;

public class Assistant {

	protected static String reader;
	protected static String reader2;
	protected static IFMLModel rootWhole = null;
	protected static EObject[] rootParents;
	protected static List<EObject> rootParentsList;
	protected static int areIndividualsloaded;
	protected static EObject IFMLTestCase1;
	protected static EObject IFMLTestCase2;
	private static List<Integer> optimisationArray = new ArrayList<Integer>();

	public static String logName;
	public static int ParentsQty;
	public static int individualSize;
	public static String exit = "N";
	public String cont = "Y";
	public static String option;

	/**
	 * <!-- begin-user-doc -->Starts MutateIFML's test case optimisation.<!-- end-user-doc -->
	 * @param generations the number of iterations
	 * @throws IOException
	 */
	public static void IFMLMutator(int generations) throws IOException {
		
		String welcomeMsg = "\n********************************************\nRandom selection between crossover and mutation will be performed by MutateIFML.\n********************************************";
		String consoleHeader1 = "\n --MUTATION IS IN PROGRESS--\nIterations # ";
		String consoleHeader2 = "\n --CROSSOVER IS IN PROGRESS--\nIterations # ";
		String loadStatus = "\nIndividuals are not loaded yet.\nLoading individuals..\n";
		String insufficientErr = "\nERROR: The mutation requires at least one parent individual to proceed.";
		String noOffspringErr = "\nFAILURE: No offspring produced.";
		
		// Create a log file.
		logName = "log/MUTATEIFML-LOG-" + MutationHelper.timeStamp() + ".txt";
		File log = new File(logName);
		BufferedWriter bw = new BufferedWriter(new FileWriter(log));
		String opening = "MUTATE IFML RUN DATE (-ddMMyy-kkmmssSS) "
				+ MutationHelper.timeStamp()
				+ "\n-----------------------------------------------------\nANCESTRY\tOFFSPRING\n-----------------------------------------------------\n";
		try {
			// Write the first line.
			bw.write(opening);
			bw.newLine();
			bw.close();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}

		// Display the welcome message on the console.
		System.out.println(welcomeMsg);

		// Randomly sets "option" between mutation or crossover.
		optimisationArray.add(1);
		optimisationArray.add(2);
		Random randomisers = new Random();
		int optimisationChoice = optimisationArray.get(randomisers
				.nextInt(optimisationArray.size()));
		option = Integer.toString(optimisationChoice);
		
		switch (option) {
		
		// Option for mutation test optimisation.
		case "1":
			System.out.println(consoleHeader1 + generations);
			// If individuals are not loaded yet, load the individuals.
			if (areIndividualsloaded != 1) {
				System.out.println(loadStatus);
				rootWhole = ModelLoader.getWholemodel();
				rootParentsList = ModelLoader.loadPopulations();
				areIndividualsloaded = ModelLoader.areIndividualsLoaded();
			}
			// Start the mutation process.
			ParentsQty = rootParentsList.size();
			if (ParentsQty > 1) {
				Mutation.goMutation(rootParentsList, rootWhole, ParentsQty);
			}
			else {
				System.out.println(insufficientErr);
				return;
			}
			break;

		// Option for crossover test optimisation.
		case "2":
			System.out.println(consoleHeader2 + generations);
			if (areIndividualsloaded != 1) {
				System.out.println(loadStatus);
				rootWhole = ModelLoader.getWholemodel();
				rootParentsList = ModelLoader.loadPopulations();
				areIndividualsloaded = ModelLoader.areIndividualsLoaded();
			}
			// Start the crossover process.
			ParentsQty = rootParentsList.size();
			if (ParentsQty > 1) {
				int numberofOP = Crossover.individualCrossover(rootParentsList, rootWhole);
				System.out.println("\nSUCCESS: " + numberofOP + " produced.");
			}
			else {
				System.out.println(noOffspringErr);
				return;
			}
			break;
		}
	}

	/**
	 * <!-- begin-user-doc -->Executes the Humbug mutation component and perform
	 * Fault-seeding on the system under test.<!-- end-user-doc -->
	 * TODO: Check and improve this regularly.
	 * @throws IOException
	 */
	public static void callHumbugMutate() throws IOException {
		// For additional argument, use the statement below.
		// String[] command = {"/Users/suhaila/Sites/humbug/bin/humbug", "help"};
		
		// Call Humbug without additional argument.
		String[] command = { "/Users/suhaila/Sites/humbug/bin/humbug", "-m" };
		ProcessBuilder probuilder = new ProcessBuilder(command);
		// Set up your work directory. Humbug will search this directory (sutDir) for the humbug.json and phpunit.xml files.
		String sutDir = getSUTDir();
		probuilder.directory(new File(sutDir));

		File outputFile = new File(
				"/Users/suhaila/Sites/testobib/mutateIFMLOutputLog.txt");
		File errorFile = new File(
				"/Users/suhaila/Sites/testobib/mutateIFMLErrorLog.txt");
		probuilder.redirectOutput(outputFile);
		probuilder.redirectError(errorFile);

		Process process = null;
		try {
			probuilder.inheritIO();
			process = probuilder.start();
			try {
				process.waitFor();
			}
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * <!-- begin-user-doc -->Executes the Humbug test method,
	 * then calculates the fitness and update the current population.<!-- end-user-doc -->
	 * @throws IOException
	 */
	public static boolean callHumbugTest() throws IOException {
		
		double oldgloMS = 0;
		double gloMS = 0;
		boolean reset = false;
		File logDir = new File("log/");
		// Create gloMS log file.
		String glomsFilename = logDir + "gloMS.txt";
		File glomsFile = new File(glomsFilename);
		
		// Retrieve oldgloMS
		String oldGMS = getOldgloms(glomsFile);
		oldgloMS = Integer.parseInt(oldGMS);

		// Retrieve the temporary directory to place the list of mutant SUTs' files.
		String property = "java.io.tmpdir";
		String tempDir = System.getProperty(property);
		String mutantList = "/humbug/mutantlist.txt";
		String mutantNamelist = tempDir + mutantList;
		String rawMS = "humbug/allmutationscore.txt";
		// Converts the list into an array.
		String[] mutantArray = FileArrayProvider.readLines(mutantNamelist);

		// The string to search and replace.
		String searchPattern = "(<td>~suhaila\\/)(\\w+\\/)";
		// Optional search pattern: String searchPattern2 = "(<td>\\/)(\\w+\\/)";
		Charset charset = StandardCharsets.UTF_8;
		String dir = getTestDir();
		File testDir = new File(dir);
		clearDirectory(testDir);

		// TODO: Remove this since the changes are made on the IFML offspring.
		File sourceDir = new File("Selenium/");
		//createAlternates (sourceDir);
		
		// After clearing the testDir directory, copy the offspring test cases from "Selenium" -> test directory.
		String seleniumDir = new java.io.File("Selenium").getAbsolutePath()
				+ "/";
		String theTestDir = testDir.getAbsolutePath();
		copyFiles(seleniumDir, theTestDir);
			

		// Place the test cases as an array of files.
		File[] offspringArray = testDir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File testDir, String fileName) {
				return fileName.endsWith(".html");
			}
		});

		if (offspringArray.length > 0) {
			for (String mutantFile : mutantArray) {
				// To change the value of baseURL, modify 'config.txt' in MutateIFML's "config" directory.
				String baseURL = getBaseURL();
				String replacement = "";
				// For localhost
				replacement = "<td>~suhaila/" + mutantFile + "/";
				// For server, use the following line
				//replacement = "<td>" + baseURL + mutantFile + "/";
				// Iterate the array containing the test cases.
				for (File member : offspringArray) {
					// For the current offspring test case, find 'searchPattern' and replace it 'replacement' to set the correct URL for testing.
					Path path = member.toPath();
					String content = new String(Files.readAllBytes(path),
							charset);
					content = content.replaceAll(searchPattern, replacement);
					Files.write(path, content.getBytes(charset));
					// Remove the mutant filename that is already used.
				}

				// Call Humbug without additional argument.
				String[] command = { "/Users/suhaila/Sites/humbug/bin/humbug",
						"-r" };
				ProcessBuilder probuilder = new ProcessBuilder(command);
				// Set up your work directory. Humbug will search this directory for the humbug.json and phpunit.xml files.
				
				probuilder.directory(new File(getSUTDir()));
				// Humbug reads the phpunit.xml file from the directory above.

				File outputFile = new File(
						"/Users/suhaila/Sites/testobib/testIFMLOutputLog.txt");
				File errorFile = new File(
						"/Users/suhaila/Sites/testobib/testIFMLErrorLog.txt");
				probuilder.redirectOutput(outputFile);
				probuilder.redirectError(errorFile);

				// Humbug is executed here.
				Process process2 = null;
				try {
					// Inherit IO.
					probuilder.inheritIO();
					process2 = probuilder.start();
					try {
						process2.waitFor();
					}
					catch (InterruptedException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
				catch (IOException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}
			}

			// Collect the mutation scores from Humbug (# mutants killed by a test case/total # mutants *100).
			collectResults(logDir, tempDir, mutantList, rawMS);
			File rawMSFile = new File(tempDir + rawMS);
			cleanUpFile(rawMSFile, logDir);
			
			int fitness[] = new int[2];
			List<String> fitOP = new ArrayList<String>();			
			if (gloMS != 0) {
				oldgloMS = gloMS;
				try {
					BufferedWriter gloMSWriter = new BufferedWriter(new FileWriter(glomsFile));
					gloMSWriter.write(Double.toString(oldgloMS));
					gloMSWriter.newLine();
					gloMSWriter.close();
				}
				catch (IOException e) {
					System.out.println("\nWarning! An exception has occured.\n" + e);
				}
				
				gloMS = evaluateFitness(offspringArray);
				
				
				
				// If gloMS > oldgloMS, update current population
				if (gloMS > oldgloMS) {
					// move offspring -> population
					reset = updateCurrentP(logDir);
				}
			}

			//moveIntoElite(fitnessFilename, glomsFile, eliteDir, directoryListing)
			// Move the tested test cases to "tested".
			File testedDir = new File("tested/");
			moveAfterTest(sourceDir, testedDir, null);
		}
		return reset;
	}

	/**
	 * <!-- begin-user-doc -->Creates alternatives for each test case.<!-- end-user-doc -->
	 * 
	 */
	private static void createAlternates(File target) {
		File[] opArray = target.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File testDir, String fileName) {
				return fileName.endsWith(".html");
			}
		});
		
	}

	/**
	 * <!-- begin-user-doc -->Updates the current population.<!-- end-user-doc -->
	 * @throws IOException
	 */
	public static boolean updateCurrentP(File popDir) throws IOException {
		List<String> tempList = new ArrayList<String>();
		String msDir = "log/MS.txt";
		String[] msLines = FileArrayProvider.readLines(msDir);
		int done = 0;
		//for each line, if MS > 0, retrieve the offspring name into an array an return it
		for (String op : msLines) {
			String s2 = "";
			if (op.contains("MS:0") && !op.contains("Glo")) {
				String[] s1 = op.split(":");
				int index = s1.length-3;
				s2 = s1[index].replace(".html", "");
			}
			if (s2 != "" && done < 1) {
				tempList.add(s2);
			}
		}	
		// select fit individuals only
		File[] p = new File("population/").listFiles();
		File e = new File("weak/");
		for (int i=0; i<tempList.size(); i++){
			for (File c : p ) {
				String name = c.getName();
				String search = tempList.get(i);
				if (name.contains(search)) {
					moveAfterTest(e, popDir, name);
				}
			}
			done = 1;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->Retrieve the previous global mutation score.<!-- end-user-doc -->
	 * @param file the source file
	 */
	public static String getOldgloms( File file ) {
	    RandomAccessFile fileHandler = null;
	    try {
	        fileHandler = new RandomAccessFile( file, "r" );
	        long fileLength = fileHandler.length() - 1;
	        StringBuilder sb = new StringBuilder();

	        for(long filePointer = fileLength; filePointer != -1; filePointer--){
	            fileHandler.seek( filePointer );
	            int readByte = fileHandler.readByte();

	            if( readByte == 0xA ) {
	                if( filePointer == fileLength ) {
	                    continue;
	                }
	                break;

	            } else if( readByte == 0xD ) {
	                if( filePointer == fileLength - 1 ) {
	                    continue;
	                }
	                break;
	            }

	            sb.append( ( char ) readByte );
	        }

	        String lastLine = sb.reverse().toString();
	        return lastLine;
	    } catch( java.io.FileNotFoundException e ) {
	        e.printStackTrace();
	        return null;
	    } catch( java.io.IOException e ) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        if (fileHandler != null )
	            try {
	                fileHandler.close();
	            } catch (IOException e) {
	                /* ignore */
	            }
	    }
	}
	
	/**
	 * <!-- begin-user-doc -->Clear the testDir directory from existing files.<!-- end-user-doc -->
	 * @param tgtDir The source directory
	 * @throws IOException
	 */
	public static void clearDirectory(File tgtDir) {
		if (tgtDir.list().length > 0) {
			for (File file : tgtDir.listFiles()) {
				file.delete();
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->Copy results from Humbug into MutateIFML.<!-- end-user-doc -->
	 * @param theSourceDir The source directory
	 * @param theTargetDir The target directory
	 * @throws IOException
	 */
	public static void copyFiles(String theSourceDir, String theTargetDir) throws IOException {
		
		String copyCmd = "cp -R " + theSourceDir + " " + theTargetDir;
		Runtime run1 = Runtime.getRuntime();
		Process process1 = run1.exec(copyCmd);
		try {
			process1.waitFor();
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader buf = new BufferedReader(new InputStreamReader(
				process1.getInputStream()));
		String line = "";
		while ((line = buf.readLine()) != null) {
			System.out.println(line);
		}
	}
	
	/**
	 * <!-- begin-user-doc -->Copy results from Humbug into MutateIFML.<!-- end-user-doc -->
	 * @param logDir MutateIFML's log directory
	 * @param humbugTempDir Humbug's temporary directory
	 * @param mutantFile The file that contains Humbug's list of mutant SUTs
	 * @param rawScore The file that contains Humbug's test result
	 * @throws IOException
	 */
	private static void collectResults(File logDir, String humbugTempDir, String mutantFile, String rawScore)
			throws IOException {
		
		String rawFTFilename = humbugTempDir + "/" + rawScore;
		File rawFTFile = new File(rawFTFilename);
		String mutantSUTsFilename = humbugTempDir + "/" + mutantFile;
		File mutantSUTsFile = new File(mutantSUTsFilename);
		
		// Copy the file from Humbug's temporary directory into MutateIFML
		// TODO: This part requires debug if possible.
		String[] copyCmdArray = new String[2];
		copyCmdArray[0] = "cp -R " + rawFTFile + " " + logDir;
		copyCmdArray[1] = "cp -R " + mutantSUTsFile + " " + logDir;
		
		Runtime run3 = Runtime.getRuntime();
		Process process3 = run3.exec(copyCmdArray, null);
		try {
			process3.waitFor();
		}
		catch (InterruptedException e8) {
			// TODO Auto-generated catch block
			e8.printStackTrace();
		}
		BufferedReader buf2 = new BufferedReader(new InputStreamReader(
				process3.getInputStream()));
		String line2 = "";
		while ((line2 = buf2.readLine()) != null) {
			System.out.println(line2);
		}
	}
	
	/**
	 * <!-- begin-user-doc -->Evaluates the fitness of the offspring.<!-- end-user-doc -->
	 * @param offsprings the offspring
	 * @throws IOException
	 */
	public static double evaluateFitness(File[] offsprings) throws IOException {
		
		// For every offspring, accumulate its mutation score
		String[] mutantscoreArray = FileArrayProvider.readLines("log/allmutationscore.txt");
		// Create a file to store calculation of MS
		String msFilename = "log/MS.txt";
		File logMS = new File(msFilename);
		BufferedWriter bwriter = new BufferedWriter(new FileWriter(logMS));
		String opName = "";
		int x = 0;
		double msts = 0;
		boolean cv = false;
		
		// Retrieve the offspring's name.
		for (File offspring : offsprings) {
			// For the current test case.
			//opNameArray[x] = offspring.getName();
			opName = offspring.getName();
			x = x+1;
			// For each mutant with its respective score on all offspring.
			String[] scoreArray = new String[mutantscoreArray.length];
			int killCount = 0;
			int diedTS = 0;
			int done = 0;
			double totalM = mutantscoreArray.length;
			double mstc = 0;
			cv = FitnessFunc.compareCFG(offspring);
			
			for (String mutantScore : mutantscoreArray) {
				StringTokenizer mt = new StringTokenizer(mutantScore, "|");
				while (mt.hasMoreElements()) {
					mstc = 0;
					String token = mt.nextToken();
					// Find the specified offspring
					if (token.contains(opName)) {
						//System.out.println("\nFOUND OFFSPRING " + opName + " IN " + token);
						scoreArray = token.split("=");
						// Accumulate the offspring MS
						try {
							// Write the offspring MS.
							bwriter.write(scoreArray[1] + ":");
							if (scoreArray[1] != null && scoreArray[1].equals("1")) {
								killCount=killCount+1;
							}
						}
						catch (IOException ioe) {
							ioe.printStackTrace();
						}
					}
				}
				if (mutantScore.contains("=1")) {
					diedTS = diedTS+1;
				}
				Double kC = new Double (killCount);
				mstc = calculateMS(kC, totalM);
			}
			//System.out.println("\n" + killCount + " mutant(s) died.");
			//System.out.println("\nMutation score: " + s);
			if (done == 0) {
				msts = calculateGLOMS(diedTS, totalM);
				done = 1;
			}
			bwriter.write(opName + ":MS:" + Double.toString(mstc));
			if (cv == true) {
				bwriter.write("CV:0");
			}
			else {
				bwriter.write("CV:1");
			}
			bwriter.newLine();
			cv = false;
		}
		System.out.println("\nThe global mutation score is " + msts);
		
		// calculate gloMS
		bwriter.write("GloMS:" + msts);
		bwriter.close();
		System.out.println("\nUpdate current population is complete.\n");
		return msts;
	}
	
	/**
	 * <!-- begin-user-doc -->Calculates the global mutations score of a population.<!-- end-user-doc -->
	 * @param killed the number of mutant SUTs that are killed
	 * @param total the total number of mutant SUTs
	 * @return the global mutaion score
	 */
	private static double calculateGLOMS(double killed, double total) {
		if (killed > 0){
			double msTS = killed*100 / total;
			return msTS;
		}
		else return 0;
	}

	/**
	 * <!-- begin-user-doc -->Calculates the mutations score of an offspring.<!-- end-user-doc -->
	 * @param killC the number of mutant SUTs that are killed
	 * @param total the total number of mutant SUTs
	 * @return the mutaion score of an offspring
	 */
	private static double calculateMS(double killC, double total) {
		if (killC > 0){
			double msTC = killC*100 / total;
			return (int)Math.round(msTC);
		}
		else return 0;
	}
	
	
	/**
	 * <!-- begin-user-doc -->Moves individuals with non-zero fitness into
	 * 'ELITE' directory after the testing is complete.<!-- end-user-doc -->
	 * @param msFilePath the log where individual fitness function is recorded
	 * @param glomsFile the log where global fitness is recorded
	 * @param eliteDir the directory where fitter individual is saved
	 * 
	 * @throws IOException
	 */
	private static void moveToElite(String msFilePath, File glomsFile, File eliteDir, File[] offsprings) throws IOException {
		// converts the list into an array.
		List<String> tempList = new ArrayList<String>();
		String[] msLines = FileArrayProvider.readLines(msFilePath);
		List<String> tempCalculator = new ArrayList<String>();
		List<String> eliteList = new ArrayList<String>();
		double opgloMs = 0;
		String row = "";
		PrintWriter out = new PrintWriter(glomsFile);

		// Retrieve the offspring's name.
		for (File offspring : offsprings) {
			// For the current test case, get its name and find it in the mutation score log.
			String opName = offspring.getName();
			for (String msmSUT : msLines) {
				StringTokenizer st = new StringTokenizer(msmSUT, "|");
				while (st.hasMoreElements()) {
					String token = st.nextToken();
					if (token.contains(opName)) {
						System.out.println(token);
						String[] score = token.split("=");
						tempCalculator.add(score[1]);
					}

				}
			}
			tempCalculator.add("|");
		}

		int opIdx = 0;
		// Calculate the gloMS and coverage.
		for (int i = 0; i < tempCalculator.size(); i++) {
			if (!tempCalculator.get(i).equals("|")) {
				double temp = Double.parseDouble(tempCalculator.get(i));
				opgloMs = opgloMs + temp;
			}
			else {
				String opName = offsprings[opIdx].getName();
				row = "Global mutation score for " + opName + ":" + opgloMs;
				// Saves the ELITE.
				if (opgloMs > 0) {
					eliteList.add(opName.replaceAll(".html", ""));
					//eliteList.add(String.valueOf(opgloMs));
				}
				out.println(row);
				System.out.println(row);
				opIdx = opIdx + 1;
				opgloMs = 0;
			}
		}
		// Close gloMS.
		out.println("\nEnd of global mutation score.\nNon-zero offspring will be chosen into elite.");
		out.close();

		// Move the offsprings into ELITE.
		File[] population = new File("population/").listFiles();
		for (int j = 0; j < population.length; j++) {
			String candidateName = population[j].getName();
			if (eliteList != null) {
				for (String e : eliteList) {
					if (candidateName.contains(e)) {
						tempList.add(population[j].getCanonicalPath());
					}
				}
			}
		}

		for (String path : tempList) {
			String moveEliteCmd = "mv " + path + " " + eliteDir;
			Runtime run = Runtime.getRuntime();
			Process moveEliteP = run.exec(moveEliteCmd);
			try {
				moveEliteP.waitFor();
			}
			catch (InterruptedException e7) {
				// TODO Auto-generated catch block
				e7.printStackTrace();
			}
			BufferedReader buf = new BufferedReader(new InputStreamReader(
					moveEliteP.getInputStream()));
			String line = "";
			while ((line = buf.readLine()) != null) {
				System.out.println(line);
			}
		}

		System.out.println("\nSelected offsprings were moved into elite:\n");
	}
	
	/**
	 * <!-- begin-user-doc -->Moves the test cases into the
	 *  target directory after the testing is complete.<!-- end-user-doc -->
	 * @param sourceDir the directory that contains untested Selenium test cases
	 * @param testedDir the directory where tested Selenium test cases will be moved
	 * @throws IOException
	 */
	private static void moveAfterTest(File tgtDir, File srcDir, String name)
			throws IOException {
		
		//sourceDir.getCanonicalPath();
		String srcPath = srcDir.getCanonicalPath();
		String tgtPath = tgtDir.getCanonicalPath();
		
		if (name != null) {
			String path = srcDir + "/" + name;
			String moveCmd = "mv " + path + " " + tgtPath;
			Runtime run = Runtime.getRuntime();
			Process moveP = run.exec(moveCmd);
			try {
				moveP.waitFor();
			}
			catch (InterruptedException e6) {
				// TODO Auto-generated catch block
				e6.printStackTrace();
			}
			BufferedReader buf = new BufferedReader(new InputStreamReader(
					moveP.getInputStream()));
			String line = "";
			while ((line = buf.readLine()) != null) {
				System.out.println(line);
			}

		}
		else {
			for (File mFile : srcDir.listFiles()) {
				String mPath = mFile.getCanonicalPath();
				String moveCmd = "mv " + mPath + " " + tgtDir;
				Runtime run = Runtime.getRuntime();
				Process moveP = run.exec(moveCmd);
				try {
					moveP.waitFor();
				}
				catch (InterruptedException e6) {
					// TODO Auto-generated catch block
					e6.printStackTrace();
				}
				BufferedReader buf = new BufferedReader(new InputStreamReader(
						moveP.getInputStream()));
				String line = "";
				while ((line = buf.readLine()) != null) {
					System.out.println(line);
				}

			}
		}
	
	}

	/**
	 * <!-- begin-user-doc -->Display the contents of a file to the screen console.<!-- end-user-doc -->
	 */
	private static void printFile(File file) throws IOException {
		System.out.println("*********************************");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		br.close();
		fr.close();
		System.out.println("*********************************");
	}

	/**
	 * <!-- begin-user-doc -->Initialise the IFML model.<!-- end-user-doc -->
	 */
	public static void initialiseModel() {

		System.out.println("\nInitialising..");
		// Initialisation methods recommended by IFML Editor.
		CorePackageImpl.init();
		ExtensionsPackageImpl.init();
		DataTypesPackageImpl.init();

		// Initialise and register the IFML model classes.
		CorePackage.eINSTANCE.eClass();
		System.out.println("Core Package Loaded..");
		ExtensionsPackage.eINSTANCE.eClass();
		System.out.println("Extensions Package Loaded..");
		DataTypesPackage.eINSTANCE.eClass();
		System.out.println("DataTypes Package Loaded..");
		System.out.println("Core Factory Loaded..");
		System.out.println("Extensions Factory Loaded..");
		System.out.println("DataTypes Factory Loaded..");
	}

	/**
	 * <!-- begin-user-doc -->Returns the filename of the current log file.<!-- end-user-doc -->
	 * @return the filename of the current log file
	 */
	public static String getLogFile() {
		return logName;
	}
	
	/**
	 * <!-- begin-user-doc -->Returns the path of the SUT.<!-- end-user-doc -->
	 * @return the path of the SUT
	 */
	public static String getSUTDir() {
		String dir = "";
		String content = "";
		try {
			content = new String(Files.readAllBytes(Paths.get("configuration/config.txt")));
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (!content.equals("")) {
			String textStr[] = content.split("\\r\\n|\\n|\\r");
			List<String> config = Arrays.asList(textStr);
			int i = 0;
			for (String s : config) {
				if (s.contains("SUT:")) {
					i = config.indexOf(s);
				}
			}	
			dir = config.get(i);
			dir = dir.replace("SUT:", "");
		}
		return dir;
	}

	/**
	 * <!-- begin-user-doc -->Returns the path of the test directory.<!-- end-user-doc -->
	 * @return the path of the test directory
	 */
	public static String getTestDir() {
		String dir = "";
		String content = "";
		try {
			content = new String(Files.readAllBytes(Paths.get("configuration/config.txt")));
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (!content.equals("")) {
			String textStr[] = content.split("\\r\\n|\\n|\\r");
			List<String> config = Arrays.asList(textStr);
			int i = 0;
			for (String s : config) {
				if (s.contains("Test Cases:")) {
					i = config.indexOf(s);
				}
			}	
			dir = config.get(i);
			dir = dir.replace("Test Cases:", "");
		}
		return dir;
	}
	
	/**
	 * <!-- begin-user-doc -->Returns the base URLin MutateIFML's config.txt.<!-- end-user-doc -->
	 * @return the base URL
	 */
	private static String getBaseURL() {
		String dir = "";
		String content = "";
		try {
			content = new String(Files.readAllBytes(Paths.get("configuration/config.txt")));
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (!content.equals("")) {
			String textStr[] = content.split("\\r\\n|\\n|\\r");
			List<String> config = Arrays.asList(textStr);
			int i = 0;
			for (String s : config) {
				if (s.contains("Base URL:")) {
					i = config.indexOf(s);
				}
			}	
			dir = config.get(i);
			dir = dir.replace("Base URL:", "");
		}
		return dir;
	}

	
	/**
	 * <!-- begin-user-doc -->Creates a backup of the database's contents prior to testing.<!-- end-user-doc -->
	 * @return TRUE if backup is a success
	 */
	public static boolean backupDB(String dbName, String dbUserName,
			String dbPassword, String path) {

		String executeCmd = "/usr/local/bin/mysqldump -u " + dbUserName + " -p"
				+ dbPassword + " --add-drop-database -B " + dbName + " -r "
				+ path;
		Process runtimeProcess;
		try {

			runtimeProcess = Runtime.getRuntime().exec(executeCmd);
			int processComplete = runtimeProcess.waitFor();

			if (processComplete == 0) {
				System.out.println("Backup created successfully");
				return true;
			}
			else {
				System.out.println("Could not create the backup");
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		return false;
	}

	/**
	 * <!-- begin-user-doc -->Restore the database's contents after testing is complete.<!-- end-user-doc -->
	 * @return TRUE if restore is a success
	 */
	public static boolean restoreDB(String dbUserName, String dbPassword,
			String source) {

		String[] executeCmd = new String[] { "/usr/local/bin/mysql",
				"--user=" + dbUserName, "--password=" + dbPassword, "-e",
				"source " + source };

		Process runtimeProcess;
		try {

			runtimeProcess = Runtime.getRuntime().exec(executeCmd);
			int processComplete = runtimeProcess.waitFor();

			if (processComplete == 0) {
				System.out.println("Backup restored successfully");
				return true;
			}
			else {
				System.out.println("Could not restore the backup");
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		return false;
	}

	/**
	 * <!-- begin-user-doc -->Remove lines that has empty space in a file.<!-- end-user-doc -->
	 * @param inputFile The file that requires clean-up
	 * @param logDir MutateIFML's log directory
	 * @throws IOException
	 */
	public static void cleanUpFile(File inputFile, File logDir) throws IOException {
	
		File tempFile = new File(logDir + "/tempFile.txt");

		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

		String lineToRemove = "";
		String currentLine;

		while((currentLine = reader.readLine()) != null) {
		    // trim newline when comparing with lineToRemove
		    String trimmedLine = currentLine.trim();
		    if(trimmedLine.equals(lineToRemove)) continue;
		    writer.write(currentLine + System.getProperty("line.separator"));
		}
		writer.close(); 
		reader.close(); 
		boolean successful = tempFile.renameTo(inputFile);
	}
	
	/**
	 * <!-- begin-user-doc -->A hidden method that can be used to test some codes 
	 * by modifying the contents of this method.
	 * Was used to test SeleniumPrinter's assertions.<!-- end-user-doc -->
	 * 
	 * @throws IOException
	 */
	public static void testThis() throws IOException {
		List<EObject> temp = ModelLoader.loadPopulations();
		EObject[] seleniumRoot = new EObject[1];
		seleniumRoot[0] = temp.get(0);
		SeleniumPrinter.printOne(seleniumRoot, 0, "test");

//        String property = "java.io.tmpdir";
//
//        String tempDir = System.getProperty(property);
//        System.out.println("OS current temporary directory is " + tempDir);

//        try {
//        	size = a.eContents().size();
//        } catch (NullPointerException npe) {
//        	System.out.println("Exception in a: " + npe);
//        }
//
//        if (a != null) {
//        	System.out.println("Null value not found ");
//        }
//        else System.out.println("Null value found");

		System.out.println("----END----");
	}
}