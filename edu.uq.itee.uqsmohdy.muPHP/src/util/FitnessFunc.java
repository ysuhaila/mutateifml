package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FitnessFunc {
	
	/**
	 * <!-- begin-user-doc -->Compare.<!-- end-user-doc -->
	 * @return the filename of the current log file
	 * @throws IOException 
	 */
	public static boolean compareCFG(File op) throws IOException {
		String s;
		String[] words = null;
		String [] list = Shared.altAnnotations();
		FileReader fr = new FileReader(op);
		BufferedReader br = new BufferedReader(fr);
		for (String text : list) {
			while((s=br.readLine())!=null) {
				words=s.split(" ");
				for (String word : words) {
					if (word.equals(text)) {
	                	 File newFile = new File(op.getName()+"XOX");
	                	 return true;
	                 }  
		        }
			}
		}
		return false;
	}
}