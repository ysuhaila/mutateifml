package util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import printer.SeleniumPrinter;
import IFML.Core.IFMLModel;
import IFML.Extensions.IFMLWindow;
import ifmlmutator.DeleteMutation;
import ifmlmutator.InsertMutation;
import ifmlmutator.SwapMutation;
import util.Assistant;

public class Mutation {

	private static LinkedHashMap<String, String> offspring1Map;
	private static LinkedHashMap<String, String> offspring2Map;
	public static int mutationRate = 3;
	private static List<HashMap<String, String>> finalOffspring3Map;
	private static List<HashMap<String, String>> finalOffspring4Map;
	private static EList<EObject> offspring1Object;
	private static EList<EObject> offspring2Object;
	private static EList<EObject> offspring1Clone;
	private static EList<EObject> offspring2Clone;
	private static List<Integer> mutationArray = new ArrayList<Integer>();
	private static EObject offspring1;
	private static EObject offspring2;

	/**
	 * Begins the mutation process <!-- begin-user-doc -->
	 * @param parents the population of parent individuals
	 * @param general the root object of the general model
	 * @param populationSize the size of the population
	 * @throws IOException
	 */
	public static void goMutation(List<EObject> parents, EObject general,
			int populationSize) throws IOException {
		HashMap<Integer, EObject> hmap = new HashMap<Integer, EObject>();
		EList<EObject> finalOffspring;
		EList<EObject> OPArray = new BasicEList<EObject>();
		Object hmapKey;
		Random randomisers = new Random();

		// create a list of filenames with the size equal to the number of parent candidates available.
		// String[] nameList = new String[populationSize];
		// randomly selects ONE index of the candidate.
		int cloneIndex = Shared.randomiseIndex(populationSize);
		// Random selection of ONE candidate individual.
		EList<EObject> candidateClone = getRandomCandidate(parents, cloneIndex);
		EObject candidate = getRandomCandidate2(parents, cloneIndex);
		// Randomly select the mutation index based on the number of Windows.
		int candidateSize = Shared.numberofWindows(candidateClone);

		// Randomly select the mutation operators (either 1,2, or 3).
		mutationArray = Arrays.asList(1, 2, 3);
		int mutationChoice = mutationArray.get(randomisers.nextInt(mutationArray.size()));
		// the switch case relies on this String.
		String mutationOperator = Integer.toString(mutationChoice);
		// Change the value below for a fixed choice of mutation operator.
		// mutationOperator = "3";

		switch (mutationOperator) {
		case "1":
			System.out.println("\nINSERT MUTATION SELECTED.\n");
			// TODO: If no suitable insert candidate found, repeat the process on a different position
			hmap = InsertMutation.insertMutation(candidate, candidateClone, general, candidateSize);
			hmapKey = hmap.keySet().toArray()[0];
			//System.out.println("FINAL OFFSPRING\n");
			finalOffspring = Shared.copyAllGenes(hmap.get(hmapKey));
			if (hmapKey.toString().equals("1")) {
				//Shared.displayContents1(finalOffspring);
				Resource sR = MutationHelper.saveModel(finalOffspring, "IMOP",
						0);
				String fileName = MutationHelper.getfileName();
				if (fileName.length() != 0) {
					fileName = fileName.replaceAll("(.core)", "");
					EObject[] selRoot = new EObject[1];
					selRoot[0] = sR.getContents().get(0);
					OPArray.add(selRoot[0]);
					OPArray.addAll(Shared.createAlternatives(selRoot));
					SeleniumPrinter.printAll(OPArray);
					//SeleniumPrinter.printOne(selRoot, 0, fileName);
					// write to log
					MutationHelper.writeLog(fileName, finalOffspring);
				}
			}
			else {
				System.out
						.println("Sorry. This run fails to produce valid offspring.\n");
			}
			break;

		case "2":
			System.out.println("\nDELETE MUTATION SELECTED.\n");
			
			offspring1 = DeleteMutation.deleteMutation(candidate, candidateClone);
			// XXXNullPointer Exception
			if (offspring1 != null) {
				offspring1Clone = Shared.copyAllGenes(offspring1);
				offspring2 = DeleteMutation.deleteMutation(offspring1, offspring1Clone);
			}
			//System.out.println("FINAL OFFSPRING AFTER DELETION\n");
			//Shared.displayContents1(offspring1Clone);
			System.out.println("=======================================");

			if (offspring1 != null) {
				offspring1Clone = Shared.copyAllGenes(offspring1);
				//Resource sR3 = MutationHelper.saveModel(offspring1Clone, nameList[0], 1);
				Resource sR3 = MutationHelper.saveModel(offspring1Clone,
						"DMOP", 1);
				// transform the offspring into Selenium test case.
				String fileName = MutationHelper.getfileName();
				if (fileName.length() != 0) {
					fileName = fileName.replaceAll("(.core)", "");
					EObject[] selRoot3 = new EObject[1];
					Shared.createAlternatives(selRoot3);
					selRoot3[0] = sR3.getContents().get(0);
					OPArray.add(selRoot3[0]);
					OPArray.addAll(Shared.createAlternatives(selRoot3));
					SeleniumPrinter.printAll(OPArray);
					//SeleniumPrinter.printOne(selRoot3, 0, fileName);
					// write to log
					MutationHelper.writeLog(fileName, offspring1Clone);
				}

			}
			else {
				System.out
						.println("Sorry. This run fails to produce valid offspring.\n");
			}
			if (offspring2 != null) {
				offspring2Clone = Shared.copyAllGenes(offspring2);
				//Shared.displayContents1(offspring2Clone);
				//Resource sR4 = MutationHelper.saveModel(offspring2Clone, nameList[1], 2);
				Resource sR4 = MutationHelper.saveModel(offspring2Clone,
						"DMOP", 2);
				// transform the offspring into Selenium test case.
				String fileName = MutationHelper.getfileName();
				if (fileName.length() != 0) {
					fileName = fileName.replaceAll("(.core)", "");
					EObject[] selRoot4 = new EObject[1];
					selRoot4[0] = sR4.getContents().get(0);
					SeleniumPrinter.printOne(selRoot4, 0, fileName);
				}
			}
			else {
				System.out
						.println("Sorry. This run fails to produce valid offspring.\n");
			}

			break;

		case "3":
			System.out.println("\nSWAP MUTATION SELECTED.\n");
			hmap = SwapMutation.swapMutation(candidate, candidateClone, general, candidateSize);
			hmapKey = hmap.keySet().toArray()[0];
			//System.out.println("FINAL OFFSPRING\n");
			finalOffspring = Shared.copyAllGenes(hmap.get(hmapKey));
			if (hmapKey.toString().equals("1")) {
				//Shared.displayContents1(finalOffspring);
				Resource sR = MutationHelper.saveModel(finalOffspring, "SWP",
						0);
				String fileName = MutationHelper.getfileName();
				if (fileName.length() != 0) {
					fileName = fileName.replaceAll("(.core)", "");
					EObject[] selRoot = new EObject[1];
					Shared.createAlternatives(selRoot);
					selRoot[0] = sR.getContents().get(0);
					SeleniumPrinter.printOne(selRoot, 0, fileName);
					// write to log
					MutationHelper.writeLog(fileName, finalOffspring);
				}
			}
			else {
				System.out
						.println("Sorry. This run fails to produce valid offspring.\n");
			}
			break;

		case "4":
			System.out.println("Testing the contents.\n");

			System.out.println("CLONED INDIVIDUALS 1\n");
//			Shared.displayContents1(clonedParent1);
			System.out.println("\nCLONED INDIVIDUALS 2\n");
//			Shared.displayContents1(clonedParent2);
//			MutationHelper.saveModel(clonedParent1, nameList[0], 1);
//			MutationHelper.saveModel(clonedParent2, nameList[1], 2);
			break;
		default:
		}

		// TODO: Use only one Selenium Printer, and put it out of the switch loop.
	}


	public static void testThis() {
		System.out.println("\nTEST THIS.\n");
		System.out.println("::::::::::::::::::::");

		final IFMLModel genRoot = ModelLoader.getRootWhole();
		final TreeIterator<EObject> objIt = genRoot.eAllContents();
		EObject finder = objIt.next();
		while (objIt.hasNext()) {
			if (finder instanceof IFMLWindow) {
				Shared.numberofInFlows(finder);
			}
			finder = objIt.next();
		}
	}

	public static void swapMutant(final EObject[] rootIndividuals,
			final EObject rootGeneral) {
		//LinkedHashMap<String,EList<EObject>> offspringList = null;

		// TODO: Make the clone method dynamic
		final EList<EObject> model1 = Shared.copyAllGenes(rootIndividuals[0]);
		Shared.copyAllGenes(rootIndividuals[1]);

		// Get all elements of the general model
		final EList<EObject> GeneralModel = Shared.copyAllGenes(rootGeneral);

		final int model1Size = model1.size();
		final int GMSize = GeneralModel.size();
		int mCtr = 0;
		int GMCtr = 0;
		final int maxwindowCtr = Shared.numberofWindows(model1);
		int windowCtr = 0;
		int swapIdx = 0;

		EObject swapObj = null;
		//Shared.displayContents1(model1);

		// iterate these individuals and the general model at the same time. Use EqualityHelper
		for (EObject obj1 : model1) {
			final EObject temp = obj1;
			// if it is not IFML Window, move the position
			// if ((obj1 instanceof InteractionFlowModel || obj1 instanceof IFMLModel ||obj1 instanceof Annotation) && mCtr < model1Size) {
			if ((obj1 instanceof IFMLWindow == false) && mCtr < model1Size - 1) {
				final int tempInd = model1.indexOf(temp);
				obj1 = model1.get(tempInd + 1);
				mCtr = mCtr + 1;
			}
			// if IFML Window, compare with the window in General Model
			else if (obj1 instanceof IFMLWindow && windowCtr < maxwindowCtr - 1) {
				final String mWindowName = ((IFMLWindow) obj1).getName();
				System.out.println(mWindowName);
				for (EObject obj2 : GeneralModel) {
					final EObject temp2 = obj2;
					//if ((obj2 instanceof InteractionFlowModel || obj2 instanceof IFMLModel || obj2 instanceof Annotation) && GMCtr < GMSize) {
					if ((obj2 instanceof IFMLWindow == false)
							&& GMCtr < GMSize - 1) {
						final int tempInd2 = GeneralModel.indexOf(temp2);
						obj2 = GeneralModel.get(tempInd2 + 1);
						GMCtr = GMCtr + 1;
					}
					// if IFML Window in General Model, start comparing
					else if (obj2 instanceof IFMLWindow) {
						final String gmWindowName = ((IFMLWindow) obj2)
								.getName();
						if (mWindowName.contains(gmWindowName)) {
							System.out.println("EQUAL");
							System.out.println(gmWindowName);
							GMCtr = GMCtr + 1;
							break;
						}

					}
				} // end for
				mCtr = mCtr + 1;
				windowCtr = windowCtr + 1;
			} // end else if

			// swap in here
			else if (obj1 instanceof IFMLWindow
					&& windowCtr == maxwindowCtr - 1) {
				final String mWindowName = ((IFMLWindow) obj1).getName();
				System.out.println(mWindowName);
				for (EObject obj2 : GeneralModel) {
					final EObject temp2 = obj2;
					if ((obj2 instanceof IFMLWindow == false)
							&& GMCtr < GMSize - 1) {
						final int tempInd2 = GeneralModel.indexOf(temp2);
						obj2 = GeneralModel.get(tempInd2 + 1);
						GMCtr = GMCtr + 1;
					}
					// if IFML Window in General Model, find alternative
					// TODO: might implement dynamic swapping, right now swap only last window element
					// for dynamic swapping, might get several alternative from LinkedHashMap
					else if (obj2 instanceof IFMLWindow) {
						final String gmWindowName = ((IFMLWindow) obj2)
								.getName();
						if (mWindowName.contains(gmWindowName) == false) {
							System.out.println("NOT EQUAL");
							System.out.println(gmWindowName);
							GMCtr = GMCtr + 1;

							// get swap index

							swapIdx = model1.indexOf(obj1);
							swapObj = obj2;
							break;
						}

					}
				} // end for
				mCtr = mCtr + 1;
			}
		}

		// swap
		model1.set(swapIdx, swapObj);
		//Shared.displayContents1(model1);

		//return offspringList;  ***CONTENTS***
	}

	/**
	 * Copy the parent individual from a list of objects into a LinkedHashMap. <!-- begin-user-doc -->
	 * Returns a LinkedHashMap that contains IFMLWindow-NavigationFlow pair of the parent individual.
	 * TODO: Remove this method as it is no longer needed
	 * @throws IOException
	 */
	public static LinkedHashMap<String, String> createIndividualMap(
			final List<EObject> individual, int listIndex) {
		// get the parent individual. XXX IndexOutOfBoundsException bug
		final EList<EObject> parentList = Shared.copyAllGenes(individual
				.get(listIndex));
		// get the number of windows inside the parent individual.
		final int finalsize = Shared.numberofWindows(parentList);
		final LinkedHashMap<String, String> individualMap = MutationHelper
				.createParentMap(individual.get(listIndex), finalsize);

		return individualMap;
	}

	/**
	 * Returns a randomly selected candidate <!-- begin-user-doc -->
	 * from the list of available candidates.
	 * 
	 * @throws IOException
	 */
	public static EList<EObject> getRandomCandidate(List<EObject> parents,
			int cloneIndex) {

		EList<EObject> candidate = new BasicEList<EObject>();
		candidate = Shared.copyAllGenes(parents.get(cloneIndex));
		//System.out.println("\n The following candidate is selected for mutation: "	+ candidate);
		return candidate;
	}
	
	/**
	 * Returns a randomly selected candidate <!-- begin-user-doc -->
	 * from the list of available candidates.
	 * @param parents the list of available parents individual
	 * @param candidateIndex the index of the selected parent individual
	 * @throws IOException
	 */
	public static EObject getRandomCandidate2(List<EObject> parents,
			int candidateIndex) {

		EObject tmp = parents.get(candidateIndex);
		EObject candidate = EcoreUtil.copy(tmp);
		//System.out.println("\n The following candidate is selected for mutation: " + candidate);
		return candidate;
	}

	/**
	 * Returns a randomly selected index <!-- begin-user-doc -->
	 *
	 * @param max the maximum positive integer
	 * @param excludedInteger integer that should be excluded from the selection
	 * @return the randomly selected index
	 */
	public static int getRandomIndex(int max, int excludedInteger) {

		Random r = new Random();
		int index = excludedInteger;
		while (index == excludedInteger) {
			index = r.nextInt(max);
		}

		return index;
	}
}