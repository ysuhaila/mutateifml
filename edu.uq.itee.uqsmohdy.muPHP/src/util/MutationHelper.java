package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.StructuralFeature;
import org.eclipse.uml2.uml.Class;

import IFML.Core.Annotation;
import IFML.Core.CoreFactory;
import IFML.Core.CorePackage;
import IFML.Core.DomainModel;
import IFML.Core.IFMLModel;
import IFML.Core.InteractionFlow;
import IFML.Core.InteractionFlowElement;
import IFML.Core.InteractionFlowModel;
import IFML.Core.InteractionFlowModelElement;
import IFML.Core.NamedElement;
import IFML.Core.NavigationFlow;
import IFML.Core.ViewComponent;
import IFML.Core.util.CoreResourceFactoryImpl;
import IFML.Extensions.IFMLWindow;
import IFML.Extensions.OnSelectEvent;
import IFML.Extensions.OnSubmitEvent;
import IFML.Core.UMLBehavior;
import IFML.Core.UMLBehavioralFeature;
import IFML.Core.UMLDomainConcept;
import IFML.Core.UMLStructuralFeature;
import IFML.Core.util.CoreResourceFactoryImpl;
import IFML.Core.util.CoreResourceImpl;


public class MutationHelper {
	
	private static String a1;
	private static String tempFlow;
	private static String curName;
	private static String insertedWindowPair;
	private static EObject bPtr;
	private static Iterator<EObject> windowTree;
	private static String matchedWindow;
	private static int navflowMatched;
	private static int navflowControl;
	private static int flowCtr;
	private static TreeIterator<EObject> sysTree;
	static EList<String> possibleFlow;
	
	private static List<HashMap<String, String>> map = new ArrayList<HashMap<String, String>>();
	private static List<LinkedHashMap<String,String>> map2 = new ArrayList<LinkedHashMap<String, String>>();
	private static LinkedHashMap<String, String> targetWindow;
	private static LinkedHashMap<String, String> tempMap;
	public static String fileName;
	
	//private static EList<EObject> offspring;

	/**
	 * Create a reference map for an individual or offspring. <!-- begin-user-doc -->
	 * From the root of the object, it gets the IFML Window - Navigation Flow pair
	 * of the object and saves the pair sequence as a LinkedHashMap.
	 */	
	static LinkedHashMap<String, String> createParentMap(EObject rootNode, int maxWindows) {
		// Declare the map.
		LinkedHashMap<String, String> parentMap = new LinkedHashMap<String, String>();
		// Display the contents of the individual first.
		//Shared.displayContents2(rootNode);
		
		// Check if the parent contains at least the root gene.
		if (rootNode == null) {
			System.out.println("\nThere are no individual selected!\n");
		}
		else {

			// Get all genes of the parent chromosome.
			TreeIterator<EObject> parentIt = rootNode.eAllContents();
			// genePtr points to the current element.
			EObject genePtr = parentIt.next();
			EObject navflowPtr;
			String previousWindow = null;
			
			for (int j = 0; j < maxWindows; j++) {			
				while(parentIt.hasNext()) {			
					// Find the default Window.
					if(genePtr instanceof IFMLWindow && ((IFMLWindow) genePtr).isIsDefault() == true) {
						EObject rootWindow = genePtr;
						genePtr = parentIt.next();
						//System.out.println("The root Window for this individual is: " + ((IFMLWindow)rootWindow).getName() + ".\n");
						
						// Find the navigation flow of the default/root Window.
						TreeIterator<EObject> rootWindowchildren = rootWindow.eAllContents();
						navflowPtr = rootWindowchildren.next();
						
						// Saves the default window of parent its navigation flow as the first entry of the LinkedHashmap.
						while (!(navflowPtr instanceof NavigationFlow)) {
							navflowPtr = rootWindowchildren.next();
						}
						if (navflowPtr instanceof NavigationFlow) {
							System.out.println("Navigation Flow:\n" + navflowPtr);
							parentMap.put(((IFMLWindow)rootWindow).getName(), ((NavigationFlow)navflowPtr).getTargetInteractionFlowElement().getName());
							previousWindow = ((NavigationFlow)navflowPtr).getTargetInteractionFlowElement().getName();
						}
					} // end if(genePtr instanceof IFMLWindow && ((IFMLWindow) genePtr).isIsDefault() == true)
					
					// Find the rest of the window genes after the default Window.
					else if (genePtr instanceof IFMLWindow && ((IFMLWindow)genePtr).getName().equals(previousWindow)) {
						// curWindow loads all elements in the current Window.
						TreeIterator<EObject> curWindow = genePtr.eAllContents();
						if (curWindow.hasNext()) navflowPtr = curWindow.next();

						while (curWindow.hasNext()) {
							// this has to be done to skip the annotation, or else navigation flow won't be found
							navflowPtr = curWindow.next();			
							if (navflowPtr instanceof NavigationFlow) {
								String interactionFlow = "";
								if (navflowPtr.eIsSet(CorePackage.eINSTANCE.getInteractionFlow_TargetInteractionFlowElement())) {
									interactionFlow = ((NavigationFlow)navflowPtr).getTargetInteractionFlowElement().toString();
								}
								String targetinteractionFlow = "";
								if (interactionFlow.length() != 0) {
									targetinteractionFlow = ((NavigationFlow)navflowPtr).getTargetInteractionFlowElement().getName();
								}
								parentMap.put(((IFMLWindow)genePtr).getName(), targetinteractionFlow);	
								if (interactionFlow.length() != 0) {
									previousWindow = ((NavigationFlow)navflowPtr).getTargetInteractionFlowElement().getName();
								}
								break;
							}  // end if (navflowPtr instanceof NavigationFlow)
						}  // end while (curWindow.hasNext())
						genePtr = parentIt.next();
					} // end else if (genePtr instanceof IFMLWindow)
					else {
						genePtr = parentIt.next();
					}  // end else 
				} // end while(parentIt.hasNext())
			} // end for
		} // end else
		// Return the map of the parent chromosome.
		return parentMap;
	}	
	
	/**
	 * C3: Create a reference map for possible offspring. <!-- begin-user-doc -->
	 * 1. load the parent model.
	 * 2. find and mark the default window as the first entry.
	 * 3. find the rest of the window.
	 */	
	static LinkedHashMap<String, String> oldcreateParentMap(EObject rootNode, int max) {
		// Declare the map.
		LinkedHashMap<String, String> oldparentMap = new LinkedHashMap<String, String>();
		
		// Check if the parent contains at least the root gene.
		if (rootNode == null) {
			System.out.println("\nThere are no individual selected!\n");
		}
		else {
			for (int j=0; j<max; j++) {
				// Get all genes of the parent chromosome.
				TreeIterator<EObject> parentIt = rootNode.eAllContents();
				// genePtr points to the current element.
				EObject genePtr = parentIt.next();
				EObject navflowPtr;
				String previousWindow = null;
				
				while(parentIt.hasNext()) {			
					// Find default Window.
					if(genePtr instanceof IFMLWindow && ((IFMLWindow) genePtr).isIsDefault() == true) {
						EObject rootWindow = genePtr;
						genePtr = parentIt.next();
						System.out.println("\nRoot Window for Parent 1 is: " + ((IFMLWindow)rootWindow).getName());
						
						// Find navigation flow inside the root Window.
						TreeIterator<EObject> rootWindowchildren = rootWindow.eAllContents();
						navflowPtr = rootWindowchildren.next();
						//System.out.println(navflowPtr);
						
						// put root window of parent and navigation flow into hashmap
						while (rootWindowchildren.hasNext()) {

							// this has to be done to skip the annotation, or else navigation flow won't be found
							navflowPtr = rootWindowchildren.next();
							if (navflowPtr instanceof NavigationFlow) {
								oldparentMap.put(((IFMLWindow)rootWindow).getName(), ((NavigationFlow)navflowPtr).getTargetInteractionFlowElement().getName());
								previousWindow = ((NavigationFlow)navflowPtr).getTargetInteractionFlowElement().getName();
								//System.out.print("\nkey and value is: "+ parentMap.get("Index"));
							}
							else {
								//System.out.println("Cuba lagi");
								//System.out.println(navflowPtr);
								navflowPtr = rootWindowchildren.next();
							}
						}  //end while (rootWindowchildren.hasNext())	
					} // end if(genePtr instanceof IFMLWindow && ((IFMLWindow) genePtr).isIsDefault() == true)
					
					// Find the rest of the window genes.
					else if (genePtr instanceof IFMLWindow  && ((IFMLWindow)genePtr).getName().equals(previousWindow)) {
						//System.out.println("\nWindow is " +genePtr);
						TreeIterator<EObject> curWindow = genePtr.eAllContents();
						navflowPtr = curWindow.next();

						while (curWindow.hasNext()) {
							// this has to be done to skip the annotation, or else navigation flow won't be found
							navflowPtr = curWindow.next();			
							if (navflowPtr instanceof NavigationFlow) {
								//System.out.println("\nNav Flow sits on " +navflowPtr);
								//System.out.println("\n\tI want to print Hashmap.\n");
								oldparentMap.put(((IFMLWindow)genePtr).getName(), ((NavigationFlow)navflowPtr).getTargetInteractionFlowElement().getName());	
								break;
							}  // end if (navflowPtr instanceof NavigationFlow)
						}  // end while (curWindow.hasNext())
						genePtr = parentIt.next();
					} // end else if (genePtr instanceof IFMLWindow)
					else {
						genePtr = parentIt.next();
					}  // end else 
				} // end while(parentIt.hasNext())
			} // end for
		} // end else
		// Return the map of the parent chromosome.
		return oldparentMap;
	}	
	
	/**
	 * F3: A method that finds similar object. <!-- begin-user-doc -->
	 */
	public static void findSimilar(String k, String v, EObject rootTree, int c, int d, String swapNow) {
		// Base case, if general model tree has no nodes
		if (rootTree == null) {
			System.out.println("\nCould not find the general model.");
		}
		else {
			if (c == 0 && d > 0) {
				// If the counter begins again, but for the second time onwards, map is reset to empty.
				map2 = new ArrayList<LinkedHashMap<String, String>>();
			}
			sysTree = rootTree.eAllContents();
			targetWindow = new LinkedHashMap<String, String>();
			// finds the matching "Window-Next Window" pair
			while (sysTree.hasNext()) {
				EObject curWin = sysTree.next();
				// if a gene of type Window is found in the general model.
				if (curWin instanceof IFMLWindow) {
					String foundWindow = ((IFMLWindow)curWin).getName();
					// Check if the selected gene (Window) of the general model matches the "Window" entry in the LinkedHashMap.
					if (isWindowEqual(foundWindow,k)) {
						// If match is found, get the navigation flows of the selected gene (Window).
						System.out.println("Window Found : " + foundWindow);
						System.out.println("\n  Next, finding the Navigation Flow that connects to this Window: " + v);
						String nextWindow = findaltNavFlow(v, curWin, 0);
						// Check if the selected Navigation Flow of the general model matches the "Next Window" entry in the LinkedHashMap.
						switch(swapNow) {
						case "0":
							if (v.startsWith(nextWindow)) {	
								System.out.println("\nFound the Navigation Flow that connects to this Window: " + nextWindow);
								// Put in a hashmap 
								targetWindow.put(foundWindow, nextWindow);
								// add the hashmap as an element in arraylist
								map2.add(targetWindow);
								return;
							}
							else {
								System.out.println("\nSorry this navigation flow does not match.");
							}
								break;
							
						case "1":
							System.out.println("\nFound alternate Navigation Flow that connects to this Window: " + nextWindow);
							// Put in a hashmap 
							targetWindow.put(foundWindow, nextWindow);
							// add the hashmap as an element in arraylist
							map2.add(targetWindow);
							
							break;
						default:
							//System.out.println("\nSorry no alternate navigation flow found.");
							System.out.println("\nSorry this navigation flow does not match.");
							break;
						} // end switch
						//return;
					}
				}
			} // end while
			return;
		}
	}
	
	// recursive method - find navigation flow
	public static String findanyNavFlow( String keyWindowName, EObject ifmlWindow, int navTreeExistence, int rd) {
		// base case
		if (navTreeExistence > 0) {
			String a2 = "Home";
			return a2;
		}
		else {
			// TODO: Introduce randomness to this insert.		
			// create TreeIterator for b first
			if (navTreeExistence == 0 && navflowControl == 0) {
				System.out.println("\nSearching for existing Navigation Flows..\n");
				windowTree = ifmlWindow.eAllContents();
				navflowControl = navflowControl + 1;
				flowCtr = 0;
				bPtr = null;
				possibleFlow = new BasicEList<String>();
			}
			a1 = keyWindowName;
			bPtr = windowTree.next();
			// loop until navigation flow is found
			while (windowTree.hasNext()) {
				if (bPtr instanceof NavigationFlow ) {
					tempFlow = ((NavigationFlow)bPtr).getTargetInteractionFlowElement().getName();
					//if (keyWindowName.contains(tempFlow) == false) {
						//System.out.println("Found possible replacements :" + tempFlow);
						possibleFlow.add(tempFlow);
						flowCtr++;
						//break;
				}
				else{
						//System.out.println("... ");
				//}
				}
				bPtr = windowTree.next();
			} // end while
			System.out.println("Found " + flowCtr + " possible replacements :" + possibleFlow);
			// randomly choose one value
			int selection = Shared.randomiseThis(flowCtr-1);
			tempFlow = possibleFlow.get(selection);
			int printedSelection = selection+1;
			System.out.println("Choosing no " + printedSelection + ", " + tempFlow);
			//matchedWindow = ((NavigationFlow)bPtr).getTargetInteractionFlowElement().getName();
			//System.out.println("  This Navigation Flow connects to: " + matchedWindow + "\n");
		}
		// reset navflowControl
		navflowControl = 0;
		return tempFlow;
	}
	
	/**
	 * F4: A recursive method that finds the navigation flow. <!-- begin-user-doc -->
	 */
	public static String findaltNavFlow(String candidateName, EObject ifmlWindow, int navTreeExistence) {
		String potentialFlow = null;
		// Base case.
		if (navTreeExistence > 0 && a1.contains(potentialFlow)) {
			return tempFlow;
		}
		else {
			// Get the children of the ifmlWindow.
			if (navTreeExistence == 0 && navflowMatched == 0) {
				System.out.println("\nSearching all Navigation Flows..\n");
				windowTree = ifmlWindow.eAllContents();
				navflowMatched = navflowMatched + 1;
				flowCtr = 0;
				bPtr = null;
				possibleFlow = new BasicEList<String>();
			}
			a1 = candidateName;
			bPtr = windowTree.next();
			// iterate through the children.
			while (windowTree.hasNext()) {
				if (bPtr instanceof NavigationFlow ) {
					System.out.println("\nCANDIDATE : " +a1+ "\nWINDOW : " +ifmlWindow+ "\nNAVIGATION FLOW : " +bPtr);
					
					String ifl = ((NavigationFlow) bPtr).getTargetInteractionFlowElement().toString();
					Pattern pattern = Pattern.compile("(name: )(.*)([)] [(]isLandmark)");
					Matcher matcher = pattern.matcher(ifl);
					String iflName = " ";
					if (matcher.find()) {
						iflName = matcher.group(2);
					}
					// the above workaround is to resolve the NullException when the code below is used.
					//tempFlow = ((NavigationFlow)bPtr).getTargetInteractionFlowElement().getName();
					tempFlow = iflName;
					if (candidateName.contains(tempFlow) == false) {
						// if there is more than one alternative flows.
						possibleFlow.add(tempFlow);
						flowCtr++;
						//break;
					}
					else{
						System.out.println("... ");
					}
				}
				bPtr = windowTree.next();
			} // end while
			System.out.println("Found " + flowCtr + " possible replacements :" + possibleFlow);
			// randomly choose one value
			int selection = 0;
			if (flowCtr > 1) {
				selection = Shared.randomiseThis(flowCtr-1);
			}
			tempFlow = possibleFlow.get(selection);
			int printedSelection = selection+1;
			System.out.println("Choosing no " + printedSelection + ", " + tempFlow);
			// reset navflowMatched
			navflowMatched = 0;
			return tempFlow;
		}
		
		//return findsimilarNavFlow(a1, bPtr, navTreeExistence);
	}
	
	// recursive method - find navigation flow
	public static String findEqualNavFlow(String targetFlow, EObject ifmlWindow, int navTreeExistence) {
		String potentialFlow = null;
		EList<EObject> nvFlow = new BasicEList<EObject>();
		// base case
		if (navTreeExistence > 0 && a1.contains(potentialFlow)) {
			return potentialFlow;
		}
		else {
			// create TreeIterator for ifmlWindow first.
			if (navTreeExistence == 0 && navflowMatched == 0) {
				//System.out.println("\nSearching all Navigation Flows..\n");
				// get all the children elements of the ifmlWindow.
				windowTree = ifmlWindow.eAllContents();
				bPtr = windowTree.next(); // TODO: Review this, based on error message " java.lang.ArrayIndexOutOfBoundsException: -1"
				navflowMatched = navflowMatched + 1;
			}
			
			a1 = targetFlow;
			
			while (windowTree.hasNext()) {
				if (bPtr instanceof NavigationFlow) {
					nvFlow.add(bPtr);
				}
				bPtr = windowTree.next();
			}
			
//			// iterate through the elements.
//			//XXX Exception in thread "main" java.lang.StackOverflowError bug
//			while (!(bPtr instanceof NavigationFlow) && windowTree.hasNext()) {
//					// skips the element if it is not a navigation flow.
//					bPtr = windowTree.next();
//			}
			for (EObject nv : nvFlow) {
				potentialFlow = ((NavigationFlow)nv).getTargetInteractionFlowElement().getName();
				if (targetFlow.contains(potentialFlow)) {
					navflowMatched = 0;
					return potentialFlow;
				}
			}
			// found a navigation flow, but not necessarily the right one.
			//potentialFlow = ((NavigationFlow)bPtr).getTargetInteractionFlowElement().getName();
			//System.out.println("  This Navigation Flow connects to: " + potentialFlow + "\n");
			
//			if (targetFlow.contains(potentialFlow)) {
//				navflowMatched = 0;
//				return potentialFlow;
//			}
		}
		//return findEqualNavFlow(a1, bPtr, navTreeExistence);
		return potentialFlow;
	}	

	/**
	 * F1: A breadth-first search method to find matching window-navigation flow pair <!-- begin-user-doc -->
	 */
	
	public static void findMatch(String k, String v, EObject rootTree, int c, int d) {
		// Matching just initiated (exist = 0), Match found (exist = 1), No match found (exist = 999)	
		// Base case, if general model tree has no nodes
		if (rootTree == null) {
			System.out.println("\nCould not find the general model.");
		}
		else {
			if (c == 0 && d > 0) {
				// If the counter begins again, but for the second time onwards, map is reset to empty.
				map = new ArrayList<HashMap<String, String>>();
			}
			sysTree = rootTree.eAllContents();
			targetWindow = new LinkedHashMap<String, String>();
			// finds the matching "Window-Next Window" pair
			while (sysTree.hasNext()) {
				EObject curWin = sysTree.next();
				// if a gene of type Window is found in the general model.
				if (curWin instanceof IFMLWindow) {
					String foundWindow = ((IFMLWindow)curWin).getName();

					// Check if the selected gene (Window) of the general model matches the "Window" entry in the LinkedHashMap.
					if (isWindowEqual(foundWindow,k)) {
						// If match is found, get the navigation flows of the selected gene (Window).
						//System.out.println("\n  Match for Window found. Next, finding the Navigation Flow that connects to this Window: " + v);
						String nextWindow = findEqualNavFlow(v, curWin, 0);
						// Check if the selected Navigation Flow of the general model matches the "Next Window" entry in the LinkedHashMap.
						if (v.startsWith(nextWindow)) {							
						//	System.out.println("\nFound the Navigation Flow that connects to this Window: " + nextWindow);
							// Put in a hashmap 
							targetWindow.put(foundWindow, nextWindow);
							// add the hashmap as an element in arraylist
							map.add(targetWindow);
							return;
						}
						else {
							System.out.println("\nSorry this navigation flow does not match.");
						}
					}
				}
			}
		}
	}
	
	public static boolean isWindowExist2 (String thisWindow, String itsPair, Iterator<EObject> sysTree) {
		if (sysTree.hasNext() == false) {
			System.out.println("\nCould not find the general model.");
		}
		else {
			//sysTree = rootTree.eAllContents();
			// making sure the "Window-Next Window" pair is not present in the map entries.
			while (sysTree.hasNext()) {
				EObject cur = sysTree.next();
				if (cur instanceof IFMLWindow) {
					curName = ((IFMLWindow)cur).getName();
					// if the name is equal
					if (isWindowEqual(curName,thisWindow)) {
						return true;
					}
					// if not equal, move to another window.
					else {
						cur = sysTree.next();
					}
				}
			}
		}
		return false;
	}
	
	public static String isWindowExist (String[] thisWindow, String[] itsPair, EObject rootTree, int count) {
		if (rootTree == null) {
			System.out.println("\nCould not find the general model.");
		}
		else {
			insertedWindowPair = null;
			for (int i = 0; i < count; i++) {	
				sysTree = rootTree.eAllContents();
				// making sure the "Window-Next Window" pair is not present in the map entries.
				while (sysTree.hasNext()) {
					EObject cur = sysTree.next();
					if (cur instanceof IFMLWindow) {
						curName = ((IFMLWindow)cur).getName();
						// if the window match the last target Window, this will be inserted as the new key.
						String tempPair = itsPair[count-1];
						if (tempPair.startsWith(curName)) {
							System.out.println("\nSelecting this one : " + curName);
							insertedWindowPair = findanyNavFlow(curName, cur, 0, count-1);
							break;
						}
					}
				}
				break;
			} // end for
		}
		//newEntryMap.put(alternateWindow, insertedWindowPair);
		return curName;	
	}
	
	public static String getinsertedWindowPair() {
		return insertedWindowPair;
	}
	
/*	public static LinkedHashMap<String, String> getmatchedMap() {
		return map;
	}*/
	
	public static List<HashMap<String, String>> getMap() {
		return map;
	}
	
	public static List<LinkedHashMap<String,String>> getMap2() {
		return map2;
	}
	
    /**
     * Returns <tt>true</tt> if both IFML Windows' names are equal.
     * @param a name of current IFML Window
     * @param b name of the IFML Window being compared with
     * @return <tt>true</tt> if both IFML Windows' names are equal
     */
	public static boolean isWindowEqual(String a, String b) {
		if (a != null) {
			if (b.contains(a) || b.startsWith(a)) { 
				//System.out.println("\n  Match found for: " +b);
				return true;
			}
		}
		return false;
	}
	
	public static boolean isWindowEqualIterator(String thekey, String thevalue, EObject rootTree) {
		if (rootTree == null) {
			System.out.println("\nCould not find the general model.");
		}
		else {
			sysTree = rootTree.eAllContents();
			// Make sure the "Window-Next Window" pair is not present in the map entries.
			while (sysTree.hasNext()) {
				EObject cur = sysTree.next();
				if (cur instanceof IFMLWindow) {
					String window = ((IFMLWindow)cur).getName();
					if (isWindowEqual(window,thekey)) {
						return true;
					}
				}
				else {
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * This method helps the getAlternatives method. <!-- begin-user-doc -->
	 * 1. It iterates the general model to find the replacement window.
	 * 2. candidateWindow holds the window that will be replaced, prevWindow reference the previous window
	 */
	public static String getAlternativesWindow( String previousKey, String candidateWindow, Iterator<EObject> GenModel ) throws IOException {
		//Iterator<EObject> GenModel = GenModel
		EObject objPtr = GenModel.next();
		//InteractionFlowElement inFlow = null;
		String curWindow = null;
		String altWindow = null;
		String nextWindow = null;
		
		while (GenModel.hasNext()) {
			if (objPtr instanceof IFMLWindow){
				curWindow = ((IFMLWindow)objPtr).getName();
				if(previousKey.contains(curWindow)) {
					// if window is the same, get the next window that is equal to the testWindow.
					altWindow = candidateWindow;
					// now, replace the candidateWindow.  Find a navigation flow with different target in the curWindow, i.e. change altWindow.
					altWindow = findaltNavFlow(candidateWindow, objPtr, 0);
					// change the previousKey
					break;
				}
			} // end if (objPtr instanceof IFMLWindow)
			objPtr = GenModel.next();		 
		}
		return altWindow;
	}
	
	/**
	 * This method helps the swap mutation operator. <!-- begin-user-doc -->
	 * 1. It iterates through the windows in the parent individual.
	 * 2. An index controller, swapIdx will randomly chose which window to replace.
	 * 3. A counter, swapCtr will randomly specify how many windows are affected.
	 * 4. If it is the selected index of a window, it will call the getAlternativesWindow to find suitable replacement.
	 * @throws IOException 
	 */
	public static List<HashMap<String, String>> getAlternatives(LinkedHashMap<String, String> p, int size, int swapIdx, int swapCtr) throws IOException {
	
		int index=0;
		String prevKey = null;
		boolean isValid = true;
		map = new ArrayList<HashMap<String, String>>();
		List<HashMap<String, String>> map2 = new ArrayList<HashMap<String, String>>();
		tempMap = new LinkedHashMap<String, String>();
		Iterator<EObject> GenIt = ModelLoader.getGeneralModelIt();
		Shared.printLinkedMap(p);
		System.out.println("\nFinding possible offspring. ");

		for (Map.Entry<String,String> entry : p.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				if(index != size-1) {
					// If the key window is not the swap index, find equal window.
					System.out.println("\nFinding Window name " + key + "...   ");
					if (isWindowExist2(key, value, ModelLoader.getGeneralModelIt())) {
						// If equal window found, move to the next pair
						System.out.println("Found.");
						tempMap.put(key, value);
						prevKey = key;
						map.add(tempMap);
						tempMap = new LinkedHashMap<String, String>();					}
					else {
						System.out.println("\nSorry, Window name " + key + " is not found.\nSorry, this individual is likely to be invalid.");
						isValid = false;
						break;
					}
				}
				// if this is the index where swap needs to be performed, perform swap
				else if (index == size-1) {	
				// If the key is the selected index, swap with other window.
					System.out.println("\nFinding Window name " + key + "...   ");
					// If this window is the same, change to another
					if (isWindowExist2(key, value, GenIt)&& prevKey != null) {
						System.out.println("Swapping now.");
						// get another window
						String thisisIt = getAlternativesWindow( key, value, ModelLoader.getGeneralModelIt());
						// update the previous entry to point to new value for next window
						tempMap.put(key,thisisIt);
						map.add(tempMap);
					}
					else {
						System.out.println("\nSorry, Window name " + key + " is not found.\nSorry, this individual is likely to be invalid.");
						isValid = false;
						break;
					}
				}
				index++;
		}
		// TODO: This one returns wonky arrangement after swapping is done on the middle element (i.e.offspring 2).
		if (isValid = true) return map;
		else return map2;
		
	}
	
	/**
	 * This method converts the HashMap into EObject. <!-- begin-user-doc -->
	 * 1. It will compare the entry in the HashMap with the General Model.
	 * 2. When it finds a match, it will add that match into the List.
	 * TODO: Improve this method, currently the conversion only works with delete mutation sometimes stop.
	 * E.g. Domain Model should appear lastly as the closing elements.
	 * @throws IOException 
	 */
	public static EList<EObject> createOffspring(List<HashMap<String, String>> offspringMap,  EList<EObject> parent) {
		EList<EObject> offspring = new BasicEList<EObject>();
		EList<EObject> finalOffspring = new BasicEList<EObject>();
		EObject rootGen = ModelLoader.getRootWhole();
		TreeIterator<EObject> GMit;
		GMit = rootGen.eAllContents();
		
		// traverse Choosing no
		List<String> windowList = new BasicEList<String>();
		List<EObject> windows = new BasicEList<EObject>();
		int countKey = 0;
		
		// creates a list of window sequence to find, windowList.
		for (HashMap<String,String> map : offspringMap) {
			for (Map.Entry<String,String> entry : map.entrySet()) {
				String OKey = entry.getKey();
				String OValue = entry.getValue();
				
				if (countKey<1) {
					windowList.add(OKey);
					windowList.add(OValue);
				}
				else {
					windowList.add(OValue);
				}
			}
			countKey++;
		}

		int foundWin = 0;
		int windowMarker = 0;
		int c = 0;
		int hasHeader = 0;
		String tempName;
		String tempKey = null;
		//System.out.println("\nConverting offspring to object model..");  // TODO: Review this, based on system failed to terminate here. Possible endless loop?
		
		// For each window name (in sequential order) in the list,
		for (String s : windowList) {
			tempKey = s.replaceAll("\'","");
			// marking the first window search.
			windowMarker = windowMarker+1;
			//windowMarker = windowList.size();
			// while no match is found for tempKey (current window), find the IFMLWindow object in the parent individual that matches the window.
			while ((foundWin != windowMarker) && (foundWin < windowMarker)) {
				//copy the root from parent individual first.  Do this once only.
				if (hasHeader != 1) {
					for (EObject object : parent) {
						// create the root object for the offspring first
						//if (object instanceof IFMLModel) {
						if (!(object instanceof IFMLWindow)) {
								offspring.add(object);
								hasHeader = 1;
						}
						else break;
					}
				}
				for (EObject object : parent) {
					if (object instanceof IFMLWindow) {
						// remove the ' character from subsequent window.
						tempName = (((IFMLWindow)object).getName().replaceAll("\'",""));
						//if window name match
						if (tempKey.contains(tempName)) {
							offspring.add(object);
							// updates foundWin if window is found.
							foundWin = foundWin+1;
							// get the window's children too
							if (windowMarker < windowList.size()) {
								TreeIterator<EObject> tempList = object.eAllContents();
								while(tempList.hasNext()) {
									EObject itObj = tempList.next();
									if (itObj instanceof NavigationFlow) {
										String flow = ((NavigationFlow)itObj).getTargetInteractionFlowElement().getName();
										// if the navigation flow points to the next window in the windowList, add that navigation flow.
										if (flow.contains(windowList.get(foundWin))) {
											offspring.add(itObj);
											//System.out.println(" Conversion in progress.. " + (c=c+1));
										}
									}
									else {
										offspring.add(itObj);
										//System.out.println("Conversion in progress." + (c=c+1));
									}

								}
							}
							break;
						}
					}
				} // end for replaceAll
				
				// find the rest of the window sequence in the general model.
				if ((foundWin != windowMarker) && (foundWin < windowMarker)) {
					System.out.println("Switching to general model.");
					EObject objPtr = GMit.next();
					while (GMit.hasNext()) {	
						if (objPtr instanceof IFMLWindow) {
							tempName = ((IFMLWindow)objPtr).getName();
							//if window name match
							if (tempName.startsWith(tempKey)) {
								offspring.add(objPtr);
								System.out.println("Conversion in progress.");
								foundWin = foundWin+1;
								
								// if this is the last window to search, exclude this part
								if (windowMarker < windowList.size()) {
									// get the window's children too
									TreeIterator<EObject> tempList = objPtr.eAllContents();
									while(tempList.hasNext()) {
										EObject itObj = tempList.next();
										if (itObj instanceof NavigationFlow) {
											String flow = ((NavigationFlow)objPtr).getTargetInteractionFlowElement().getName();
											// if the navigation flow points to the next window in the windowList, add that navigation flow.
											if (flow.contains(windowList.get(foundWin))) {
												offspring.add(itObj);
												System.out.println("Conversion in progress." + (c=c+1));
											}
										}
									}
								}

								break;
							}
							// if the window does not match, find next window.
							objPtr = GMit.next();
						}
						else {
							// if objPtr is not an IFMLWindow, continue to next object.
							objPtr = GMit.next();
						}
					} // end while (GMit.hasNext())
				}

			} // end while (foundWin != windowMarker) 
		} // end for (int i=0; i>=listSize; i++) 

		finalOffspring = cleanModel(offspring);
		
		return finalOffspring;
	}
	
	/**
	 * <!-- begin-user-doc -->Saves the offspring into the "population" directory 
	 * as an IFML test case.<!-- end-user-doc -->
	 * @param model the offspring
	 * @param filename the filename for the offspring
	 * @param filenumber the unique id attached to the offspring's filename
	 * @throws IOException 
	 */
	public static Resource saveModel(EList<EObject> model, String filename, int filenumber) throws IOException {		

		// Copy the model first and save the copy.
		//System.out.println("\nOFFSPRING BEFORE SAVED\n");
		//Shared.displayContents1(model);
		EcoreUtil.Copier copier = new EcoreUtil.Copier(true, false);
		EObject modelCopy = copier.copy(model.get(0));
		copier.copyReferences();
		
		String FileId = timeStamp();
		String Name = filename + FileId + filenumber;
		fileName = filename + FileId + filenumber + ".core";
		File offspringDir = new File("population/" + fileName);
		// For testing purposes, uncomment this.
		//File offspringDir = new File("elite/" + fileName);

		
		//for (EObject change : modelCopy) {
			if (modelCopy instanceof IFMLModel) {
				((IFMLModel)modelCopy).setId(filename + FileId);
				//break;
			}
		//}

		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("core", new CoreResourceFactoryImpl());
		ResourceSet rSet = new ResourceSetImpl();
	
		String path = offspringDir.getAbsolutePath();
		Resource myOffspringModel = rSet.createResource(URI.createFileURI(path));
		myOffspringModel.getContents().add(modelCopy);
		
	      try {
	    	  myOffspringModel.save(null);
	        } catch (IOException e) {
	          e.printStackTrace();
	        }
		
		return myOffspringModel;
	}
	
	/**
	 * Returns a unique id for the filename. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return filenam's id
	 * @generated
	 */
	public static String timeStamp() {
		String timestamp = null;
	    //getting current date and time using Date class
	    DateFormat df = new SimpleDateFormat("-ddMMyy-kkmmssSS");
	    Date dateobj = new Date();
		timestamp = df.format(dateobj).toString();
		return timestamp;
	}
		
	/**
	 * Create a new model. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * TODO: Maybe remove this altogether
	 * @generated
	 */
	public static EObject createInitialModel() {
		CorePackage corePackage = CorePackage.eINSTANCE;
		CoreFactory coreFactory2 = corePackage.getCoreFactory();
		EObject rootObject = coreFactory2.createIFMLModel();
		DomainModel domainModel = coreFactory2.createDomainModel();

		((IFMLModel) rootObject).setDomainModel(domainModel);
		return rootObject;
	}
	
	// TODO: Remove this
	public static CoreFactory getCoreFactory() {
		CorePackage corePackage = CorePackage.eINSTANCE;
		CoreFactory coreFactory3 = corePackage.getCoreFactory();
		return coreFactory3;
	}

	/**
	 * Create a new model without its Domain Model Elements. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * TODO: Maybe remove this altogether when it is no longer used.
	 * @generated
	 */
	public static EList<EObject> removeDomainModel(EList<EObject> m) {
		EList<EObject> model1 = new BasicEList<EObject>();
		for (EObject k : m) {
			if (k instanceof DomainModel) {
				break;
			}
			else {
				//
				model1.add(EcoreUtil.copy(k));
			}
		}
		
		return model1;
	}
	
	/**
	 * Clean up the Interaction Flow Model in the model. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static EList<EObject> cleanModel(EList<EObject> m) {
		EList<EObject> windowList = new BasicEList<EObject>();
		// get the window list first
		Iterator<EObject> iterator = m.iterator();
		while (iterator.hasNext()) {	
			EObject obj = iterator.next();
			if (obj instanceof IFMLWindow) {
				windowList.add(obj);
			}
		}	

		int idx = 0;
		// remove IFMLWindow that is not in the list.
		for (EObject j : m) {
			if (j instanceof InteractionFlowModel) {
				for (int i=0; i<windowList.size(); i++) {
					// if the window did not exist, add the the window into the interaction flow model.
					if (!((InteractionFlowModel)j).getInteractionFlowModelElements().contains(windowList.get(i))) {
						((InteractionFlowModel)j).getInteractionFlowModelElements().add((InteractionFlowModelElement) windowList.get(i));
					}
					
				}

			}
		}
		
		return m;
		
	}
	
	/**
	 * Remove unwanted ViewComponent, Details, List <!-- begin-user-doc --> <!-- end-user-doc -->
	 * and their Navigation Flow in the offspring model.
	 * @generated
	 */
	public static EList<EObject> cleanupModel(EList<EObject> theOffspring, int insertIndex, int navFlowIndex, int nextWindowIndex, int windowIndex) {
		EList<EObject> model1 = new BasicEList<EObject>();
		int tempIndex = 0;
		int t = theOffspring.size();
		int windowI = 0;
		String navFlowID = "";
		
		for (int i=0; i<t; i++) {
			// copy all except the excessive component from offspring into model1.
			if (i <= navFlowIndex) {
				if (i == navFlowIndex) {
					navFlowID = ((NavigationFlow)theOffspring.get(i)).getId();
				}
				model1.add(theOffspring.get(i));
			}
			else if(i >= nextWindowIndex) {
				model1.add(theOffspring.get(i));
			}
		}
		
		// in the InteractionFlowModel
		// get the container (Interaction Flow Model)
		EList<EObject> deleteContainer = new BasicEList<EObject>();
		EObject savethis = null;
		Iterator<EObject> topIt = model1.iterator(); 
		EObject ifmlElement = topIt.next();
		while(!(ifmlElement instanceof InteractionFlowModel)) {
			ifmlElement = topIt.next();
		}
		if (ifmlElement instanceof InteractionFlowModel) {
			// get the window in the InteractionFlowModel.
			EObject child = ((InteractionFlowModel)ifmlElement).getInteractionFlowModelElements().get(windowIndex);
			Iterator<EObject> childIt = child.eAllContents();
			while (childIt.hasNext()) {
				EObject temp = childIt.next();
				if (temp instanceof NavigationFlow) {
					if (((NavigationFlow)temp).getId().equals(navFlowID)) {
						// savethis contains the container that has the intended NavigationFlow
						savethis = temp.eContainer().eContainer();
					}
					if (!((NavigationFlow)temp).getId().equals(navFlowID)) {
						EObject o = temp.eContainer();
						// check this
						// if this container is not the container that holds the intended NavigationFLow
						if (!deleteContainer.contains(o.eContainer()) && !(o.eContainer().equals(savethis))) {
							deleteContainer.add(o.eContainer());
							//EcoreUtil.remove(temp.eContainer().eContainer());
						}
					}
				}
			}
		}
		
		// remove the unwanted viewComponent or NavigationFlow
		for (EObject component : deleteContainer) {
			String componentID = ((NamedElement)component).getId();
			Iterator<EObject> topIt2 = model1.iterator(); 
			EObject ifmlElement2 = topIt.next();
			while(!(ifmlElement2 instanceof InteractionFlowModel)) {
				ifmlElement2 = topIt2.next();
			}
			if (ifmlElement2 instanceof InteractionFlowModel) {
				// get the window in the InteractionFlowModel.
				EObject child2 = ((InteractionFlowModel)ifmlElement).getInteractionFlowModelElements().get(windowIndex);
				Iterator<EObject> childIt2 = child2.eAllContents();
				while (childIt2.hasNext()) {
					EObject temp2 = childIt2.next();
					if (temp2 instanceof ViewComponent && temp2.equals(savethis)) {
						Iterator<EObject> childIt3 = temp2.eAllContents();
						while (childIt3.hasNext()) {
							EObject temp4 = childIt3.next();
							if (temp4 instanceof NavigationFlow) {
								if (!((NavigationFlow)temp4).getId().equals(navFlowID)) {
									EcoreUtil.remove(temp4);
								}
							}
						}
					}
					if (component.equals(temp2)) {
						EcoreUtil.remove(temp2);
						break;
					}
				}
			}
		}

		
		return model1;
	}
	
	/**
	 * Return the filename for the offspring<!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the filename for the offspring
	 */
	public static String getfileName() {
		if (fileName.isEmpty()) {
			return "";
		}
		else return fileName;
	}
	
	/**
	 * Logs the parents of the offspring and the offspring's filename <!-- begin-user-doc -->
	 * 
	 * @param offspringfileName the filename
	 * @param offspring the offspring
	 */
	public static void writeLog(String offspringfileName, EList<EObject> offspring) {
		String logFile = Assistant.getLogFile();
		BufferedWriter bw = null;
		try  
		{
			String ancestry = "";
			if (offspring.get(0) instanceof IFMLModel) {
				ancestry = ((IFMLModel)offspring.get(0)).getName();
			}
			String contents = ancestry + "\t" + offspringfileName + "\n";
		    FileWriter fstream = new FileWriter(logFile, true); //true tells to append data.
		    bw = new BufferedWriter(fstream);
		    bw.write(contents);
		}
		catch (IOException e)
		{
		    System.err.println("Error: " + e.getMessage());
		}
		finally
		{
		    if(bw != null) {
		        try {
					bw.close();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		}
	}
}