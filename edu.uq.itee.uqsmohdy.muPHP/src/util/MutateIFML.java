/**
 * <copyright>
 *
 * Copyright (c) 2015 School of Information Technology and Electrical Engineering, The University of Queensland.
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package util;

/**
 * Info: An application of optimising IFML test case
 * Version: 1.0.2
 * Date: 15 June 2017
 * Author: Suhaila Mohd. Yasin
 */

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import IFML.Core.IFMLModel;
import printer.SeleniumPrinter;
import util.Assistant;
import wamutator.WebMutate;

public class MutateIFML {

	/**
	 * MAIN PROGRAM
	 * @param args
	 * @throws IOException
	 */
	public static void main(final String[] args) throws IOException {

		System.out.println("********************************************");
		String welcomeMsg = "\nWelcome to MutateIFML.\nMutateIFML can run up to 20 cycles.\nA cycle has 100 generations."
				+ "\nMutateIFML will reset the current cycle\nif ofspring are produced.";
		String guide = "\nHOW TO USE:\n1. Before running MutateIFML, open the config.txt file\nin the configuration"
				+ " folder.\n2. Make sure the correct directories have been specified\nfor the following items:"
				+ "\n\t2.1. SUT - The local directory for the system under\n\ttest. Leave blank if not in use. "
				+ "\n\t2.2 Test Cases - The directory where the Selenium\n\t.html test cases is located."
				+ "\n\t2.3 Base URL - The URL address for the system under\n\ttest if testing is performed using a server. Leave"
				+ "\n\tblank if not in use.";
		
		int cycle = 0;
		int maxGeneration = 100;
		boolean resetMe = false;
		
		// Humbug's Test Controller can be invoked by inserting Assistant.callHumbugTest();
		// Humbug's Mutation Controller can be invoked by inserting Assistant.callHumbugMutate();
		// System.out.println("\nStarting the fault seeding process first.");
		
		// Initialise the EMF model first.
		Assistant.initialiseModel();

		for (int i=0; i<maxGeneration; i++) {
			System.out.println(welcomeMsg);
			System.out.println(guide);
			//System.out.println("\nCurrent generation: " + Integer.toString(i));
			
			if (resetMe != true && cycle < 20) {
				Assistant.IFMLMutator(i);
				Assistant.callHumbugTest();
				Assistant.restoreDB("a2049869_admin", "s4313098", new File("dbbackup/backupdb.sql").getCanonicalPath());
				// debug Assistant.java line 340-348 insert command to move fit offspring into current population
				System.out.println("Reset the current generation");
			}
			else {
				resetMe = false;
				i = 0;
				cycle = cycle + 1;
			}
		}
		System.out.println("\nThank you for using MutateIFML.");
		System.exit(0);
	}
}