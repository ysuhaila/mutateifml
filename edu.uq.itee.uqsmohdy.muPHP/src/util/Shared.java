package util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;

import org.eclipse.emf.common.util.AbstractTreeIterator;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import IFML.Core.Annotation;
import IFML.Core.CorePackage;
import IFML.Core.CoreFactory;
import IFML.Core.DomainModel;
import IFML.Core.IFMLAction;
import IFML.Core.IFMLModel;
import IFML.Core.InteractionFlowModelElement;
import IFML.Core.NamedElement;
import IFML.Core.InteractionFlowElement;
import IFML.Core.InteractionFlowModel;
import IFML.Core.NavigationFlow;
import IFML.Core.ViewComponent;
import IFML.Core.impl.DomainModelImpl;
import IFML.Core.impl.IFMLModelImpl;
import IFML.DataTypes.DataTypesPackage;
import IFML.Extensions.ExtensionsPackage;
import IFML.Extensions.IFMLWindow;
import IFML.Extensions.OnSelectEvent;

public class Shared {
	
	protected static IFMLModel rootWhole = null;
	protected static EObject[] rootParents;
	protected static int areIndividualsloaded;
	protected static int ParentsQty;
	
	private static EObject rootWindow;
	private static EList<EObject> chromosome;
	private static Iterator<EObject> parentIterator;
	private static EObject genePointer;

	/**
	 * <!-- begin-user-doc -->A method that finds a navigation flow 
	 * in an IFML Window.<!-- end-user-doc -->
	 */
	public static EList<EObject> findNavigationFlows(EObject window) {
		EObject finder = null;
		EList<EObject> outFlowsList = new BasicEList<EObject>();
		TreeIterator<EObject> iter = null;
		if (window instanceof IFMLWindow) {
			iter = window.eAllContents();
			finder = iter.next();
			// Find Navigation Flow(s).
			while (iter.hasNext()) {
				if (finder instanceof NavigationFlow == false) {
					finder = iter.next();
				}
				else {
					outFlowsList.add(finder);
					finder = iter.next();
				}
			} 
		}
		return outFlowsList;
	}	
	
	/**
	 * <!-- begin-user-doc -->A method that returns true if a required navigation flow 
	 * exist in an IFML Window.<!-- end-user-doc -->
	 */
	public static boolean isNavigationFlowExist(EObject currentWindow, String outWindow) {
		EObject finder = null;

		TreeIterator<EObject> iter = currentWindow.eAllContents();
		// Find Navigation Flow(s).
		while (iter.hasNext()) {
			finder = iter.next();
			if (finder instanceof NavigationFlow) {
				InteractionFlowElement out = ((NavigationFlow)finder).getTargetInteractionFlowElement();
				if(out != null) {
					String OutFlow = out.getName();
					// If the navigation flow connects to nextWindow, return true.
					if (OutFlow.contains(outWindow)) {
						return true;
					}
				}
			}
		} 
		return false;
	}	

	/**
	 * <!-- begin-user-doc -->Returns a list of IFML Windows from the general model
	 * that can be inserted between the previous IFML Window and the next IFML Window.<!-- end-user-doc -->
	 * 
	 * @param previousWindow the IFML Window before the insertion point
	 * @param nextWindow the IFML Window after the insertion point
	 * @return a list of available IFML Window genes for insert mutation
	 */
	public static EList<EObject> findBridgeWindows(EObject previousWindow, String nextWindow) {
		
		EList<EObject> finalBridgeWindows = new BasicEList<EObject>();
		EList<EObject> BridgeWindows = new BasicEList<EObject>();
		
		if (previousWindow instanceof IFMLWindow || previousWindow instanceof IFMLAction) {
			String objectClass = previousWindow.eClass().getName();
			TreeIterator<EObject> iter = previousWindow.eAllContents();
			EObject finder = iter.next();
			// Builds a list of out Interaction Flow(s) of the previousWindow.
			while (iter.hasNext()) {
				if (finder instanceof NavigationFlow){
					EObject targetFlow = ((NavigationFlow)finder).getTargetInteractionFlowElement();
					if (targetFlow != null) {
						// Captures every IFML Windows that have connections from the previousWindow.
						String name = ((NamedElement)targetFlow).getName();
						// Avoid adding targetFlow that is the same as the existing nextWindow.
						if (!name.contains(nextWindow)) {
							BridgeWindows.add(targetFlow);
						}
						
					}
					finder = iter.next();
				}
				else finder = iter.next();
			} 
		}
		
		// Use the tempWindows list to get all listed windows from the general model.
		// Filter BridgeWindows to only contain IFML Window(s) that connect(s) to nextWindow.
		if (!BridgeWindows.isEmpty()) {
			for (EObject obj : BridgeWindows) {				
				// If nextWindow is not defined, keep all alternative Windows.
				if(nextWindow.isEmpty()) {
					finalBridgeWindows.add(obj);
				}
				// If returns true if the TargetInteractionFlow for any of this window equals to nextWindow. 
				// Keep the window, and remove the rest.
				else if (isNavigationFlowExist(obj, nextWindow)) {
					finalBridgeWindows.add(obj);
				}
			}
		}
		
		if (BridgeWindows.isEmpty() || finalBridgeWindows.isEmpty()) {
			System.out.println("\n:::WARNING: Cannot find suitable Window to insert:::");
		}
		
		return finalBridgeWindows;
	}
	
	private static EList<EObject> getTempWindows(EObject sysRoot, EList<EObject> tempWindows) {
		EList<EObject> finalWindows = new BasicEList<EObject>();
		
		for (EObject obj : tempWindows) {
			String objName = ((NamedElement)obj).getName();
			TreeIterator<EObject> sysIt = sysRoot.eAllContents();
			while (sysIt.hasNext()) {
				EObject finder = sysIt.next();
				
				if (finder instanceof IFMLWindow) {
					String finderName = ((NamedElement)finder).getName();
					if (finderName.equalsIgnoreCase(objName)) {
						EObject tempObj = EcoreUtil.copy(finder);
						finalWindows.add(tempObj);
					}
				}
			}		
		}	
		return finalWindows;
	}

	/**
	 * <!-- begin-user-doc -->Returns a list of IFML Windows from the general model 
	 * that could bridge between the previous IFML Window and the next IFML Window.<!-- end-user-doc -->
	 */
	public static EList<EObject> findBridgeModelElement(EObject previousModelElement, String nextWindow) {
		EObject finder = null;
		EList<EObject> finalBridgeWindows = new BasicEList<EObject>();
		EList<EObject> BridgeWindows = new BasicEList<EObject>();
		TreeIterator<EObject> iter = null;
		if (previousModelElement instanceof InteractionFlowModelElement) {
			iter = previousModelElement.eAllContents();
			finder = iter.next();
			// Ccreates a list of Navigation Flow(s) for the previouswindow.
			while (iter.hasNext()) {
				if (!(finder instanceof NavigationFlow)) {
					finder = iter.next();
				}
				else if (finder instanceof NavigationFlow){
					InteractionFlowElement IFlow = ((NavigationFlow)finder).getTargetInteractionFlowElement();
					if (IFlow != null) {
						// Directly captures every IFML Windows that are connected to the previousWindow
						BridgeWindows.add(EcoreUtil.copy(((NavigationFlow) finder).getTargetInteractionFlowElement()));
						// Add the interaction flow.

					}
					finder = iter.next();
				}
			} 
		}

		// Filters BridgeWindows to only contain IFML Window(s) that connect(s) to nextWindow.
		if (!BridgeWindows.isEmpty()) {
			for (EObject obj : BridgeWindows) {				
				// If the TargetInteractionFlow for any of this window equals to nextWindow, keep the window, and remove the rest.
				if(nextWindow.isEmpty()) {
					finalBridgeWindows.add(obj);
				}
				// Retrieve the Navigation Flow(s) for each window that connect(s) to nextWindow.
				else if (isNavigationFlowExist(obj, nextWindow) && !((IFMLWindow)obj).getName().contains("Staff Login")) {
					finalBridgeWindows.add(obj);
				}
			}
		}
		else {
			System.out.println("\n:::WARNING: Cannot find suitable Window to insert. Stopping the program.::");
			System.exit(0);
		}
		
		return finalBridgeWindows;
	}
	
	/**
	 * <!-- begin-user-doc -->Returns a list of IFML Window objects.<!-- end-user-doc -->
	 */
	public static EObject getBridgeWindows(String window) {
		EObject theWindow = null;
		EObject sysRoot = ModelLoader.getRootWhole();
		TreeIterator<EObject> sysIt = sysRoot.eAllContents();
		EObject obj = sysIt.next();
		
		while (sysIt.hasNext()) {
			if (obj instanceof IFMLWindow == false) {
				obj = sysIt.next();
			}
			else {
				String tempWindow = ((IFMLWindow) obj).getName();
				if ((window != null && !window.isEmpty()) && (window.contains(tempWindow))) {
					// Return this.
					theWindow = obj;
					break;
				}
				else {
					obj = sysIt.next();
				}
			}
		}
		return theWindow;
	}
	
	/**
	 * <!-- begin-user-doc -->Returns a list of the offspring's alternatives.<!-- end-user-doc -->
	 */
	public static EList<EObject> createAlternatives(EObject[] target) {
		EObject theWindow = null;
		String [] options = altAnnotations();
		EObject finder = null;
		EObject opRoot = target[0];
		TreeIterator<EObject> opIt = opRoot.eAllContents();
		EObject obj = opIt.next();
		String[] replacementArray = altAnnotations();
		int count = replacementArray.length;
		int index = 0;
		EList<EObject> altOPArray = new BasicEList<EObject>(); 
		
		EcoreUtil.Copier copier = new EcoreUtil.Copier(true, false);
		
		while (opIt.hasNext()) {
			if (obj instanceof IFMLWindow == false) {
				obj = opIt.next();
			}
			else {
				theWindow = obj;
				TreeIterator<EObject> iter = null;
				if (theWindow instanceof IFMLWindow) {
					iter = theWindow.eAllContents();
					finder = iter.next();
					// Find Annotation element.
					while (iter.hasNext()) {
						if (finder instanceof Annotation == false) {
							finder = iter.next();
						}
						else {
							for (String replacement:replacementArray) {
								((Annotation) theWindow).setText(replacement);
								//EObject newOP = copier.copy(opRoot);
								//copier.copyReferences();
								String prevName = ((IFMLModel)opRoot).getName();
								((IFMLModel)opRoot).setName(prevName+"-CL-"+index);
								altOPArray.add(opRoot);
								index++;
							}
							
							finder = iter.next();
						}
					} 
				}
				break;
			}
		}
		return altOPArray;		
		
	}
	
	// TODO: Remove method Shared.copyAllGenes if EcoreUtil.copy is relevant.
	/**
	 * <!-- begin-user-doc -->P1: A method that prints the linked hashmap contents.<!-- end-user-doc -->
	 * @throws IOException 
	 */		
	public static void printMap(List<HashMap<String, String>> m) {
		System.out.println("\n\n  OFFSPRING CONTENTS  ");
		for (HashMap<String, String> entries : m ) {
			System.out.println("  Window : " + entries);
			//System.out.println("  Next Window : " + values);	
		}
	}
	
	/**
	 * <!-- begin-user-doc -->The method load that uses loading method from ModelLoader.<!-- end-user-doc -->
	 * @throws IOException 
	 */
	public static void loadAllModels() throws IOException {
		System.out.println("\n Loading the general model and the parent individuals.");
		rootWhole = ModelLoader.getWholemodel();
		rootParents = ModelLoader.loadIndividuals();
		areIndividualsloaded = ModelLoader.areIndividualsLoaded();
	}
	
	/**
	 * <!-- begin-user-doc -->The method returns the number of parent indidviduals loaded.<!-- end-user-doc -->
	 * @throws IOException 
	 */
	public static void getIndividualsQuantity() {
		EObject[] individuals = getrootParents();
		ParentsQty = individuals.length;
		System.out.println("\n" + ParentsQty + " candidates loaded.\n");
	}
	
	/**
	 * <!-- begin-user-doc -->The method returns the size of the population.<!-- end-user-doc -->
	 * @throws IOException 
	 */
	public static int getPopulationSize() throws IOException {
		int size;
		List<EObject> rootlist = ModelLoader.loadPopulations();
		size = rootlist.size();
		return size;
	}
	
	/**
	 * <!-- begin-user-doc -->P1: A method that prints the linked hashmap contents.<!-- end-user-doc -->
	 * @throws IOException 
	 */		
	static void printListofListMap(List<LinkedHashMap<String, String>> m) {
		System.out.println("\n\n  OFFSPRING CONTENTS  ");
		for (HashMap<String, String> entries : m ) {
			System.out.println("  Window : " + entries);
			//System.out.println("  Next Window : " + values);	
		}
	}
	
	/**
	 * <!-- begin-user-doc -->P1: A method that prints the linked hashmap contents.<!-- end-user-doc -->
	 * @throws IOException 
	 */		
	public static void printLinkedMap(LinkedHashMap<String,String> m) {
		System.out.println("\n  OFFSPRING CONTENTS  ");
		for (Map.Entry<String,String> entries : m.entrySet()) {
			String keys = entries.getKey();
			String values = entries.getValue();
			System.out.println("  Window : " + keys);
			System.out.println("  Next Window : " + values);	
		}
	}
	
	/**
	 * <!-- begin-user-doc -->P1: A method that prints the linked hashmap contents.<!-- end-user-doc -->
	 * @throws IOException 
	 */		
	static void printHashMap(HashMap<String,String> m) {
		System.out.println("\n\n  OFFSPRING CONTENTS  ");
		for (Map.Entry<String,String> entries : m.entrySet()) {
			String keys = entries.getKey();
			String values = entries.getValue();
			System.out.println("  Window : " + keys);
			System.out.println("  Next Window : " + values);	
		}
	}
	
	/**
	 * <!-- begin-user-doc -->Displays the contents of a model.<!-- end-user-doc -->
	 * @param e the model
	 */
	public static void displayContents1(EList<EObject> e) {
		if (e != null) {
			System.out.println("\n***CONTENTS***\n");
			for (EObject childObj : e) {
				System.out.println(childObj);
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->D3: A simple method that displays the contents of a model.<!-- end-user-doc -->
	 * TODO: Check and clean-up code.
	 */
	public static void displayContents2(EObject obj) {
		System.out.println("\n***CONTENTS***\n");
		System.out.println(obj);
		TreeIterator<EObject> it = obj.eAllContents();
		EObject childObj = it.next();
		while (it.hasNext() && childObj instanceof DomainModel == false) {
			System.out.println(childObj);
			childObj = it.next();
		}
	}
	
	/**
	 * <!-- begin-user-doc -->Returns a copy of the individual's chromosome.<!-- end-user-doc -->
	 * @param rootChromosome the element that is going to be copied
	 * @return a copy of the individuals' chromosome
	 */	
	public static EList<EObject> copyGene(EObject rootChromosome) {	
		// Check if the root node of the chromosome exist.
		if (rootChromosome instanceof DomainModelImpl) {
			genePointer = parentIterator.next();
			chromosome.remove(genePointer);
			return chromosome;
		}
		else {
			// Make sure that parentIterator is initiated once only, at the start.
			if (rootChromosome instanceof IFMLModel) {
				// Reset chromosome
				chromosome = new BasicEList<EObject>();
				EObject tmp = EcoreUtil.copy(rootChromosome);
				chromosome.add(tmp);
				parentIterator = rootChromosome.eAllContents();
				genePointer = parentIterator.next();
				if (genePointer instanceof Annotation) {
					tmp = EcoreUtil.copy(genePointer);
					chromosome.add(tmp);
					genePointer = parentIterator.next();
				}
				
				copyGene(genePointer);

			}
			else {
				EObject tmp2 = EcoreUtil.copy(genePointer);
				chromosome.add(tmp2);
				genePointer = parentIterator.next();
				copyGene(genePointer);
			}
			//copyGene(genePointer);
		}
		return chromosome;
	}
	

		
	/**
	 * <!-- begin-user-doc -->A recursive method that reads the root object of a model 
	 * and returns a list of the model's children.<!-- end-user-doc -->
	 * @param rootModel the root object of an IFML model
	 * @return the list of the model's children
	 */	
	public static EList<EObject> copyAllGenes(EObject rootModel) {	
		// Check if the root node of the chromosome exist
		if (rootModel == null) {
			System.out.println("\nNo Individual Found.");
			return chromosome;
		}
		// Copy all including Domain Model
		else {
			//Shared.displayContents2(rootModel);
			EcoreUtil.Copier copier = new EcoreUtil.Copier(true, false);
			EObject rootChromosome = copier.copy(rootModel);
			copier.copyReferences();
			// Make sure that parentIterator is initiated once only, at the start.
			if (rootChromosome instanceof IFMLModel) {
				// Reset chromosome
				chromosome = new BasicEList<EObject>();
				chromosome.add(rootChromosome);
				parentIterator = rootChromosome.eAllContents();
				genePointer = parentIterator.next();
				
				while (parentIterator.hasNext()) {
					chromosome.add(genePointer);
					genePointer = parentIterator.next();
				}
			}
		}
		//Shared.displayContents1(chromosome);
		return chromosome;
	}
	
	/**
	 * <!-- begin-user-doc -->C2: A recursive method that copies the individuals.<!-- end-user-doc -->
	 * @return 
	 */	
	public static EList<EObject> copyAllGenes2(EObject rootChromosome) {	
		// Check if the root node of the chromosome exist
		if (rootChromosome == null) {
			System.out.println("\nNo Individual Found.");
			return chromosome;
		}
		// Copy all including Domain Model
		else {
			// Make sure that parentIterator is initiated once only, at the start.
			if (rootChromosome instanceof IFMLModel) {
				// Reset chromosome
				chromosome = new BasicEList<EObject>();
				chromosome.add(rootChromosome);
				parentIterator = rootChromosome.eAllContents();
				genePointer = parentIterator.next();
				if (genePointer instanceof Annotation) {
					chromosome.add(genePointer);
					genePointer = parentIterator.next();
				}			
			}
			else {
				chromosome.add(genePointer);
				genePointer = parentIterator.next();
			}
			copyGene(genePointer);
		}
		return chromosome;
	}
	
	/**
	 * <!-- begin-user-doc -->A recursive method that copies the individuals.<!-- end-user-doc -->
	 * @return 
	 */	
	public static BasicEList<EObject> cloneGenes(EList<EObject> individual) {	
		BasicEList<EObject> clone = new BasicEList<EObject>();
		for (EObject gene : individual) {
			clone.add(EcoreUtil.copy(gene));
		}
		return clone;
	}
	
	/**
	 * <!-- begin-user-doc -->C1: A simple method that returns the size of the selected individual.<!-- end-user-doc -->
	 */	
	private static int countselectedIndividuals(EObject[] rootNode) {
		int count = rootNode.length;
		return count;
	}

	/**
	 * <!-- begin-user-doc -->F2: A method that finds the default window.<!-- end-user-doc -->
	 */
	private static EObject findrootWindow(EObject[] rootNode) {
		
		// If root window exist.
		if (rootWindow != null) {
			System.out.println("\nRoot window already exist.\n");
			return rootWindow;
		}
		else {
			EList<EObject> model = new BasicEList<EObject>();
			int count = countselectedIndividuals(rootNode);
			
			// For every rootNode, retrieve its children element.
			for (int i=0; i<count; i++) {
				model = copyGene(rootNode[i]);
				// For every element inside the current model, find the default window.
				for(EObject objmarker: model) {
					// If default window for the current model is found, assign the window name to rootWindow.
					if(objmarker instanceof IFMLWindow && ((IFMLWindow) objmarker).isIsDefault() == true) {
						rootWindow = objmarker;
						System.out.println("\nRoot Window is: " + ((IFMLWindow)rootWindow).getName());
						System.out.println("\nFor Window: " + ((IFMLWindow)rootWindow).getName() + " has in Interaction Flow: " + ((IFMLWindow)objmarker).getInInteractionFlows().get(0).getId());
					}
				} // End for
			} // End for
		} // End else
		return rootWindow;
	}
	
	/**
	 *  <!-- begin-user-doc -->Returns the number of IFMLWindow elements 
	 *  and IFML Action elements found in the specified individual.<!-- end-user-doc -->
	 *  @param theChromosome the list of EObjects in the individual
	 *  @return the total count of IFMLWindow elements in the individual
	 */	
	public static int numberofWindows(EList<EObject> theChromosome) {
		int windowCount = 0;
		
		Iterator<EObject> iterator = theChromosome.iterator();
		while (iterator.hasNext()) {	
			EObject geneObj = iterator.next();
			if (geneObj instanceof IFMLWindow) {
				windowCount++;
			}
			else if (geneObj instanceof IFMLAction) {
				windowCount++;
			}
		}	
		//System.out.println("\nThe size of this individual is " + windowCount + ".\n");
		return windowCount;
	}
	
	/**
	 * <!-- begin-user-doc -->N1: A simple method that returns the number 
	 * of navigation flows for a window.<!-- end-user-doc -->
	 */	
	static void numberofInFlows(EObject theWindow) {
		System.out.println(((IFMLWindow)theWindow).getOutInteractionFlows());
		
	}
	
	
	/**
	 * <!-- begin-user-doc -->G1: A simple method that returns the size 
	 * of the selected individual.<!-- end-user-doc -->
	 */	
	public static int getChromosomeSize(EList<EObject> theChromosome) {
		int chromosomeSize = 0;
		for(EObject geneObj : theChromosome) {
			chromosomeSize++;
		}	
		return chromosomeSize;
	}
	
	/**
	 * <!-- begin-user-doc -->G2: Generate the random number between two individuals.<!-- end-user-doc -->
	 */	
	public static int generateCrossoverIndex(int individualSize1, int individualSize2) {	
		int shortestIndividual;
		// Find the shortest array
		if (individualSize1 < individualSize2) {
			shortestIndividual = individualSize1;
		}
		else {
			shortestIndividual = individualSize2;
		}
		
		System.out.println("\nShortest Individual is " + shortestIndividual);
		Random rand = new Random();
		// nextInt(size of range) + 1 because size of range started from 0 until (shortestIndividual-1)
		int  rd = rand.nextInt(shortestIndividual)+1;
		return rd;
	}
	
	/**
	 * <!-- begin-user-doc -->G3: Generate the random number for one individual.<!-- end-user-doc -->
	 */	
	public static int getRandomRate(LinkedHashMap<String, String> individual) {	
		int size = individual.size();
		//System.out.println ("size " + size);
		Random random = new Random();
		int randomRate = 0;
		if (size>0) {
			randomRate = random.nextInt(size)+1;
		}

		if ( size == 0) {
			randomRate = 1;
		}
		if (randomRate > 0) {
			randomRate = 1;
		}
		return randomRate;
	}
	
	public static int randomiseThis(int n) {	
		Random randomise = new Random();
		int randomiser = randomise.nextInt(n)+1; // FIXME: Review this, based on an error message " n must be positive".
		return randomiser;
	}
	
	// For randomising index only, based on the given size.
	public static int randomiseIndex(int n) {	
		Random randomise = new Random();
		int randomiser = randomise.nextInt(n-1);
		return randomiser;
	}
	
	public static int getSize(EObject obj) {
		EObject item;
		int count = 1;
		TreeIterator<EObject> tree = obj.eAllContents();
		while (tree.hasNext()) {
			count++;
			item = tree.next();
		}
		return count;
	}
	
	public static void savetheModel(EObject model, String filename) throws IOException {
		
		File offspringDir = new File("testfolder/" + filename + ".core");
		String path = offspringDir.getAbsolutePath();
		ResourceSet rSet = ModelLoader.getResSet();
		Resource myModel = rSet.createResource(URI.createFileURI(path));
		
		myModel.getContents().add(model);
		myModel.save(null);
	
	}
	
	/**
	 * <!-- begin-user-doc -->Returns the children of a model.<!-- end-user-doc -->
	 * @return the parent individual
	 */	
	public static EList<EObject> returnChildren(EObject rootModel) {	
		// check if the root node of the chromosome exist
		EList<EObject> parentObject = new BasicEList<EObject>();
		if (rootModel == null) {
			System.out.println("\n No Children Found.");
			return parentObject;
		
		}
		else {
			//EcoreUtil.Copier copier = new EcoreUtil.Copier(true, false);
			EObject rootChromosome = rootModel;
			//copier.copyReferences();
			// Make sure that parentIterator is initiated once only, at the start.
			// Reset chromosome.
			parentObject.add(0, rootChromosome);
			parentIterator = rootChromosome.eAllContents();
			EObject genePointer = parentIterator.next();
			while(parentIterator.hasNext()) {
				parentObject.add(genePointer);
				genePointer = parentIterator.next();
			}
		}
		return parentObject;
	}
	
	/**
	 * <!-- begin-user-doc -->Creates a list of predefined Annotations.<!-- end-user-doc -->
	 */
	public static String[] altAnnotations() {
		String[] annlistArray = new String[] {"C++","**","[[","((","||","??","@","%","&","$","#","!","~","+","=",":","<",">","123","-1","0"};
		return annlistArray;
	}	
	
	public static EObject[] getrootParents() {
		return rootParents;
	}
	
	public static int getParentsQty() {
		return ParentsQty;
	}
}