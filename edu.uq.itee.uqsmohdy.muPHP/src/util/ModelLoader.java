/**
 * 
 */
package util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.AbstractTreeIterator;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import IFML.Core.IFMLModel;
import IFML.Core.util.CoreResourceFactoryImpl;


/**
 * @author suhaila
 *
 */
public class ModelLoader {

	private static int individualsCtr;
	private static int areIndividualsloaded;
	private static ResourceSet rsParents;
	private static Resource resourceWhole;
	private static Resource[] resourceParents;
	private static EObject[] rootIndividuals;
	
	private static IFMLModel rootWhole;
	private static ResourceSet resSet;
	
	/**
	 * <!-- begin-user-doc -->Loads the general IFML model of the SUT.<!-- end-user-doc -->
	 * @throws IOException
	 */
	public static IFMLModel getWholemodel() throws IOException {
		System.out.println("Finding the general model here:");
		final File generalmodelDir = new File("generalmodel");
		final int wholeCtr = generalmodelDir.listFiles().length;

		// If the general model exist, get the resource for the general model.
		if (wholeCtr > 0) {

			final String s = new File("generalmodel/system.core").getAbsolutePath();
			System.out.println(s);
			final ResourceSet rcsWhole = getResourceSet();
			System.out.println(rcsWhole);
			resourceWhole = rcsWhole.createResource(URI.createFileURI(s));
			resourceWhole.load(null);

			// Get the root object of the general model.
			rootWhole = (IFMLModel) resourceWhole.getContents().get(0);
			System.out.println("\nGeneral model successfully loaded!");

			final AbstractTreeIterator<EObject> j = (AbstractTreeIterator<EObject>) rootWhole
					.eAllContents();
			final boolean childrenExistence = j.hasNext();
			
			// Check if the general model contains children elements.
			if (childrenExistence == false) {
				System.out.println("Error: The General Model is empty!");
				System.exit(0);
			}
		}
		else {
			System.out
					.println("\nError: Could not find the general model in the specified directory!  MutateIFML will stop.");
			System.exit(0);
		}

		return EcoreUtil.copy(rootWhole);
	}
	
	/**
	 * <!-- begin-user-doc -->L1 : Load the test case from "population".<!-- end-user-doc -->
	 * @throws IOException
	 */
	public static EObject[] loadIndividuals() throws IOException {
		System.out.println("\n --LOAD CANDIDATES--");

		final File candidatesDir = new File("population");
		final File[] candidateFiles = candidatesDir.listFiles();
		EObject[] rootIndividuals = null;
		final Resource[] resourceParents;
		final ResourceSet rsParents;

		int count = countIndividuals(candidateFiles);

		// If "population" directory contains individuals.
		if (count > 0) {

			// If more than two individuals exist, restrict count to only two individuals.
			if (count > 2) {
				count = 2;
			}

			System.out.println("\nFound " + count + " candidates.");
			// Initialise an array of String, p to contain the path to the individuals.
			final String[] p = new String[count];

			// Create arrays of EObject to store the root object of each individuals.
			rootIndividuals = new EObject[count];
			resourceParents = new Resource[count];
			rsParents = getResourceSet();

			for (int i = 0; i < count; i++) {
				//System.out.println(candidateFiles[i].getAbsolutePath());
				p[i] = candidateFiles[i].getAbsolutePath();
				//System.out.println(p[i]);
			}

			// Create the resource for each selected individuals.
			for (int x = 0; x < count; x++) {
				resourceParents[x] = rsParents.createResource(URI
						.createFileURI(p[x]));
				resourceParents[x].load(null);
				EcoreUtil.Copier copier = new EcoreUtil.Copier();
				rootIndividuals[x] = copier.copy(resourceParents[x].getContents().get(0));
				copier.copyReferences();
				//rootIndividuals[x] = resourceParents[x].getContents().get(0);
				//System.out.println("\n Candidate " + rootIndividuals[x]
				//		+ " successfully loaded.");
				
				areIndividualsloaded = 1;
			} // end for loop
		} // end if(count > 0)

		else {
			// Tell user no individuals found in the "population" folder.
			System.out.println("No candidate found.  Exiting..");
		}
		return rootIndividuals;
	}

	/**
	 * <!-- begin-user-doc -->Loads all candidates from the "population" directory.<!-- end-user-doc -->
	 * @throws IOException
	 */
	public static List<EObject> loadPopulations() throws IOException {

		final File populationDir = new File("population");
		final File[] populationFiles = populationDir.listFiles(new FilenameFilter() {
		    public boolean accept(File testDir, String fileName) {
		        return fileName.endsWith(".core");
		    }
		});
		List<EObject> roots = new ArrayList<EObject>();
		List<EObject> roots2 = new ArrayList<EObject>();
		final Resource[] populationResource;
		final ResourceSet populationRSet;

		int populationSize = countIndividuals(populationFiles);
		// If "population" folder contains individuals.
		if (populationSize > 0) {
			System.out.println("\nFound " + populationSize + " individuals in the initial population.\n");
			// Initialise an array of String, p to contain the path to the individuals.
			String[] indPath = new String[populationSize];

			// Create arrays of EObject to store the root object of each individuals.
			populationResource = new Resource[populationSize];
			populationRSet = getResourceSet();

			for (int i = 0; i < populationSize; i++) {
				//System.out.println(candidateFiles[i].getAbsolutePath());
				indPath[i] = populationFiles[i].getAbsolutePath();
				//System.out.println(indPath[i]);
			}
			// Create the resource for each selected individuals.
			for (int x = 0; x < populationSize; x++) {
				populationResource[x] = populationRSet.createResource(URI
						.createFileURI(indPath[x]));
				populationResource[x].load(null);
				roots.add(x, populationResource[x].getContents().get(0));
				roots2.add(x, EcoreUtil.copy(roots.get(x)));
				//System.out.println("Candidate " + roots.get(x)	+ " successfully loaded.\n");
				areIndividualsloaded = 1;
			} // end for loop
		} // end if(count > 0)

		else {
			// Inform user no individuals are found in the "population" folder.
			System.out.println("\nERROR: The population is empty. No candidate found.");
		}
		return roots2;
	}

	/**
	 * <!-- begin-user-doc -->Loads all candidates from the "ELITE" directory.<!-- end-user-doc -->
	 * @throws IOException
	 */
	public static List<EObject> loadElites() throws IOException {

		final File eliteDir = new File("elite");
		final File[] eliteFiles = eliteDir.listFiles(new FilenameFilter() {
		    public boolean accept(File testDir, String fileName) {
		        return fileName.endsWith(".core");
		    }
		});
		List<EObject> roots = new ArrayList<EObject>();
		List<EObject> roots2 = new ArrayList<EObject>();
		final Resource[] eliteResource;
		final ResourceSet eliteRSet;

		int eliteSize = countIndividuals(eliteFiles);
		// If "population" folder contains individuals.
		if (eliteSize > 0) {
			System.out.println("\nFound " + eliteSize + " candidates.\n");
			// Initialise an array of String, p to contain the path to the individuals.
			String[] indPath = new String[eliteSize];

			// Create arrays of EObject to store the root object of each individuals.
			eliteResource = new Resource[eliteSize];
			eliteRSet = getResourceSet();

			for (int i = 0; i < eliteSize; i++) {
				//System.out.println(candidateFiles[i].getAbsolutePath());
				indPath[i] = eliteFiles[i].getAbsolutePath();
				//System.out.println(indPath[i]);
			}
			// Create the resource for each selected individuals.
			for (int x = 0; x < eliteSize; x++) {
				eliteResource[x] = eliteRSet.createResource(URI
						.createFileURI(indPath[x]));
				eliteResource[x].load(null);
				roots.add(x, eliteResource[x].getContents().get(0));
				roots2.add(x, EcoreUtil.copy(roots.get(x)));
				System.out.println("Candidate " + roots.get(x)
						+ " successfully loaded.\n");
				areIndividualsloaded = 1;
			} // end for loop
		} // end if(count > 0)

		else {
			// Tell user no individuals found in the "population" folder.
			System.out.println("\nERROR: The population is empty. No candidate found.");
		}
		return roots2;
	}


	/**
	 * <!-- begin-user-doc -->G1: This method caches an instance of the model factory.<!-- end-user-doc -->
	 */
	private static ResourceSet getResourceSet() {
		//if (resSet  == null) {

			// Register the XMI resource factory for the .core extension.
			final Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
			final Map<String, Object> m = reg.getExtensionToFactoryMap();
			m.put("core", new CoreResourceFactoryImpl());
			//m.put("core", new CoreResourceFactoryImpl());
			// Construct resource set.
			resSet = new ResourceSetImpl();
		//}
		return resSet;
	}

	public static TreeIterator<EObject> loadUMLResource(Resource resourceUML) {
		
		// Register the XMI resource factory for the .uml extension.
		final Resource.Factory.Registry reg2 = Resource.Factory.Registry.INSTANCE;
		final Map<String, Object> m2 = reg2.getExtensionToFactoryMap();
		m2.put("uml", new CoreResourceFactoryImpl());

		
		ResourceSet resourceSetUML = new ResourceSetImpl();
		File umlFile = new File("generalmodel/model.uml");
		String umlPath = umlFile.getAbsolutePath();
		resourceUML = resourceSetUML.createResource(URI.createFileURI(umlPath));
		try {
			resourceUML.load(null);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		TreeIterator<EObject> eAllContents = resourceUML.getAllContents();
		return eAllContents;
	}
	
	/**
	 * <!-- begin-user-doc -->C1: This method counts the number of individuals in the target folder.<!-- end-user-doc -->
	 */
	public static int countIndividuals(File[] i) {
		individualsCtr = i.length;
		return individualsCtr;
	}
	
	/**
	 * <!-- begin-user-doc -->Getter and setter methods.<!-- end-user-doc -->
	 */
	public static Resource getResourceWhole() {
		return resourceWhole;
	}

	public static void setResourceWhole(Resource resourceWhole) {
		ModelLoader.resourceWhole = resourceWhole;
	}

	public static IFMLModel getRootWhole() {
		return rootWhole;
	}

	public static void setRootWhole(IFMLModel rootWhole) {
		ModelLoader.rootWhole = rootWhole;
	}
	
	public static EObject[] getrootIndividuals() {
		return rootIndividuals;
	}

	public static void setrootIndividuals(EObject[] rootIndividuals) {
		ModelLoader.rootIndividuals = rootIndividuals;
	}
	
	/**
	 * <!-- begin-user-doc -->Returns 1 if the individuals were already loaded, 0 if they weren't.<!-- end-user-doc -->
	 */
	public static int areIndividualsLoaded() {
		return areIndividualsloaded;
	}
	
	// TODO: Delete this method if it is not used.
	public static ResourceSet getResSet() {
		resSet = getResourceSet();
		return resSet;
	}
	
	public static Iterator<EObject> getGeneralModelIt() {
		EObject root = getRootWhole();
		Iterator<EObject> it = root.eAllContents();
		return it;
	}
	
}
