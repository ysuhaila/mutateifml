package printer;

/**
  * Initialize registries to support IFML usage. This method is intended for initialization of standalone behaviors for which plugin extension registrations have not been applied. <p>  A null resourceSet may be provided to initialize the global package registry and global URI mapping registry. <p>  A non-null resourceSet may be provided to identify specific package and global URI mapping registries. <p> This method is used to configure the ResourceSet used to load the OCL Standard Library.
  * @param resourceSet to be initialized or null for global initialization
  * @return a failure reason, null if successful
  * @since 3.0
  */

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import util.ModelLoader;
import IFML.Core.Annotation;
import IFML.Core.CorePackage;
import IFML.Core.InteractionFlow;
import IFML.Core.NamedElement;
import IFML.Extensions.Details;
import IFML.Extensions.List;
import IFML.Extensions.OnSelectEvent;

public class SeleniumPrinterResource {

	protected static File[] offspringFiles;
	protected static String FILE_EXT;
	protected static int existingoffsprings; 
	
	/**
	 * Load the test cases from the <i>population</i> directory.<!-- begin-user-doc -->
	 * @param existingoffsprings 
	 * @throws IOException
	 */
	public static EObject[] loadIFMLTestCase(int existingoffsprings) throws IOException {
		// Get the first model element and cast it to the right type, i.e.
		// everything is hierarchical included in the first node
		
		System.out.println("\n --LOAD OFFSPRINGS--");
		int count = existingoffsprings;

		final File offspringDir = new File("population/");
		offspringFiles = offspringDir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
		        return name.toLowerCase().endsWith(".core");
			}
		});
		EObject[] offspringsRoot = null;
		Resource[] resourceOffsprings;
		ResourceSet rsOffsprings;

		// If "population" folder contains offsprings.
		if (count > 0) {
			System.out.println("\nFound " + count + " offsprings.");
			// Initialise an array of String, op to contain the path to the individuals.
			String[] op = new String[count];

			// Create arrays of EObject to store the root object of each individuals.
			offspringsRoot = new EObject[count];
			resourceOffsprings = new Resource[count];
			rsOffsprings = ModelLoader.getResSet();

			for (int i = 0; i < count; i++) {
				op[i] = offspringFiles[i].getAbsolutePath();
				//System.out.println(op[i]);
			}

			// Create the resource for each selected individuals.
			for (int x = 0; x < count; x++) {
				resourceOffsprings[x] = rsOffsprings.createResource(URI
						.createFileURI(op[x]));
				resourceOffsprings[x].load(null);
				offspringsRoot[x] = resourceOffsprings[x].getContents().get(0);
				System.out.println("\nSuccesfully loaded candidate " + offspringsRoot[x]);
			} // end for loop
		} // end if(count > 0)

		else {
			// Tell user no individuals found in the "population" folder.
			System.out.println("No offspring found.");
		}
		return offspringsRoot;
	}
	
	// XXX Change here so that annotations for onSelectEvent wil be found in this method
	/**
	 * Transforms the <i>onSelectEvent</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param element the <i>onSelectEvent</i> IFML element
	 * @param annotation the annotation contained in the <i>onSelectEvent</i> IFML element
	 */
	public static void onSelectEventtoSelenese(EObject element, String annotation) {	
		String buttonOrLink = "";
		String htmlClick = "";
		
		if (annotation.equalsIgnoreCase("secondary")) {
			// If this link goes to the OpenBiblio Help page that opens in secondary window.
			buttonOrLink = ((NamedElement) element).getName().replaceAll("'", "");
			htmlClick = "\n<tr>\n\t<td>clickAndWait</td>\n\t<td>"
					+ buttonOrLink
					+ "</td>\n\t<td></td>\n</tr>\n"
					+ "<tr>\n<td>waitForPopUp</td>\n\t<td>secondary</td>\n\t<td>30000</td>\n</tr>";
		}
		else {
			buttonOrLink = ((NamedElement) element).getName().replaceAll("'", "");
			htmlClick = "\n<tr>\n\t<td>clickAndWait</td>\n\t<td>"
					+ buttonOrLink
					+ "</td>\n\t<td></td>\n</tr>";
		}
		try {
			SeleniumPrinterHelper.SeleneseWriter(htmlClick);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Transforms the <i>Form</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param element the <i>Form</i> IFML element
	 * @param parentElement the container element
	 */
	public static void formtoSelenese(EObject element, EObject parentElement) {
		String windowName = ((NamedElement) parentElement).getName();
		String formName = ((NamedElement) element).getName();
		String htmlForm = "<tr>\n\t<td>verifyElementPresent</td>\n\t<td>//form[1]</td>\n\t<td>"
				+ windowName + "</td>\n</tr>\n";
	}
	
	/**
	 * Transforms the <i>SimpleField</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param element the <i>SimpleField</i> IFML element
	 * @param addition2 additional information
	 */
	public static void simpleFieldtoSelenese(EObject element, String addition2) {
		String simplefield = ((NamedElement) element).getName();
		String htmlsimpleField = "";
		if (!(addition2.equalsIgnoreCase("optional"))) {
			htmlsimpleField = "<tr>\n\t<td>type</td>\n\t<td>"
					+ simplefield + "</td>\n\t<td>" + addition2 
					+ "</td>\n</tr>\n";
			try {
				SeleniumPrinterHelper.SeleneseWriter(htmlsimpleField);
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Transforms the <i>SimpleField</i>'s <i>Annotation</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param element the <i>SimpleField</i>'s <i>Annotation</i> IFML element
	 */
	public static void fieldAnnotationtoSelenese(EObject element) {
		String simplefieldAnn = ((Annotation) element).getText();
		String htmlsimpleFieldAnn = "<td>"
				+ simplefieldAnn
				+ "</td>\n</tr>\n";
		try {
			SeleniumPrinterHelper.SeleneseWriter(htmlsimpleFieldAnn);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Transforms the <i>SimpleField</i>'s <i>Annotation</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param element the <i>SimpleField</i>'s <i>Annotation</i> IFML element
	 */
	public static void fieldAnnotationtoSelenese2(EObject element) {
		String fieldName = ((NamedElement) element).getName();
		String htmlsimpleFieldAnn = "\t<td></td>\n</tr>\n";
		try {
			SeleniumPrinterHelper.SeleneseWriter(htmlsimpleFieldAnn);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("WARNING: Cannot find the value for this field: "
				+ fieldName);
	}
	
	/**
	 * Transforms the <i>Annotation</i> elements inside the <i>List</i> or <i>Details</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param obj the <i>List</i> or <i>Details</i> IFML element
	 * @param listOrdetailsAnn the <i>List</i> or <i>Details</i>' <i>Annotation</i> IFML element
	 */
	public static void listOrdetailsAnnotationtoSelenese(EObject obj,
			String annData) {
		String htmllistOrdetailsName = "";
		String htmllistOrdetails = "";
		String notNullAssertion = "";

		if (obj instanceof List) {
			htmllistOrdetails = "\n<tr>\n\t<td>verifyText</td>"
					+ "\n\t<td>link=regexpi:..\\s" + annData + "\\s.</td>\n\t<td>"
					+ "regexpi:..\\s" + annData + "\\s.</td>\n</tr>";
			notNullAssertion = "\n<tr>\n\t<td>verifyNotText</td>"
					+ "\n\t<td>link=regexpi:\\A\\s</td>\n\t<td>regexpi:\\A\\s"
					+ "</td>\n</tr>";
		}
		else if (obj instanceof Details) {
			htmllistOrdetailsName = ((NamedElement)obj).getName();
			htmllistOrdetails = "\n<tr>\n\t<td>verifyElementPresent</td>"
					+ "\n\t<td>" + htmllistOrdetailsName + "</td>\n\t<td></td>\n</tr>";
		}

		if (htmllistOrdetails.length() != 0) {
			try {
				SeleniumPrinterHelper.SeleneseWriter(htmllistOrdetails);
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (notNullAssertion.length() != 0) {
			try {
				SeleniumPrinterHelper.SeleneseWriter(notNullAssertion);
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	
	/**
	 * Transforms the <i>SelectionField</i>'s <i>Annotation</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param element the <i>SelectionField</i>'s <i>Annotation</i> IFML element
	 */
	public static String getselectionAnnotation(EObject element) {
		String addText = ((Annotation) element).getText();
		return addText;
	}
	
	/**
	 * Transforms the <i>SimpleField</i>'s <i>Annotation</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param element the <i>SimpleField</i>'s <i>Annotation</i> IFML element
	 */
	public static String getfieldAnnotation(EObject element) {
		String addText = ((Annotation) element).getText();
		return addText;
	}
	
	/**
	 * Transforms the <i>SelectionField</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param element the <i>SelectionField</i> IFML element
	 * @param addition additional information
	 */
	public static void selectionFieldtoSelenese(EObject element, String addition) {
		String selectionField = ((NamedElement) element).getName();
		String htmlselectionField = "";
		if (addition.equalsIgnoreCase("click")) {
			htmlselectionField = "<tr>\n\t<td>" + addition + "</td>\n\t<td>"
					+ selectionField
					+ "</td>\n\t<td></td>\n</tr>\n";
		}
		else {
			htmlselectionField = "<tr>\n\t<td>select</td>\n\t<td>"
					+ selectionField
					+ "</td>\n\t<td>" + addition + "</td>\n</tr>\n";
		}
		
		try {
			SeleniumPrinterHelper.SeleneseWriter(htmlselectionField);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Transforms the <i>NavigationFlow</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param element the <i>NavigationFlow</i> IFML element
	 * @param annotation the <i>NavigationFlow</i>'s <i>Annotation</i> IFML element
	 */
	public static void outFlowtoSelenese(EObject element, String annotation) {	
		String buttonOrLink = "";
		String targetFlow = "";
		String htmlClick = "";
		
		if (annotation.equalsIgnoreCase("secondary")) {
			// If this link goes to the OpenBiblio Help page that opens in secondary window.
			if (element instanceof OnSelectEvent) {
				buttonOrLink = ((NamedElement) element).getName().replaceAll("'", "");
				if (((OnSelectEvent) element).getInInteractionFlows().size() == 1) {
					InteractionFlow iflow = (InteractionFlow) ((OnSelectEvent) element).getInInteractionFlows().get(0);
					if (iflow.eIsSet(CorePackage.eINSTANCE.getInteractionFlow_TargetInteractionFlowElement())) {
						EObject targetWindow = ((InteractionFlow)iflow).getTargetInteractionFlowElement();
						targetFlow = ((NamedElement)targetWindow).getName().replaceAll("'", "");
						if (buttonOrLink.contains("link") && !targetFlow.contains("Biblio Info") && !targetFlow.contains("Bibliography Description") && !targetFlow.contains("Member Info") && !buttonOrLink.contains("Print")) {
							buttonOrLink = "link=" + targetFlow;
						}
					}
				}
			}
			htmlClick = "\n<tr>\n\t<td>clickAndWait</td>\n\t<td>"
					+ buttonOrLink
					+ "</td>\n\t<td></td>\n</tr>\n"
					+ "<tr>\n<td>waitForPopUp</td>\n\t<td>secondary</td>\n\t<td>30000</td>\n</tr>";
		}
		else {
			// XXX Jim's code suggestion
			if (element instanceof OnSelectEvent) {
				buttonOrLink = ((NamedElement) element).getName().replaceAll("'", "");
				int isOutFlow = ((OnSelectEvent) element).getOutInteractionFlows().size();
				String out =  ((OnSelectEvent) element).getOutInteractionFlows().toString();
				if (isOutFlow == 1) {
					InteractionFlow iflow = (InteractionFlow) ((OnSelectEvent) element).getOutInteractionFlows().get(0);
					if (iflow.eIsSet(CorePackage.eINSTANCE.getInteractionFlow_TargetInteractionFlowElement())) {
						EObject targetWindow = ((InteractionFlow)iflow).getTargetInteractionFlowElement();
						targetFlow = ((NamedElement)targetWindow).getName().replaceAll("'", "");
						// if the OnSelectEvent contains certain terms, replace the contents of buttonOrLink with the specified string
						if (buttonOrLink.contains("link") && !buttonOrLink.contains("Bibliography Search") && !targetFlow.contains("Biblio Info") && !targetFlow.contains("Bibliography Description") && !targetFlow.contains("Member Info") && !buttonOrLink.contains("Print")) {
							buttonOrLink = "link=" + targetFlow;
						}
					}
				}
			}
			htmlClick = "\n<tr>\n\t<td>clickAndWait</td>\n\t<td>"
					+ buttonOrLink
					+ "</td>\n\t<td></td>\n</tr>";
		}
		try {
			SeleniumPrinterHelper.SeleneseWriter(htmlClick);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	// TODO: Figure out how to allow the submit button to dynamically detect the value to print based on the target interaction flow.
	// Refer to the onSelectEvent printer method
	/**
	 * Transforms the <i>SubmitButton</i> IFML element into <i>Selenese</i>.<!-- begin-user-doc -->
	 * @param element the <i>SubmitButton</i> IFML element
	 */
	public static void submitButtontoSelenese(EObject element) {
		String submitbtn = ((NamedElement) element).getName().replace("\\", "");
		String htmlClick = "\n<tr>\n\t<td>clickAndWait</td>\n\t<td>"
				+ submitbtn
				+ "</td>\n\t<td></td>\n</tr>";

		try {
			SeleniumPrinterHelper.SeleneseWriter(htmlClick);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Starts the process of transforming all IFML Models <!-- begin-user-doc -->
	 * into Selenium test cases
	 * @throws IOException
	 */
	public static void printAll() throws IOException {
		FILE_EXT = ".core";
		//String[] fileNames = new File("population/").list();
		File[] coreFiles = new File("population/").listFiles(new FilenameFilter() {
		    public boolean accept(File testDir, String fileName) {
		        return fileName.endsWith(".core");
		    }
		});

		// get the number of offsprings from the "output" folder.	
		existingoffsprings = coreFiles.length;
		String[] fileNames = new String[existingoffsprings];
		int i = 0;
		for (File f : coreFiles) {
			fileNames[i] = f.getName().replaceAll(".core", "");
			i++;
		}

		for (int a=0; a<existingoffsprings; a++) {
			// Create the test case
//			String fName = fileNames[a];
//			SeleniumPrinterResource.createSeleniumTestCase(fName);
//			// Print the header
//			SeleniumPrinterResource.SeleneseheaderWriter();
//			String baseURL = getBaseURL(a);
//			printBaseURL(baseURL);
//			printTitle(a);
//			SeleniumPrinter.getOpenURL(a);
//			SeleniumPrinter.getAllelement(a);
//			SeleniumPrinterResource.SelenesefooterWriter();
//			// close the Printer.
//			SeleniumPrinterResource.CloseSeleneseWriter();
		}
	}
	
}