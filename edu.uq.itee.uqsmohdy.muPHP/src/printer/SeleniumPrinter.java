package printer;

/**
  * Initialize registries to support IFML usage. This method is intended for initialization of standalone behaviors for which plugin extension registrations have not been applied. <p>  A null resourceSet may be provided to initialize the global package registry and global URI mapping registry. <p>  A non-null resourceSet may be provided to identify specific package and global URI mapping registries. <p> This method is used to configure the ResourceSet used to load the OCL Standard Library.
  * @param resourceSet to be initialized or null for global initialization
  * @return a failure reason, null if successful
  * @since 3.0
  */

import java.io.IOException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import IFML.Core.Annotation;
import IFML.Core.DomainModel;
import IFML.Core.IFMLModel;
import IFML.Core.UMLDomainConcept;
import IFML.Core.UMLStructuralFeature;
import IFML.Core.ViewComponent;
import IFML.Extensions.Details;
import IFML.Extensions.Form;
import IFML.Extensions.IFMLWindow;
import IFML.Extensions.List;
import IFML.Extensions.OnSelectEvent;
import IFML.Extensions.OnSubmitEvent;
import IFML.Extensions.SelectionField;
import IFML.Extensions.SimpleField;

public class SeleniumPrinter {

	protected static EObject[] modelRoot;

	/**
	 * <!-- begin-user-doc -->Starts the process of transforming an IFML Model
	 * into a Selenium test case.<!-- end-user-doc -->
	 * @param modelRoot the root object of the IFML Models
	 * @param id the position of the IFML Model
	 * @param fileName the filename of the IFML Model
	 * @throws IOException
	 */
	public static void printOne(EObject[] modelRoot, int id, String fileName) throws IOException {
			
		// Create the test case file.
		SeleniumPrinterHelper.createSeleniumTestCase(fileName);
		// Print the header.
		SeleniumPrinterHelper.SeleneseheaderWriter();
		printBaseURL(modelRoot, id);
		printTitle(modelRoot, id);
		getOpenURL(modelRoot, id);
		getAllelement(id);
		SeleniumPrinterHelper.SelenesefooterWriter();
		// Close the printer.
		SeleniumPrinterHelper.CloseSeleneseWriter();
	}
	
	/**
	 * <!-- begin-user-doc -->Starts the process of transforming all IFML models
	 * into Selenium test cases.<!-- end-user-doc -->
	 * @param modelRoot the root object of the IFML Models
	 * @param id the position of the IFML Model
	 * @param fileName the filename of the IFML Model
	 * @throws IOException
	 */
	public static void printAll(EList<EObject> collection) throws IOException {
		int id = 0;
		
		for (EObject op:collection) {
			String name = ((IFMLModel)op).getName();
			// Create the test case file.
			SeleniumPrinterHelper.createSeleniumTestCase(name);
			// Print the header.
			SeleniumPrinterHelper.SeleneseheaderWriter();
			printBaseURL(modelRoot, id);
			printTitle(modelRoot, id);
			getOpenURL(modelRoot, id);
			getAllelement(id);
			SeleniumPrinterHelper.SelenesefooterWriter();
			// Close the printer.
			SeleniumPrinterHelper.CloseSeleneseWriter();
		}
		
	}
	
	/**
	 * <!-- begin-user-doc -->Prints the URL.<!-- end-user-doc -->
	 * @throws IOException
	 */
	public static void printBaseURL(EObject[] modelRoot, int id) throws IOException {
		// Load and retrieve the root object of the model first.
		
		String baseURL = getBaseURL(modelRoot, id);
		String seleniumlink = "<link rel=\"selenium.base\" href=\"" + baseURL
				+ "\" />\n";
		SeleniumPrinterHelper.SeleneseWriter(seleniumlink);
	}	

	/**
	 * <!-- begin-user-doc -->Retrieve the base URL for the test case.<!-- end-user-doc -->
	 * @param modelRoot the root object of the IFML Models
	 * @param index the position of the selected IFML Model
	 * @throws IOException
	 */
	public static String getBaseURL(EObject[] modelRoot, int index) throws IOException {
		// Load and retrieve the root object of the model first.
		String baseURL = null;
		
		TreeIterator<EObject> it = (TreeIterator<EObject>) modelRoot[index].eAllContents();
		if (modelRoot[index] instanceof IFMLModel) {
			final EObject base = it.next();
			if (base instanceof Annotation) {
				baseURL = ((Annotation) base).getText();
				// For demo purposes with external host.
				baseURL = "http://";
			}
			else {
				//baseURL = "http://localhost/";
				baseURL = "http://";
			}
		}
		return baseURL;
	}	

	/**
	 * <!-- begin-user-doc -->Prints the title of the test case.<!-- end-user-doc -->
	 * @param index the position of the IFML Model's title
	 * @throws IOException
	 */
	public static void printTitle(EObject[] modelRoot, int index) throws IOException {
		String htmltitle = null;
		String htmlbody = null;
		htmltitle = "<title>" + ((IFMLModel)modelRoot[index]).getName() + "</title>\n</head>\n";
		if(htmltitle != null && !htmltitle.isEmpty()) {
			SeleniumPrinterHelper.SeleneseWriter(htmltitle);
		}
		htmlbody = "<body>\n<table cellpadding=\"1\" cellspacing=\"1\" border=\"1\">\n<thead>\n<tr><td rowspan=\"1\" colspan=\"3\">"
				+ ((IFMLModel)modelRoot[index]).getName() + "</td></tr>\n</thead><tbody>\n";
		if(htmlbody != null && !htmlbody.isEmpty()) {
			SeleniumPrinterHelper.SeleneseWriter(htmlbody);
		}
	}	

	/**
	 * <!-- begin-user-doc -->Returns the URL for Selenium "Open" command from the test case.<!-- end-user-doc -->
	 * @param modelRoot the root object of the IFML Models
	 * @param index the position of the selected IFML Model
	 */
	public static void getOpenURL(EObject[] mRoot, int index) throws IOException {

		EObject openUrl = null;
		String testcaseURL = null;
		String alternateURL = "~suhaila/testobib/home/index.php";
		String htmlOpen = null;
		
		// Load and retrieve the root object of the model first.
		modelRoot = mRoot;	
		TreeIterator<EObject> it = (TreeIterator<EObject>) modelRoot[index].eAllContents();
		EObject objPointer = it.next();
		// Find the first IFML Window.
		while (objPointer instanceof IFMLWindow == false) {
			objPointer = it.next();
		}
		
		while (it.hasNext()) {
			if (objPointer instanceof IFMLWindow
					&& ((IFMLWindow) objPointer).isIsDefault()) {
				openUrl = it.next();
				if (openUrl instanceof Annotation) {
					testcaseURL = ((Annotation) openUrl).getText();
					// Get the remaining URL of the test case and print it out.
					htmlOpen = "<tr>\n\t<td>open</td>\n\t<td>" + testcaseURL
							+ "</td>\n\t<td></td>\n</tr>\n";
					SeleniumPrinterHelper.SeleneseWriter(htmlOpen);
					break;
				}
				else {
					htmlOpen = "<tr>\n\t<td>open</td>\n\t<td>" + alternateURL + "</td>\n\t<td></td>\n</tr>\n";
					System.out
					.println("WARNING: Cannot find the URL of this test case while ");
					SeleniumPrinterHelper.SeleneseWriter(htmlOpen);
					break;
				}
			}
			objPointer = it.next();
		}
	}

	/**
	 * <!-- begin-user-doc -->Returns the content of test case.<!-- end-user-doc -->
	 * @param index the position of the test case
	 */
	public static void getAllelement(int index) throws IOException {	

		TreeIterator<EObject> opIt = (TreeIterator<EObject>) modelRoot[index].eAllContents();
		
		EObject objPointer = opIt.next();
		EObject windowMarker = null;
		EObject saveButton = null;
		//System.out.println("FIRST: "+objPointer);
		//System.out.println("MARKER: "+windowMarker);
 
		// Finds the first IFMLWindow.
		while (!(objPointer instanceof IFMLWindow)) {
			objPointer = opIt.next();
		}
		windowMarker = objPointer;
		
		// Traverse through all element. current=IFMLWindow
		while (!(objPointer instanceof DomainModel) && !(objPointer instanceof UMLDomainConcept) && !(objPointer instanceof UMLStructuralFeature)) {
			// Find IFMLWindow.
			if (((IFMLWindow) windowMarker).isIsDefault()) {
				// Window level.
				do {
					if (opIt.hasNext()) objPointer = opIt.next();
					// Find the button first in current Window if it exists.
					if (objPointer instanceof ViewComponent && !(objPointer instanceof Form) && !(objPointer instanceof Details) && !(objPointer instanceof List)) {
						String viewComponentAnnotation = "";
						EList<EObject> viewComponent = objPointer.eContents();
						for (EObject obj : viewComponent) {
							if (obj instanceof Annotation) {
								viewComponentAnnotation = ((Annotation)obj).getText();
								if (opIt.hasNext()) objPointer = opIt.next();
							}
							else if (obj instanceof OnSelectEvent) {
								//TODO Add assert here if possible.
								SeleniumPrinterResource.outFlowtoSelenese(obj, viewComponentAnnotation);
								if (opIt.hasNext()) objPointer = opIt.next();
							}
						}
					}
					else if (objPointer instanceof List || objPointer instanceof Details) {
						TreeIterator<EObject> list = objPointer.eAllContents();
						while (list.hasNext()) {
							EObject obj = list.next();
							if (obj instanceof OnSelectEvent) {
								//TODO add assert if feasible
								SeleniumPrinterResource.outFlowtoSelenese(obj, "");
								if (opIt.hasNext()) objPointer = opIt.next();
							}
							else if (obj instanceof Annotation) {
								String listOrdetailsAnn = ((Annotation)obj).getText();
								SeleniumPrinterResource.listOrdetailsAnnotationtoSelenese(objPointer, listOrdetailsAnn);
								if (opIt.hasNext()) objPointer = opIt.next();
							}
						}
						
					}
					// Find form if navigation button does not exist.
					else if (objPointer instanceof Form) {
						// Add assert for Form.
						SeleniumPrinterResource.formtoSelenese(objPointer, windowMarker);
						// Retrieve child elements of Form.
						EList<EObject> formChildren = objPointer.eContents();
						for (EObject obj : formChildren) {
							if (obj instanceof OnSubmitEvent) {
								// If the submit button is found first, save the submit button to use later.
								saveButton = obj;
							}
							else if (obj instanceof SimpleField) {
								String addThis2 = "";
								// If the field has input value attached to it, find it in its annotation.
								EList<EObject> fieldChildren = obj.eContents();
								if (!fieldChildren.isEmpty()) {
									for (EObject item: fieldChildren) {
										if (item instanceof Annotation) {
											addThis2 = SeleniumPrinterResource.getfieldAnnotation(item);
										}
									}
								}
								SeleniumPrinterResource.simpleFieldtoSelenese(obj, addThis2);

							}
							else if (obj instanceof SelectionField) {
								String addThis = "";
								// If the field has input value attached to it, find it in its annotation.
								EList<EObject> selectionChildren = obj.eContents();
								if (!selectionChildren.isEmpty()) {
									for (EObject item: selectionChildren) {
										if (item instanceof Annotation) {
											addThis = SeleniumPrinterResource.getselectionAnnotation(item);
										}
									}
								}
								SeleniumPrinterResource.selectionFieldtoSelenese(obj, addThis);
							}
						}
						// Lastly, print the submit button.
						SeleniumPrinterResource.submitButtontoSelenese(saveButton);
						if (opIt.hasNext()) objPointer = opIt.next();
					} // End else if Form.
				} while (!(objPointer instanceof IFMLWindow));
				// If next IFMLWindow is found, update the windowMarker.
				windowMarker = objPointer;
			}
			// If the current IFMLWindow is not a default.
			else if (objPointer instanceof IFMLWindow) {
				do {
					if (opIt.hasNext()) objPointer = opIt.next();
					// Find the button first in current Window if it exists.
					if (objPointer instanceof ViewComponent && !(objPointer instanceof Form) && !(objPointer instanceof Details) && !(objPointer instanceof List)) {
						String viewComponentAnnotation = "";
						EList<EObject> viewComponent = objPointer.eContents();
						for (EObject obj : viewComponent) {
							if (obj instanceof Annotation) {
								viewComponentAnnotation = ((Annotation)obj).getText();
								if (opIt.hasNext()) objPointer = opIt.next();
							}
							else if (obj instanceof OnSelectEvent) {
								//TODO Add assert here if possible
								SeleniumPrinterResource.outFlowtoSelenese(obj, viewComponentAnnotation);
								if (opIt.hasNext()) objPointer = opIt.next();
							}
						}
					}
					else if (objPointer instanceof List || objPointer instanceof Details) {
						TreeIterator<EObject> list = objPointer.eAllContents();
						while (list.hasNext()) {
							EObject obj = list.next();
							if (obj instanceof OnSelectEvent) {
								//TODO Add assert here if possible
								SeleniumPrinterResource.outFlowtoSelenese(obj, "");
								if (opIt.hasNext()) objPointer = opIt.next();
							}
							else if (obj instanceof Annotation) {
								String listOrdetailsAnn = ((Annotation)obj).getText();
								SeleniumPrinterResource.listOrdetailsAnnotationtoSelenese(objPointer, listOrdetailsAnn);
								if (opIt.hasNext()) objPointer = opIt.next();
							}
						}
						
					}
					// Find form if navigation button does not exist.
					else if (objPointer instanceof Form) {
						// Add assert for Form here if possible.
						SeleniumPrinterResource.formtoSelenese(objPointer, windowMarker);
						// Retrieve the child elements of Form.
						EList<EObject> formChildren = objPointer.eContents();
						for (EObject obj : formChildren) {
							if (obj instanceof OnSubmitEvent) {
								// If the submit button is found first, save the submit button to use later.
								saveButton = obj;
							}
							else if (obj instanceof SimpleField) {
								String addThis2 = "";
								// If the field has input value attached to it, find it in its annotation.
								EList<EObject> fieldChildren = obj.eContents();
								if (!fieldChildren.isEmpty()) {
									for (EObject item: fieldChildren) {
										if (item instanceof Annotation) {
											addThis2 = SeleniumPrinterResource.getfieldAnnotation(item);
										}
									}
								}
								SeleniumPrinterResource.simpleFieldtoSelenese(obj, addThis2);
							}
							else if (obj instanceof SelectionField) {
								String addThis = "";
								// If the field has input value attached to it, find it in its annotation.
								EList<EObject> selectionChildren = obj.eContents();
								if (!selectionChildren.isEmpty()) {
									for (EObject item: selectionChildren) {
										if (item instanceof Annotation) {
											addThis = SeleniumPrinterResource.getselectionAnnotation(item);
										}
									}
								}
								SeleniumPrinterResource.selectionFieldtoSelenese(obj, addThis);
							}
						}
						// Lastly, print the submit button.
						SeleniumPrinterResource.submitButtontoSelenese(saveButton);
						if (opIt.hasNext()) objPointer = opIt.next();
					} // End else if Form.
				} while (!(objPointer instanceof IFMLWindow) && !(objPointer instanceof DomainModel) && !(objPointer instanceof UMLDomainConcept) && !(objPointer instanceof UMLStructuralFeature));
				// If next IFMLWindow is found, update the windowMarker.
				windowMarker = objPointer;
			}
		} // End objPointer instanceof DomainModel !=true)
	}
	
}
