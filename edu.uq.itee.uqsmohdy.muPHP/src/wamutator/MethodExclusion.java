package wamutator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;



public class MethodExclusion {
	
	public static void excludeMethod(File base, File srcDir, String source, String folder, int index) throws IOException {
		
		int count = 0;
		File destDir = null;
		// this works by reading line by line. It is efficient in memory handling, but less effective for some mutation operator
		LineIterator baseIt = FileUtils.lineIterator(base, "UTF-8");
		try {
		    while (baseIt.hasNext()) {

		        String line = baseIt.nextLine();
		        // create copies based on count
		        if (line.contains("()") && line.contains("->") && !line.contains("if") && !line.contains("elseif") && !line.contains("foreach")) {
		        	count++;
		        	destDir = new File(source + "mtSME" + count );
		        	FileUtils.copyDirectory(srcDir, destDir);
		        }
		    }
		} finally {
		    LineIterator.closeQuietly(baseIt);
		}
	
		//
		File [] targets = new File (destDir + "/" + folder).listFiles();
		File target = targets[index];
		int i = 1;
			
		LineIterator fileIt = FileUtils.lineIterator(target, "UTF-8");
		try {
		    while (fileIt.hasNext()) {
		    	
		        String line = fileIt.nextLine();
		        // do something with line
		        // TODO: (1)Based on the list of mutantSUT created above, sequentially analyse the same file in each mutantSUT, and mutate each line that fulfils the condition below. 
		        if (line.contains("()") && line.contains("->") && !line.contains("if") && !line.contains("elseif") && !line.contains("foreach")) {
			    	
		        	line = "//" + line + "MUTANT";
		        }
				System.out.println("here");
				i++;
		    }
		} finally {
		    LineIterator.closeQuietly(fileIt);
		}
	
	
	}
}