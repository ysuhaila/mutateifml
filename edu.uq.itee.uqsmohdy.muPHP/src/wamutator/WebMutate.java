package wamutator;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.FileArrayProvider;

public class WebMutate {
	
	/**
	 * Starts the Web Mutator <!-- begin-user-doc -->
	 * this is for Mac version
	 * @throws IOException
	 */
	public static void mutateWeb() throws IOException {
	
		// Retrieve the list of directory to mutate
		String source = "/Users/suhaila/Sites/";
		String [] targetDir = FileArrayProvider.readLines("configuration/target.txt");
		String [] includeList = FileArrayProvider.readLines("configuration/include list.txt");
	
		Charset charset = StandardCharsets.UTF_8;
		
		//set limit to the number of mutants (Temporary)
		int max = 3;
		 
		for (int i = 0; i < max; i++) {		
			// set the directory first
			File srcDir = new File(targetDir[0]);
			
			// locate the folder to mutate and count the number of files in the folder
			int j = 0;
			while (j < includeList.length) {
				//String targetFile = destDir + "/" + includeList[j];
				File [] targets = new File(srcDir + "/" + includeList[j]).listFiles();
				
				//based on the number of files in the folder, mutate these files	
				int limit = targets.length;
				int k = 0;
				while (k < limit) {
					
					MethodExclusion.excludeMethod(targets[k], srcDir, source, includeList[j], k);
					
					k++;
				}

				
				j++;
			}
			
			

			
			
			
			
		}
		System.out.println("hurrah");
		//Sample
		//String content = new String(Files.readAllBytes(path), charset);
		//content = content.replaceAll("foo", "bar");
		//Files.write(path, content.getBytes(charset));
		
			
	}
	
	
}