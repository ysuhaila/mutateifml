package ifmlmutator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import util.ModelLoader;
import util.MutationHelper;
import util.Shared;
import IFML.Core.IFMLModel;
import IFML.Core.InteractionFlowModel;
import IFML.Core.NavigationFlow;
import IFML.Extensions.IFMLWindow;

public class DeleteMutation {

	/**
	 * Delete the last element of the offspring <!-- begin-user-doc -->
	 * 
	 * @param chromosome the parent individual
	 * @param position the position for performing the delete mutation
	 * @return the offspring
	 */
	
	public static EObject deleteMutation(EObject chromosome, EList<EObject> chromosomeClone) {
		String windowId = null;
		int deleteIndex = 0;
		List<String> windowList = new ArrayList();
		EList<EObject> chromosomeCopy = new BasicEList<EObject>();
		// position must be more than 2 to avoid deleting the default IFML Window.
		int deleteIndex2 = Shared.numberofWindows(chromosomeClone) - 1;

		// TODO 140116 Currently this deletes the last Window only. Refine so that it randomly picks the index for deletion.
		// Dynamic deletion requires: 1)the name of the next IFML Window after the delete position, nextWindow 
		// 2)Checking with the general model whether the IFML Window before the delete position has valid flow to the nextWindow
		if (deleteIndex2 < 3) {
			System.out
					.println("The delete operation could not be performed on this candidate.");
			chromosome = null;
			return chromosome;
		}

		// saves list of windows
		for (EObject target : chromosomeClone) {
			if (target instanceof IFMLWindow) {
				windowList.add(((IFMLWindow) target).getName());
			}
		}
		
		//Change the name of the candidate to update its ancestry
		if (chromosome instanceof IFMLModel) {
			String ancestry = ((IFMLModel)chromosome).getName();
			((IFMLModel)chromosome).setName(ancestry+"-DM");
		}
		
		// iterate through the chromosome to find the IFML Windows' container
		TreeIterator<EObject> candidateIt = chromosome.eAllContents();
		EObject browser = candidateIt.next();
		while (! (browser instanceof InteractionFlowModel)){
			browser = candidateIt.next();
		}
		
		// iterate the Interaction Flow Model Elements
		EList<EObject> contents = ((InteractionFlowModel)browser).eContents();
		EObject targetGene = contents.get(deleteIndex2);
		// perform the deletion
		EcoreUtil.delete(targetGene);
		
		// find the last Navigation Flow
		TreeIterator<EObject> windowIt = contents.get(deleteIndex2-1).eAllContents();
		EObject outFlow = windowIt.next();
		while (windowIt.hasNext() && !(outFlow instanceof NavigationFlow)) {
			outFlow = windowIt.next();
		}
		
		// clear the last Navigation Flow that initially contained the deleted gene as the target interaction flow
		//((NavigationFlow)outFlow).setTargetInteractionFlowElement(null);
		EcoreUtil.delete(outFlow);
		
		return chromosome;
	}

	//traverse through general model to find broken link. TODO : This is broken right now.
	private static EList<EObject> refineModel(EList<EObject> chromosome, List<String> windowList, int listSize) {
		EObject genRoot = ModelLoader.getRootWhole();
		EcoreUtil.Copier copier = new EcoreUtil.Copier();
		EObject sysRoot = copier.copy(genRoot);
		copier.copyReferences();

		int idx = 0;
		int yes = 0;

		TreeIterator<EObject> sysIt = sysRoot.eAllContents();
		EObject obj = sysIt.next();
		while (sysIt.hasNext()) {
			//if (!(obj instanceof IFMLWindow)) {
			if (!(obj instanceof IFMLWindow)) {
				obj = sysIt.next();
			}
			else {
				String tempWindow = ((IFMLWindow) obj).getName();
				String window = windowList.get(idx);
				if (window.contains(tempWindow)) {
					// if match, determine that the navigation flow is valid.
					TreeIterator<EObject> children = obj.eAllContents();
					EObject k = children.next();
					while (children.hasNext()) {

						if (k instanceof NavigationFlow) {
							String out = ((NavigationFlow) k)
									.getTargetInteractionFlowElement()
									.getName();
							if (windowList.get(idx + 1).contains(out)) {
								yes = 1;
								break;
							}
							else {
								k = children.next();
							}
						}
						else {
							k = children.next();
						}
					}
					if (yes != 1) {
						windowList.remove(idx + 1);
					}

				}
			}
		}
		int max = windowList.size();
		idx = 0;
		int mark = 0;
		for (EObject obj2 : chromosome) {
			if (obj2 instanceof IFMLWindow) {
				String winName = ((IFMLWindow) obj2).getName();
				// delete if name does not exist in list
				if (idx != max) {
					if (winName.contains(windowList.get(idx))) {
						idx = idx + 1;
					}
				}
				else {
					mark = chromosome.indexOf(obj2);
					break;
				}
			}
		}

		chromosome.subList(0, mark);

		return chromosome;
	}

	/**
	 * D1: Find similar offspring in General Model <!-- begin-user-doc -->
	 * 1. Use Hashmap for Parent 1 (parent1Map) and Parent 2 (parent2Map)
	 * 2. Delete the last Window if alternative offspring exist in General Model
	 */
	public static List<HashMap<String, String>> deleteMutation2(
			final LinkedHashMap<String, String> P, int done) {
		// Requirement: HashMap of an individual, the root element of the general model.
		// Traverse the General Map

		System.out
				.println("::::::::::::::::::::\n  DELETE MUTATION  \n  FINDING SIMILAR OFFSPRING IN THE GENERAL MODEL");
		System.out.println("::::::::::::::::::::");

		List<HashMap<String, String>> matchedMap;
		final IFMLModel genRoot = ModelLoader.getRootWhole();
		final TreeIterator<EObject> GenModelIt = genRoot.eAllContents();
		GenModelIt.next();
		;

		final int PSize = P.size();
		int counter = 0;

		Shared.printLinkedMap(P);

		// finding the parent individual in the general model.
		for (final Map.Entry<String, String> entry1 : P.entrySet()) {
			if (counter < PSize - 1) {
				final String key1 = entry1.getKey();
				final String value1 = entry1.getValue();
				//System.out.println("\n:::: SEARCH BEGINS ::::\n");
				//System.out.println("\nFinding Window name " + key1);
				if (counter != 0) {
					// A guard used to reset the map
					done = 0;
				}
				MutationHelper.findMatch(key1, value1, genRoot, counter, done);
			}
			counter++;
		}

		matchedMap = MutationHelper.getMap();
		System.out
				.println("\nOffspring is valid. Delete mutation will be performed.\nOffspring produced.");

		Shared.printMap(matchedMap);
		System.out.println("\n End of Offspring contents.\n");
		return matchedMap;
	}

}