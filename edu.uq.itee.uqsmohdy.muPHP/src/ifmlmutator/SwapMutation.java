package ifmlmutator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import util.MutationHelper;
import util.Shared;
import IFML.Core.Annotation;
import IFML.Core.IFMLModel;
import IFML.Core.InteractionFlowElement;
import IFML.Core.InteractionFlowModel;
import IFML.Core.InteractionFlowModelElement;
import IFML.Core.NamedElement;
import IFML.Core.NavigationFlow;
import IFML.Core.ViewComponent;
import IFML.Core.ViewElement;
import IFML.Core.ViewElementEvent;
import IFML.Core.impl.CoreFactoryImpl;
import IFML.Extensions.IFMLWindow;
import IFML.Extensions.OnSelectEvent;
import IFML.Extensions.impl.ExtensionsFactoryImpl;

public class SwapMutation {
	
	private static List<HashMap<String, String>> matchedMap;
	private static List<HashMap<String, String>> alternateMap;
	private static int chromosomeIndex;
	private static int newPrevWindowIndex;
	private static int prevWinSize;
	private static int previousWindowIndex;
	private static int nextWindowIndex;
	private static String previousWindow;
	private static String nextWindow;
	
	/**
	 * Swap mutation that swap a different gene into the specified window position. <!-- begin-user-doc -->
	 */
	public static HashMap<Integer, EObject> swapMutation(EObject chromosome, EList<EObject> chromosomeClone, EObject genRoot, int chromosomeSize) {
		
		System.out.println("\nSWAP MUTATION.\n");
		System.out.println("::::::::::::::::::::");
	
		EList<EObject> substituteWindows = new BasicEList<EObject>();
		HashMap<Integer, EObject> hmap = new HashMap<Integer, EObject>();
		int isSwap = 0;
		int selection = 0;
		EObject offspring = null;
		
		EcoreUtil.Copier copier = new EcoreUtil.Copier();
		EObject sysRoot = copier.copy(genRoot);
		copier.copyReferences();
		
		//prepare a list of available positions
		List<Integer> availChoices = new ArrayList<Integer>();
		int control = chromosomeSize;
		if (chromosomeSize > 3) {
			while (control != 3) {
				availChoices.add(control);
				control --;
			}
		}
		
		// randomly choose one of the available position
		Random randomiser = new Random();
		while (isSwap != 0) {
			while (substituteWindows.size() < 1 && availChoices.size() > 0) {
				selection = availChoices.get(randomiser.nextInt(availChoices.size()));
				// create a list of suitable windows to swap.
				substituteWindows = getSubstitutes(sysRoot, chromosome, chromosomeSize, selection);
				if (substituteWindows.size() < 1 && availChoices.size() > 0) {
					availChoices.remove((Integer)selection);
				}
			}
			
			chromosomeIndex = selection-1;
			newPrevWindowIndex = 0;
			prevWinSize = 0;
			previousWindowIndex = selection-2;
			nextWindowIndex = selection;
						
			//Change the name of the candidate to update its ancestry
			if (chromosome instanceof IFMLModel) {
				String ancestry = ((IFMLModel)chromosome).getName();
				((IFMLModel)chromosome).setName(ancestry+"-SM");
			}
			
			// if no alternative out flows available for previousWindow. Stop Insert mutation and return to main.
			if (substituteWindows.isEmpty()) {
				System.out.println("\n:::WARNING: No suitable mutant found for index " + selection 
						+ ":::\n:::Swapping is not performed.:::\n");
				//hmap.put(isSwap, offspring);
				//return hmap;
			}
			else if (substituteWindows.size() == 1) {
				// get the size of the mutant
				int mutantSize = Shared.getSize(substituteWindows.get(0));
				// if only one suitable mutant exist, copy the chromosome and add the mutant.
				offspring = swapMutantToOffspring(chromosome, chromosomeClone, chromosomeIndex, substituteWindows.get(0));
				EObject selectedWindow = substituteWindows.get(0);
				System.out.println("\n ++SWAPPING AT INDEX " + chromosomeIndex + " AND THE SWAPPED GENE IS " + ((IFMLWindow) selectedWindow).getName() + "++");
				// relink the Navigation Flow of previousWindow to mutant.
				String mutantName = ((IFMLWindow) selectedWindow).getName();
				offspring = relinkPreviousWindow(offspring, previousWindowIndex);
				// find Annotation element and swap it.
				if(!nextWindow.isEmpty()) {
					// relink the Navigation Flow to the nextWindow.
					offspring = setTargetToNextWindow(offspring, chromosomeIndex);
				}
				isSwap = 1;
			}
			else {
				// if substituteWindows have more than one candidate, random selection is applied.
				int selectedWindowIndex = Shared.randomiseIndex(substituteWindows.size());
				EObject selectedWindow = EcoreUtil.copy(substituteWindows.get(selectedWindowIndex));
				// get the size of the mutant
				int mutantSize = Shared.getSize(substituteWindows.get(selectedWindowIndex));
				// copy the chromosome and add the mutant.
				offspring = swapMutantToOffspring(chromosome, chromosomeClone, chromosomeIndex, selectedWindow);
				System.out.println("\n ++SWAPPING AT INDEX " + chromosomeIndex + " AND THE SWAPPED GENE IS " + ((IFMLWindow) selectedWindow).getName() + "++");
				// get the index for nextWindow.
				
				
				// relink the Navigation Flow of previousWindow to mutant.
				offspring = relinkPreviousWindow(offspring, previousWindowIndex);
				if(!nextWindow.isEmpty()) {
					// relink the Navigation Flow in the mutant to the nextWindow. 
					offspring = setTargetToNextWindow(offspring, chromosomeIndex);
					// find Annotation element and swap it.
				}
				else {
					offspring = removeOutflows(offspring, previousWindowIndex);
				}
				isSwap = 1;
			}
		}

		hmap.put(isSwap, offspring);
		return hmap;
	}
	
	/**
	 * Performs the swap mutation to the gene. <!-- begin-user-doc -->
	 * @param the ParentChromosome the parent individual's root object
	 * @param the Parent the parent individual
	 * @param the InsertionIndex the IFML Window's index where swapping will take place
	 * @param theMutant the swap mutant gene
	 * @param windowIndex
	 * @return the offspring
	 */
	private static EObject swapMutantToOffspring(EObject theParentChromosome, EList<EObject> theParent, int theSwapIndex, EObject theMutant) {
		
		// iterate through the chromosome to find the IFML Windows' container
		EObject themutant = EcoreUtil.copy(theMutant);
		TreeIterator<EObject> candidateIt = theParentChromosome.eAllContents();
		EObject browser = candidateIt.next();
		while (! (browser instanceof InteractionFlowModel)){
			browser = candidateIt.next();
		}		
		// insert the mutant gene
		if (browser instanceof Annotation) {
			((InteractionFlowModel)browser).getInteractionFlowModelElements().set(theSwapIndex,  (InteractionFlowModelElement) themutant);
		}
		return theParentChromosome;
	}
	
	/**
	 * Performs the swap mutation to the Annotation child element. <!-- begin-user-doc -->
	 * @param the ParentChromosome the parent individual's root object
	 * @param the Parent the parent individual
	 * @param the InsertionIndex the IFML Window's index where swapping will take place
	 * @param theMutant the swap mutant gene
	 * @param windowIndex
	 * @return the offspring
	 */
	private static EObject swapAnnotationInOffspring(EObject theParentChromosome, EList<EObject> theParent, int theSwapIndex, EObject theMutant) {
		
		// iterate through the chromosome to find an Annotation child element.
		EObject themutant = EcoreUtil.copy(theMutant);
		TreeIterator<EObject> candidateIt = theParentChromosome.eAllContents();
		EObject browser = candidateIt.next();
		while (! (browser instanceof InteractionFlowModel)){
			browser = candidateIt.next();
		}
		
		// inserts the mutant
		if (browser instanceof InteractionFlowModel) {
			((InteractionFlowModel)browser).getInteractionFlowModelElements().set(theSwapIndex,  (InteractionFlowModelElement) themutant);
		}

		return theParentChromosome;
	}
	
	
	/**
	 * Updates the navigation flow in the previous window. <!-- begin-user-doc -->
	 * @param theOffspring the offspring
	 * @param thePrevWinIndex the index of the IFML Window before the mutant gene
	 * @return the updated offspring
	 */
	private static EObject relinkPreviousWindow(EObject theOffspring, int thePrevWinIndex) {
		//EList<EObject> m = new BasicEList<EObject>();
		int mutantIndex = thePrevWinIndex+1;
		
		// Get the name of the TargetInteractionFlow.
		//EObject mutant = theOffspring.get(mutantIndex);
		//InteractionFlowElement destination = (InteractionFlowElement)mutantWindow;
		InteractionFlowElement destination = null;
		String destinationName = null;
		
		// get the container (Interaction Flow Model)
		TreeIterator<EObject> topIt = theOffspring.eAllContents(); 
		EObject ifmlElement = topIt.next();
		while(!(ifmlElement instanceof InteractionFlowModel)) {
			ifmlElement = topIt.next();
		}
		if (ifmlElement instanceof InteractionFlowModel) {
			// change the targetInteractionFlow in the container
			EList<EObject> contents = ((InteractionFlowModel)ifmlElement).eContents();
			destination = ((InteractionFlowElement)contents.get(mutantIndex));
			destinationName = ((NamedElement)destination).getName();
			EObject child = ((InteractionFlowModel)ifmlElement).getInteractionFlowModelElements().get(thePrevWinIndex);
			
			Iterator<EObject> prevWindowIt = child.eAllContents();
			int done = 0;
			while (prevWindowIt.hasNext()) {
				EObject navigation = prevWindowIt.next();
				if (navigation instanceof OnSelectEvent) {
					((OnSelectEvent) navigation).setName("link=" + destinationName);
				}
				else if (navigation instanceof NavigationFlow) {
					((NavigationFlow) navigation).setTargetInteractionFlowElement(destination);
					System.out.println("\n Changing the Navigation Flow " + ((NavigationFlow) navigation).getId() + " target to " + destinationName);
					done = 1;
					break;
						
				}
				else if (!(prevWindowIt.hasNext()) && done !=1){
					// if no navigation flow found, create a new link and navigation flow and add them to the IFML Window
					ViewComponent vc = CoreFactoryImpl.eINSTANCE.createViewComponent();
					OnSelectEvent ose = ExtensionsFactoryImpl.eINSTANCE.createOnSelectEvent();
					NavigationFlow nf = CoreFactoryImpl.eINSTANCE.createNavigationFlow();
					((NavigationFlow) nf).setTargetInteractionFlowElement(destination);
					((OnSelectEvent) ose).getOutInteractionFlows().add(nf);
					((OnSelectEvent) ose).setName("link=" + destinationName);
					((ViewComponent)vc).getViewElementEvents().add(ose);
					((IFMLWindow)child).getViewElements().add(vc);
				}
			}
		}

		return theOffspring;
	}
	
	/**
	 * Updates the navigation flow to the next window. <!-- begin-user-doc -->
	 * @param theOffspring the offspring
	 * @param mutantIndex the index of the mutant gene
	 * @return the updated offspring
	 */
	private static EObject setTargetToNextWindow(EObject theOffspring, int mutantIndex) {
		
		int nextWindowIndex = mutantIndex+1;
		int done = 0;
		ViewElement ve = null;
		ViewElementEvent vee = null;
		
		// Get the name of the TargetInteractionFlow.
		InteractionFlowElement destination = null;
		String destinationName = null;
		
		// get the container (Interaction Flow Model)
		TreeIterator<EObject> topIt = theOffspring.eAllContents(); 
		EObject ifmlElement = topIt.next();
		while(!(ifmlElement instanceof InteractionFlowModel)) {
			ifmlElement = topIt.next();
		}
		if (ifmlElement instanceof InteractionFlowModel) {
			// get the mutant Window in the container
			EList<EObject> contents = ((InteractionFlowModel)ifmlElement).eContents();
			destination = ((InteractionFlowElement)contents.get(nextWindowIndex));
			destinationName = ((NamedElement)destination).getName();
			// change the targetInteractionFlow in the mutant Window
			EObject mutantWindow = ((InteractionFlowModel)ifmlElement).getInteractionFlowModelElements().get(mutantIndex);
			Iterator<EObject> mutantWindowIt = mutantWindow.eAllContents();
			while (mutantWindowIt.hasNext()) {
				EObject child = mutantWindowIt.next();
				if (child instanceof OnSelectEvent) {
					String childName = ((NamedElement)child).getName();
					if (childName.contains(destinationName)) {
						((ViewElementEvent) child).getOutInteractionFlows().get(0).setTargetInteractionFlowElement(destination);
						// retrieve the ViewElement object for the navigation flow
						if (child.eContainer() instanceof ViewElement) {
							ve = (ViewElement) child.eContainer();
							vee = (ViewElementEvent) child;
							done = 1;
							break;
						}
					}
			
				}
			}
			
			// remove other View Elements.
			EList<EObject> removeThis = new BasicEList<EObject>();
			if (ve != null) {
				EList<EObject> extra = mutantWindow.eContents();
				for (EObject obj : extra) {
					if (!(obj.equals(ve))) {
						removeThis.add(obj);
					}
				}
				
				int removeSize = removeThis.size();
				for (int n=0; n<removeSize; n++) {
					EcoreUtil.delete(removeThis.get(n));
				}
			}
			
			// remove other View Element Event, i.e. buttons and links in the existing ViewElement
			EList<EObject> removeVee = new BasicEList<EObject>();
			if (vee != null) {
				EList<EObject> extraVee = ve.eContents();
				for (EObject obj : extraVee) {
					if (!(obj.equals(vee))) {
						removeVee.add(obj);
					}
				}
				
				int removeSize = removeVee.size();
				for (int n=0; n<removeSize; n++) {
					EcoreUtil.delete(removeVee.get(n));
				}
			}
		}
		
		return theOffspring;
	}
	
	/**
	 * Removes excessive outFlows if the mutant gene is the last gene in the offspring. <!-- begin-user-doc -->
	 * @param theOffspring the offspring
	 * @return the updated offspring
	 */
	private static EObject removeOutflows(EObject theOffspring, int thePrevWinIndex) {
		int mutantIndex = thePrevWinIndex+1;
		
		EObject mutantGene = ((IFMLModel)theOffspring).getInteractionFlowModel().getInteractionFlowModelElements().get(mutantIndex);
		((IFMLWindow)mutantGene).getViewElements().clear();
		
		return theOffspring;
	}
	
	/** 
	 * Returns a List of HashMap offspring after swapping is performed. <!-- begin-user-doc -->
	 * @throws IOException
	 */
	public static List<HashMap<String, String>> swapMutation2(
			final LinkedHashMap<String, String> parent, final int done)
			throws IOException {

		matchedMap = new ArrayList<HashMap<String, String>>();
		// Perform the swap mutation
		alternateMap = new ArrayList<HashMap<String, String>>();
		// get the number of existing parent candidates.
		final int parentsSize = parent.size();
		// default value of swapCtr
		int swapCtr = 1;
		// swapCtr must be positive
		if (parentsSize > 0) {
			swapCtr = Shared.randomiseThis(parentsSize-1);
		}
		final int swapIdx = Shared.getRandomRate(parent);

		//System.out.println("\nGene index " +swapIdx+" is randomly chosen for swap mutation operator.\n " + swapCtr + " gene/s will be swapped.");

		// finding the parent individual in the general model.
		// TODO: Remove swapIdx and swapCtr
		alternateMap = MutationHelper.getAlternatives(parent, parentsSize, swapIdx,
				swapCtr);
		alternateMap = MutationHelper.getMap();
		//System.out
		//.println("\nOffspring is valid. Swap mutation performed.\nOffspring produced.");
		Shared.printMap(alternateMap);
		System.out.println("\n End of Offspring contents.\n");
		return alternateMap;
	}
	
	/** 
	 * Returns a list of substitute Windows. <!-- begin-user-doc -->
	 * @param sysRoot the general model
	 * @param chromosome the parent individual
	 * @param size the size of the parent individual
	 * @param selection the selected position for swapping
	 * @return the list of substitute Windows
	 */
	public static EList<EObject> getSubstitutes (EObject sysRoot, EObject chromosome, int size, int selection) {
		
		chromosomeIndex = selection-1;
		newPrevWindowIndex = 0;
		prevWinSize = 0;
		previousWindowIndex = selection-2;
		nextWindowIndex = selection;
		previousWindow = "nil";
		nextWindow = "";
		
		// iterate through the chromosome to find the IFML Windows' container
		TreeIterator<EObject> candidateIt = chromosome.eAllContents();
		EObject browser = candidateIt.next();
		while (! (browser instanceof InteractionFlowModel)){
			browser = candidateIt.next();
		}
		
		// iterate the Interaction Flow Model Elements
		EList<EObject> contents = ((InteractionFlowModel)browser).eContents();
		//get the name of the previous Window and the next Window
		if(previousWindow.equals("nil")) {
			previousWindow = ((NamedElement)contents.get(previousWindowIndex)).getName();
			if (size > 3 && nextWindowIndex < size) {
				nextWindow = ((NamedElement)contents.get(nextWindowIndex)).getName();
			}
		}
		
		EList<EObject> substitutes = new BasicEList<EObject>();
		// traverse the general model to find suitable mutant(s).
		TreeIterator<EObject> sysIt = sysRoot.eAllContents();
		EObject obj = sysIt.next();
		while (sysIt.hasNext()) {
			//if (!(obj instanceof IFMLWindow)) {
			if (!(obj instanceof IFMLWindow)) {
				obj = sysIt.next();
			}
			else {
				String tempWindow = ((IFMLWindow) obj).getName();
				if ((previousWindow != null && !previousWindow.isEmpty()) && (tempWindow.equalsIgnoreCase(previousWindow))) {
					// retrieve a list of alternative out flow(s) for obj (previousWindow).
					substitutes.addAll(Shared.findBridgeWindows(obj, nextWindow));
					break;
				}
				else {
					obj = sysIt.next();
				}
			}
		}
		
		// additional, if selected gene exist in the substituteWindows list, remove it
		String selectedGeneName = ((NamedElement)contents.get(chromosomeIndex)).getName();
		int unwanted = 0;
		String unwantedExist = "";
		if (!(substitutes.isEmpty())) {
			for (EObject a : substitutes) {
				String aName = ((NamedElement)a).getName();
				if (aName.equalsIgnoreCase(selectedGeneName)) {
					unwanted = substitutes.indexOf(a);
					unwantedExist = "y";
					break;
				}
			}
		}
		// remove the unwanted substitute window
		if (unwantedExist.equals("y")) {
			substitutes.remove(unwanted);
		}
		
		return substitutes;
	}
	
}