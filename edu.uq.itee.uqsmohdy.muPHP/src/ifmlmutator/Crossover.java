package ifmlmutator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections.iterators.ArrayListIterator;
import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.lang.ArrayUtils;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import printer.SeleniumPrinter;
import util.MutationHelper;
import util.Shared;
import ifmlmutator.CrossoverHelper;
import IFML.Core.IFMLAction;
import IFML.Core.IFMLModel;
import IFML.Core.InteractionFlowElement;
import IFML.Core.InteractionFlowModel;
import IFML.Core.InteractionFlowModelElement;
import IFML.Core.NamedElement;
import IFML.Core.NavigationFlow;
import IFML.Core.impl.DomainModelImpl;
import IFML.Extensions.IFMLWindow;

public class Crossover {

	private static List<EObject> parentsCopy;
	private static List<EList<EObject>> offsprings;
	public static List<String> theCrossPositionList;
    public static String P1Name;
    public static String P2Name;
    public static String previousWindow = "";
    public static String crossWindow = "";

	/**
	 * <!-- begin-user-doc -->Performs random cut and splice crossover.<!-- end-user-doc -->
	 * 
	 * @param parents the list containing the parent individuals.
	 * @param generalModel the system under test's IFML model.
	 * @throws IOException ***CONTENTS***
	 * @return the number of offspring(s)
	 */
	public static int individualCrossover(List<EObject> parents,
			EObject generalModel) throws IOException {
		
		// Create a copy of the parent individuals for recursion of the crossover later.
		parentsCopy = new ArrayList<EObject>(parents);
		// "offsprings" are for storing offspring created by the crossover operator.
		// "tempOptions" contains a list of alternative crossWindow substitutes 
		// (i.e. the crossover point) retrieved from the general model.
		offsprings = new ArrayList<EList<EObject>>();
		List<String> tempOptions = new ArrayList<String>();
		List<Integer> selectedIndex = new ArrayList<>();
		EList<EObject> Parent1TmpList;
		
		int Parent1TmpSize = 0;
		int PTmp2ListSize = 0;
		int crossIndex = 0;
		int end = 0;
		int totalOP = 0;
		
		EObject Parent1Tmp = null;

		// If the size of first parent is less than 4, repeat selection.
		// This prerequisite is to prevent the crossover operator from splicing
		// the login elements that occupy the first 3 elements.
		while ( Parent1TmpSize < 4 ) {
			// Step 1: Randomly retrieve the first parent individual from current population.
			Parent1Tmp = CrossoverHelper.getFirstParent(parentsCopy);
			// Convert Parent 1 into an EList of Windows EObject.
			Parent1TmpList = Shared.copyAllGenes(Parent1Tmp);
			Parent1TmpSize = Shared.numberofWindows(Parent1TmpList);
		}
		
		// Eliminate Parent1Tmp from parents' list to avoid the same individual selected as the second parent.
		parentsCopy = CrossoverHelper.removeSelection(parentsCopy);
		
		while (end != 1) {
			
			Random randomise = new Random();
			// Step 2: Select the crossover position for Parent 1.
			// Only allow the crossover to happen after the login IFMLWindow elements.
			int max = Parent1TmpSize - 4;
			if (crossIndex != 0) {
				int size = selectedIndex.size();
				while (selectedIndex.contains(crossIndex) && size < max) {
					// Select other crossover position
					crossIndex = randomise.nextInt(Parent1TmpSize - 4) + 4;  // FIXME: IllegalArgument error n must be positive is sometimes triggered here.
				}
				if (!selectedIndex.contains(crossIndex)) {
					selectedIndex.add(crossIndex);
				}
			}	
			else {
				// First time selection of crossover position.
				if (Parent1TmpSize == 4) {
					crossIndex = 4;
					selectedIndex.add(crossIndex);
				}
				else {
					crossIndex = randomise.nextInt(Parent1TmpSize - 4) + 4;	
					selectedIndex.add(crossIndex);
				}
			}
			
			// Get the previous window and the crossover window names.
			previousWindow = CrossoverHelper.getPreviousWindow(Parent1Tmp, crossIndex-1);
			crossWindow = CrossoverHelper.getCrossWindow(Parent1Tmp, crossIndex);
			
			// Step 3: Create a list of substitute windows from the general model.
			TreeIterator<EObject> genIt = generalModel.eAllContents();

			if (previousWindow.length() != 0 && crossWindow.length() != 0) {
				tempOptions = CrossoverHelper.getAlternatives(previousWindow, crossWindow,
						genIt);
			}
			
			// "clonedtmpOptions" will clone "tempOptions" and will be used to eliminate substitute windows that is equal to the windowName (optional) from tempOptions.
			if (!tempOptions.isEmpty()) {
				List<String> clonedtmpOptions = new ArrayList<String>(tempOptions);
				for (String o : clonedtmpOptions) {
					if (o.equals(previousWindow)) {
						tempOptions.remove(o);
					}
				}
			}
			
			EObject Parent2Tmp = null;
			String CrossPosition = null;
			// If tempOptions is empty, notify user and repeat the crossover operation.
			if (tempOptions.isEmpty()) {
				System.out.println("\nThe selected crossover position does not yield positive result for crossover.\nOther parent individual will be selected.\n");
			}	// FIXME: Sometimes this part goes into multiple loops.
			
			else {
				// Step 4: If tempOptions not empty, search other parent candidates in the current population 
				// for matching (previous-crossover) windows pair.
				// "PTmp2List" list all the available candidates for Parent2.
				List<EObject> PTmp2List = getNextParent(tempOptions, parentsCopy, crossWindow, generalModel);
				PTmp2ListSize = PTmp2List.size();
				
				if (PTmp2ListSize<5) {
					for (EObject partner : PTmp2List) {
						int candidate2Index = PTmp2List.indexOf(partner);
						Parent2Tmp = PTmp2List.get(candidate2Index);
						CrossPosition = theCrossPositionList.get(candidate2Index);
						// Perform crossover
						List<EList<EObject>> tmpOP = CrossoverHelper.crossOver(crossIndex, Parent1Tmp, Parent2Tmp, crossWindow,
								CrossPosition);
						offsprings.addAll(tmpOP);
					}
				}
				else {
					// Limit the crossover to produce a maximum of 5 pairs if there are too many candidates.
					for (int i=0; i<5; i++) {
						//int candidate2Index = PTmp2List.indexOf(partner);
						//Parent2Tmp = PTmp2List.get(candidate2Index);
						//CrossPosition = theCrossPositionList.get(candidate2Index);
						Parent2Tmp = PTmp2List.get(i);
						CrossPosition = theCrossPositionList.get(i);
						// Perform crossover
						List<EList<EObject>> tmpOP = CrossoverHelper.crossOver(crossIndex, Parent1Tmp, Parent2Tmp, crossWindow,
								CrossPosition);
						offsprings.addAll(tmpOP);
					}
				}
				end = end + 1;
			}
				
			if (end == 1) {
				// Display the offsprings.
				int n = 1;
				for (EList<EObject> offspring : offsprings) {
					System.out.println("\nOFFSPRING PRODUCED.\nThe offspring will be transformed into Selenium test cases.\n");
					// Use the following line to view the IFML elements inside the offspring.
					//Shared.displayContents1(offspring);
					// Change the destination of the offspring in saveModel when debugging.
					Resource seleniumResource = MutationHelper.saveModel(offspring,
							"COP", n);
					// Transform the IFML offspring into a Selenium test case.
					String fileName = MutationHelper.getfileName();
					fileName = fileName.replaceAll("(.core)", "");
					EObject[] seleniumRoot = new EObject[1];
					seleniumRoot[0] = seleniumResource.getContents().get(0);
					SeleniumPrinter.printOne(seleniumRoot, 0, fileName);
					// Write to log
					MutationHelper.writeLog(fileName, offspring);
					n++;
				}
			}
			// If after exhausted all selection, no crossover performed, end this operation.
			//end = 1;
		}
		
		// If crossover completes, return the number of offsprings.
		totalOP = offsprings.size();
		return totalOP;

	}





	/**
	 * <!-- begin-user-doc -->Returns the second candidate for crossover.<!-- end-user-doc -->
	 * 
	 * @param windowOptions other IFML Windows
	 * @param parents a list of parent candidates or individuals
	 * @param crossWindow the crossover Window of the first parent individual
	 * @param genModel the general model
	 * @return a list of individuals as second candidates for crossover
	 * @throws IOException
	 */
	
	//TODO: Review this, because the theCrossPositionList size does not tally with PTmp2List
	private static List<EObject> getNextParent(List<String> windowOptions,
			List<EObject> parents, String crossWindow, EObject genModel) {
		
		List<EObject> candidateList = new ArrayList<EObject>();
		List<String> candidateIndexList = new ArrayList<String>();
		
		// For each window's name in windowOptions, 
		for (String window : windowOptions) {
			for (EObject candidate : parents) {
				// Iterate the parents until the candidate is found.
				TreeIterator<EObject> iterator = candidate.eAllContents();
				iterator.next();
				EObject target = iterator.next();
				Boolean notFound = true;
				while (notFound != false) {
					while (!(target instanceof IFMLWindow)) {
						target = iterator.next();
					}
					// Must make sure the login windows is skipped
					if (((IFMLWindow) target).getName().equalsIgnoreCase(
							"Staff Login")) {
						target = iterator.next();
						notFound = false;
					}
					target = iterator.next();
				}
				
				// Stops searching when the DomainModel is reached.
				while (!(target instanceof DomainModelImpl)) {
					while (!(target instanceof IFMLWindow)) {
						if (target instanceof DomainModelImpl) {
							break;
						}
						else {
							target = iterator.next();
						}
					}
					if (target instanceof IFMLWindow) {
						String targetName = ((IFMLWindow) target).getName();
						if (window.contains(targetName)) {
							// save this candidate.
							candidateList.add(candidate);
							candidateIndexList.add(window);
							break;
						}
						else {
							target = iterator.next();
						}
					}
				}
			}
		}
		getCandidateList(candidateIndexList);
		
		// Eliminate members in the list that might produce invalid second offspring
		int listSize = candidateList.size();
		for (int i=0; i<listSize; i++) {
			String prevWindow = candidateIndexList.get(i);
			//if the crossover is not valid, delete the member
			TreeIterator<EObject> genTree = genModel.eAllContents();
			if (!(CrossoverHelper.isValid(prevWindow, crossWindow, genTree))) {
				candidateIndexList.remove(i);
				candidateList.remove(i);
				theCrossPositionList.remove(i); // recently added 5/5/16. pending review.
				listSize = candidateList.size();
			}
		}
		
		// returns the potential candidates, saved as a list.
		return candidateList;
	}
	
	/**
	 * <!-- begin-user-doc -->Saves the list of crossIndex 
	 * for every candidate in candidateList.<!-- end-user-doc -->
	 */
	public static void getCandidateList(List<String> list) {
		theCrossPositionList = new ArrayList<String>();
		theCrossPositionList.addAll(list);
	}

	/**
	 * <!-- begin-user-doc -->Perform a cut and splice crossover technique.<!-- end-user-doc -->
	 * 
	 * @throws IOException
	 */
	private static List<EList<EObject>> crossOver(EList<EObject> parent1,
			EList<EObject> parent2, String stopPoint, String crossoverPoint) {
		EList<EObject> offspring1 = new BasicEList<EObject>();
		EList<EObject> offspring2 = new BasicEList<EObject>();
		EList<EObject> clone1 = new BasicEList<EObject>();
		EList<EObject> clone2 = new BasicEList<EObject>();
		List<EList<EObject>> offspring = new ArrayList<EList<EObject>>();
		EList<String> tempWinList1 = new BasicEList<String>();
		EList<String> tempWinList2 = new BasicEList<String>();
		EList<EObject> tempWin1 = new BasicEList<EObject>();
		EList<EObject> tempWin2 = new BasicEList<EObject>();
		clone1.addAll(EcoreUtil.copyAll(parent2));
		clone2.addAll(EcoreUtil.copyAll(parent1));

		String objectName = null;

		// First, copy the genes of Parent1 from index 0 until the stopPoint into offspring1.
		Iterator<EObject> parentIt = parent1.iterator();
		while (parentIt.hasNext()) {
			EObject object = parentIt.next();
			if (object instanceof IFMLModel) {
				((IFMLModel) object).getName();
			}
			if (object instanceof IFMLWindow) {
				objectName = ((IFMLWindow) object).getName();
				if (objectName.equals(stopPoint)) {
					break;
				}
			}
			EObject tmp1 = EcoreUtil.copy(object);
			offspring1.add(tmp1);
			EObject tmp2 = EcoreUtil.copy(object);
			clone2.remove(tmp2);
			EcoreUtil.delete(tmp2);
			System.out.println(offspring1);
			System.out.println(".........................");
		}
		int done = 0;
		// Then, copy the genes of Parent2 from the crossoverPoint to the end into offspring1.
		for (Iterator<EObject> iter = parent2.iterator(); iter.hasNext();) {
			EObject object = iter.next();
			if (object instanceof IFMLModel) {
				((IFMLModel) object).getName();
			}
			// Next, move all genes before IFML Window in clone 1 to offspring2.
			while (object instanceof IFMLWindow == false && done != 2) {
				offspring2.add(object);
				clone1.remove(object);
				done = 1;
				object = iter.next();
			}
			while (iter.hasNext() && done != 2) {
				if (object instanceof IFMLWindow) {
					objectName = ((IFMLWindow) object).getName();
					// If the window is not the crossover point, move the window and its children from clone 1 into offspring2.
					if (objectName.equals(crossoverPoint) == false) {
						offspring2.add(object);
						TreeIterator<EObject> tempchildren = object
								.eAllContents();
						// Move all the children of the window first.
						while (tempchildren.hasNext()) {
							EObject tempPtr = tempchildren.next();
							offspring2.add(tempPtr);
							clone1.remove(tempPtr);
						}
						// Then, remove the window from clone 1.
						clone1.remove(object);
						System.out.println(clone1);
						System.out.println(".........................");
						object = iter.next();
					}
					// If the window is the crossover point, do not do anything but exit the loop.
					else if (objectName.equals(crossoverPoint)) {
						done = 2;
						break;
					}
				}
				object = iter.next();
			}
		}

		// Create a list of added windows for both clones.
		for (Iterator<EObject> genesIt1 = clone1.iterator(); genesIt1.hasNext();) {
			EObject object = genesIt1.next();
			if (object instanceof IFMLWindow) {
				String objName = ((IFMLWindow) object).getName();
				tempWinList1.add(objName);
				tempWin1.add(object);
			}
		}
		for (Iterator<EObject> genesIt2 = clone2.iterator(); genesIt2.hasNext();) {
			EObject object = genesIt2.next();
			if (object instanceof IFMLWindow) {
				String objName = ((IFMLWindow) object).getName();
				tempWinList2.add(objName);
				tempWin2.add(object);
			}
		}

		// Append the rest of the clone to offspring
		offspring1.addAll(clone1);
		offspring2.addAll(clone2);

		// Update the container object (InteractionFlowModel) to contain all added windows in both offspring.
		offspring1 = updateContainer(offspring1, tempWin1, tempWinList2);
		offspring2 = updateContainer(offspring2, tempWin2, tempWinList1);

		// Add both offsprings to list of offspring.
		offspring.add(offspring1);
		offspring.add(offspring2);

		return offspring;
	}

	/**
	 * <!-- begin-user-doc -->Updates the container object (InteractionFlowModel)  
	 * of the offspring to contain all added IFML Windows.<!-- end-user-doc -->
	 * 
	 * @param offspring the IFML model
	 * @param addedwindows the list of IFMLWindows that will be added
	 * @param deletedwindowList the list of IFMLWindows that will be removed
	 * @return the updated IFML Model
	 */
	public static EList<EObject> updateContainer(EList<EObject> offspring,
			EList<EObject> addedwindows, EList<String> deletedwindowList) {

		// First, delete the unwanted windows
		for (String deletedName : deletedwindowList) {
			for (EObject d : offspring) {
				if (d instanceof InteractionFlowModel) {
					//d.eContents();
					for (EObject window : d.eContents()) {
						String windowName = ((NamedElement) window).getName();

						if (windowName.contains(deletedName)) {
							((InteractionFlowModel) d)
									.getInteractionFlowModelElements().remove(
											window);
							break;
						}

					}

				}
			}
		}
		// Next, update the container with new window
		for (EObject d : offspring) {
			if (d instanceof InteractionFlowModel) {
				((InteractionFlowModel) d)
						.getInteractionFlowModelElements()
						.addAll((Collection<? extends InteractionFlowModelElement>) addedwindows);

			}
		}

		return offspring;
	}
	
	/**
	 * <!-- begin-user-doc -->Updates the container object (InteractionFlowModel) 
	 * of the offspring to contain all added IFML Windows.<!-- begin-user-doc -->
	 * @param offspring the IFML model
	 * @return the updated IFML Model
	 */
	public static EObject updateContainer2(EObject offspring) {

		// Iterate the offspring to find the InteractionFlowModel object.
		TreeIterator<EObject> opIt = offspring.eAllContents();
		EObject browser = opIt.next();
		while (! (browser instanceof InteractionFlowModel)){
			browser = opIt.next();
		}	
		
		// Iterate the Interaction Flow Model object, copy the genes to a list and delete the genes from the clone.
		EList<EObject> ifmlWindowsList = ((InteractionFlowModel)browser).eContents();
		
		EObject outFlow = null;
		int counter = 0;
		int max = ifmlWindowsList.size()-1;
		for (EObject gene : ifmlWindowsList) {
			if (gene instanceof IFMLWindow && counter < max) {
				TreeIterator<EObject> geneIt1 = gene.eAllContents();
				EObject iflow1;
				while (geneIt1.hasNext()) {
					iflow1 = geneIt1.next();
					if (iflow1 instanceof NavigationFlow) {
						outFlow = iflow1;
						//System.out.println("found");
						break;
					}
				}	
				// Update outFlow to the new target window.
				if (counter < max && ifmlWindowsList.get(counter) != null) {
					InteractionFlowElement target1 = ((InteractionFlowElement)ifmlWindowsList.get(counter+1));
					if (target1 != null) {
						((NavigationFlow)outFlow).setTargetInteractionFlowElement(target1);
						String targetName1 = ((NamedElement)target1).getName();
						//if (!(targetName1.contains("Home") || targetName1.equals("Staff Login"))) {
						//	((NamedElement)outFlow.eContainer()).setName("link=" + targetName1.replace("'", ""));
						//}
					}
				}
			}
			else if (gene instanceof IFMLAction && counter < max) {
				TreeIterator<EObject> geneIt2 = gene.eAllContents();
				EObject iflow2;
				while (geneIt2.hasNext()) {
					iflow2 = geneIt2.next();
					if (iflow2 instanceof NavigationFlow) {
						outFlow = iflow2;
						break;
					}
				}
				// Next, update outFlow to the new target window.
				// TODO: Maybe change the OnSubmitEvent to OnSelectEvent as this sometimes causes issues during the transformation
				if (counter < max && ifmlWindowsList.get(counter) != null) {
					InteractionFlowElement target2 = ((InteractionFlowElement)ifmlWindowsList.get(counter+1));
					if (target2 != null) {
						((NavigationFlow)outFlow).setTargetInteractionFlowElement(target2);
						String targetName2 = ((NamedElement)target2).getName();
						//if (!(targetName2.contains("Home") || targetName2.equals("Staff Login"))) {
						//	((NamedElement)outFlow.eContainer()).setName("link=" + targetName2);
						//}
					}
				}
			}
			counter++;
		}

		return offspring;
	}
}