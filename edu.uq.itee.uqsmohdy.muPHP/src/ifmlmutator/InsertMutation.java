package ifmlmutator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import util.Shared;
import IFML.Core.IFMLModel;
import IFML.Core.InteractionFlowElement;
import IFML.Core.InteractionFlowModel;
import IFML.Core.InteractionFlowModelElement;
import IFML.Core.NamedElement;
import IFML.Core.NavigationFlow;
import IFML.Core.ViewComponent;
import IFML.Core.ViewElement;
import IFML.Core.ViewElementEvent;
import IFML.Core.impl.CoreFactoryImpl;
import IFML.Extensions.IFMLWindow;
import IFML.Extensions.OnSelectEvent;
import IFML.Extensions.impl.ExtensionsFactoryImpl;

public class InsertMutation {
	
	/**
	 * Insert mutation that inserts an IFML Window into the specified window position. <!-- begin-user-doc -->
	 * @param chromosome the root object of the candidate
	 * @param chromosomeClone a clone of the candidate
	 * @param genRoot the root object of the general model
	 * @param chromosomeSize the size of the candidate
	 * @return the offspring
	 */
	public static HashMap<Integer, EObject> insertMutation(EObject chromosome, EList<EObject> chromosomeClone, EObject genRoot, int chromosomeSize) {
		
		System.out.println("\nINSERT MUTATION.\n");
		System.out.println("::::::::::::::::::::");

		EcoreUtil.Copier copier = new EcoreUtil.Copier();
		EObject sysRoot = copier.copy(genRoot);
		copier.copyReferences();
		
		HashMap<Integer, EObject> hmap = new HashMap<Integer, EObject>();
		int isInsert = 0;
		EObject tempOffspring = null;
		EObject tempOP = null;
		//EObject temp = null;
		List<Integer> selectedIndex = new ArrayList<>();
		Random randomise = new Random();
		
		// control the randomness to prevent insert mutation from happening before the login IFMLWindow.
		int selection = 0;
		int max = chromosomeSize - 4;
		
		while (isInsert != 1) {
			// the idea is to make sure that selection is set to a position that was not yet selected.
			if (selection != 0) {
				int size = selectedIndex.size();
				while (selectedIndex.contains(selection) && size < max) {
					selection = randomise.nextInt(chromosomeSize - 4) + 4;
				}
				if (!selectedIndex.contains(selection)) {
					selectedIndex.add(selection);
				}
			}
			else {
				if (chromosomeSize == 4) {
					selection = 4;
					selectedIndex.add(selection);
				}
				else {
					selection = randomise.nextInt(chromosomeSize - 4) + 4;
				}
			}
			
			//TODO: Make the mutation operator selects a different position if the current position fails to deliver an offspring. 
			//TODO: Next, if all position has been exhausted, return to Assistant to select another candidate.
			
			int chromosomeIndex = selection-1;
			//int newPrevWindowIndex = 0;
			//int prevWinSize = 0;
			int previousWindowIndex = selection-2;
			int nextWindowIndex = selection-1;
			String previousWindow = "nil";
			String nextWindow = "";
			
			// XXX check the relevancy of this part
			//String special = "Staff Login";
			//System.out.println("\nFOR REFERENCE");
			//Shared.displayContents1(chromosomeClone);
			
			//Change the name of the candidate to update its ancestry
			if (chromosome instanceof IFMLModel) {
				String ancestry = ((IFMLModel)chromosome).getName();
				((IFMLModel)chromosome).setName(ancestry+"-IM");
			}
			
			// iterate through the chromosome to find the IFML Windows' container
			TreeIterator<EObject> candidateIt = chromosome.eAllContents();
			EObject browser = candidateIt.next();
			while (! (browser instanceof InteractionFlowModel)){
				browser = candidateIt.next();
			}
			// iterate the Interaction Flow Model Elements
			EList<EObject> contents = ((InteractionFlowModel)browser).eContents();
			
			//get the name of the previous Window and the next Window
			if(previousWindow.equals("nil")) {
				previousWindow = ((NamedElement)contents.get(previousWindowIndex)).getName();
				if (chromosomeSize > 3) {
					nextWindow = ((NamedElement)contents.get(nextWindowIndex)).getName();
				}
			}
			
			// create a list of suitable windows to insert.
			EList<EObject> substituteWindows = new BasicEList<EObject>();
			
			// traverse the general model to find suitable mutant(s).
			TreeIterator<EObject> sysIt = sysRoot.eAllContents();
			EObject obj = sysIt.next();
			while (sysIt.hasNext()) {
				//if (!(obj instanceof IFMLWindow)) {
				if (!(obj instanceof IFMLWindow)) {
					obj = sysIt.next();
				}
				else {
					String tempWindow = ((IFMLWindow) obj).getName();
					if ((previousWindow != null && !previousWindow.isEmpty()) && (tempWindow.equalsIgnoreCase(previousWindow))) {
						// retrieve a list of alternative out flow(s) for obj (previousWindow).
						substituteWindows.addAll(Shared.findBridgeWindows(obj, nextWindow));
						break;
					}
					else {
						obj = sysIt.next();
					}
				}
			}
			
			// insertion. if no alternative out flows available for previousWindow, reselect another position.
			//chromosomeIndex = newPrevWindowIndex + prevWinSize;
			if (substituteWindows.isEmpty()) {
				System.out.println("\n:::WARNING: No suitable insert mutant found for index position " + selection 
						+ ":::\n:::Starting again with another index position.:::\n");
			}
			else if (substituteWindows.size() == 1) {
				// get the size of the mutant
				//int mutantSize = Shared.getSize(substituteWindows.get(0));
				// if only one suitable mutant exist, copy the chromosome and add the mutant.
				tempOffspring = insertMutantToOffspring(chromosome, chromosomeClone, chromosomeIndex, substituteWindows.get(0));
				EObject selectedWindow = substituteWindows.get(0);
				String mutantWindowName = ((NamedElement) selectedWindow).getName();
				System.out.println("\n ++INSERTION AT INDEX " + chromosomeIndex + " AND THE INSERTED GENE IS " + mutantWindowName + "++");
				// relink the Navigation Flow of previousWindow to mutant.
				//String mutantName = ((IFMLWindow) selectedWindow).getName();
				tempOffspring = relinkPreviousWindow(tempOffspring, previousWindowIndex);
				if(!nextWindow.isEmpty()) {
					// relink the Navigation Flow to the nextWindow.
					tempOffspring = setTargetToNextWindow(tempOffspring, chromosomeIndex);
				}
				isInsert = 1;
			}
			else {
				// if substituteWindows have more than one candidate, random selection is applied.
				int selectedWindowIndex = Shared.randomiseIndex(substituteWindows.size());
				EObject selectedWindow = substituteWindows.get(selectedWindowIndex);
				String mutantWindowName = ((NamedElement) selectedWindow).getName();
				// get the size of the mutant
				//int mutantSize = Shared.getSize(substituteWindows.get(selectedWindowIndex));
				// copy the chromosome and add the mutant.
				tempOP = insertMutantToOffspring(chromosome, chromosomeClone, chromosomeIndex, selectedWindow);
				System.out.println("\n ++INSERTION AT INDEX " + chromosomeIndex + " AND THE INSERTED GENE IS " + mutantWindowName + "++");
				// get the index for nextWindow.
				
				// insert a new Annotation if there is a need.
				tempOffspring = util.Shared.replaceAnnotation(tempOP, mutantWindowName);
				
				// relink the Navigation Flow of previousWindow to mutant.
				tempOffspring = relinkPreviousWindow(tempOffspring, previousWindowIndex);
				if(!nextWindow.isEmpty()) {
					// relink the Navigation Flow in the mutant to the nextWindow. 
					tempOffspring = setTargetToNextWindow(tempOffspring, chromosomeIndex);
				}
				else {
					tempOffspring = removeOutflows(tempOffspring, previousWindowIndex);
				}
				isInsert = 1;
			}
		}
		
		hmap.put(isInsert, tempOffspring);
		return hmap;
	}
	
	/**
	 * Performs the insert mutation. <!-- begin-user-doc -->
	 * @param the ParentChromosome the parent individual's root object
	 * @param the Parent the parent individual
	 * @param the InsertionIndex the IFML Window's index where insertion will take place
	 * @param theMutant the insert mutant gene
	 * @param windowIndex
	 * @return the offspring
	 */
	private static EObject insertMutantToOffspring(EObject theParentChromosome, EList<EObject> theParent, int theInsertionIndex, EObject theMutant) {
		
		// iterate through the chromosome to find the IFML Windows' container
		EObject themutant = EcoreUtil.copy(theMutant);
		TreeIterator<EObject> candidateIt = theParentChromosome.eAllContents();
		EObject browser = candidateIt.next();
		while (! (browser instanceof InteractionFlowModel)){
			browser = candidateIt.next();
		}
		
		// insert the mutant gene
		if (browser instanceof InteractionFlowModel) {
			((InteractionFlowModel)browser).getInteractionFlowModelElements().add(theInsertionIndex,  (InteractionFlowModelElement) themutant);
		}
		

		return theParentChromosome;
	}
	
	/**
	 * Updates the navigation flow in the previous window. <!-- begin-user-doc -->
	 * @param theOffspring the offspring
	 * @param thePrevWinIndex the index of the IFML Window before the mutant gene
	 * @return the updated offspring
	 */
	public static EObject relinkPreviousWindow(EObject theOffspring, int thePrevWinIndex) {
		//EList<EObject> m = new BasicEList<EObject>();
		int mutantIndex = thePrevWinIndex+1;
		
		// Get the name of the TargetInteractionFlow.
		//EObject mutant = theOffspring.get(mutantIndex);
		//InteractionFlowElement destination = (InteractionFlowElement)mutantWindow;
		InteractionFlowElement destination = null;
		String destinationName = null;
		
		// get the container (Interaction Flow Model)
		TreeIterator<EObject> topIt = theOffspring.eAllContents(); 
		EObject ifmlElement = topIt.next();
		while(!(ifmlElement instanceof InteractionFlowModel)) {
			ifmlElement = topIt.next();
		}
		if (ifmlElement instanceof InteractionFlowModel) {
			// get the previous Window in the container
			EList<EObject> contents = ((InteractionFlowModel)ifmlElement).eContents();
			destination = ((InteractionFlowElement)contents.get(mutantIndex));
			destinationName = ((NamedElement)destination).getName();
			EObject prevWindow = ((InteractionFlowModel)ifmlElement).getInteractionFlowModelElements().get(thePrevWinIndex);
			// change the targetInteractionFlow in the previous Window
			int done = 0;
			Iterator<EObject> prevWindowIt = prevWindow.eAllContents();
			while (prevWindowIt.hasNext()) {
				EObject child = prevWindowIt.next();
				// For now, the process of relinking the previous window with the mutant window can only be done
				// by changing the existing OnSelectEvent element, or creating a new OnSelectEvent element
				// TODO: Find a way to recreate the link that is ffaithful to the way it is modeled in the general model.
				if (child instanceof OnSelectEvent) {
					((OnSelectEvent) child).setName("link=" + destinationName);
				}
				else if (child instanceof NavigationFlow) {
					((NavigationFlow) child).setTargetInteractionFlowElement(destination);
					System.out.println("\n Changing the Navigation Flow " + ((NavigationFlow) child).getId() + " target to " + destinationName);
					done = 1;
					break;
						
				}
				else if (!(prevWindowIt.hasNext()) && done !=1){
					// if no navigation flow found, create a new link and navigation flow and add them to the IFML Window
					ViewComponent vc = CoreFactoryImpl.eINSTANCE.createViewComponent();
					OnSelectEvent ose = ExtensionsFactoryImpl.eINSTANCE.createOnSelectEvent();
					NavigationFlow nf = CoreFactoryImpl.eINSTANCE.createNavigationFlow();
					((NavigationFlow) nf).setTargetInteractionFlowElement(destination);
					((OnSelectEvent) ose).getOutInteractionFlows().add(nf);
					((OnSelectEvent) ose).setName("link=" + destinationName);
					((ViewComponent)vc).getViewElementEvents().add(ose);
					((IFMLWindow)prevWindow).getViewElements().add(vc);
				}
			}
		}

		return theOffspring;
	}

	/**
	 * Updates the navigation flow to the next window. <!-- begin-user-doc -->
	 * @param theOffspring the offspring
	 * @param mutantIndex the index of the mutant gene
	 * @return the updated offspring
	 */
	public static EObject setTargetToNextWindow(EObject theOffspring, int mutantIndex) {
		
		int nextWindowIndex = mutantIndex+1;
		int done = 0;
		ViewElement ve = null;
		ViewElementEvent vee = null;
		
		// Get the name of the TargetInteractionFlow.
		InteractionFlowElement destination = null;
		String destinationName = null;
		
		// get the container (Interaction Flow Model)
		TreeIterator<EObject> topIt = theOffspring.eAllContents(); 
		EObject ifmlElement = topIt.next();
		while(!(ifmlElement instanceof InteractionFlowModel)) {
			ifmlElement = topIt.next();
		}
		if (ifmlElement instanceof InteractionFlowModel) {
			// get the mutant Window in the container
			EList<EObject> contents = ((InteractionFlowModel)ifmlElement).eContents();
			destination = ((InteractionFlowElement)contents.get(nextWindowIndex));
			destinationName = ((NamedElement)destination).getName();
			// change the targetInteractionFlow in the mutant Window
			EObject mutantWindow = ((InteractionFlowModel)ifmlElement).getInteractionFlowModelElements().get(mutantIndex);
			Iterator<EObject> mutantWindowIt = mutantWindow.eAllContents();
			while (mutantWindowIt.hasNext()) {
				EObject child = mutantWindowIt.next();
				if (child instanceof OnSelectEvent) {
					String childName = ((NamedElement)child).getName();
					if (childName.contains(destinationName)) {
						((ViewElementEvent) child).getOutInteractionFlows().get(0).setTargetInteractionFlowElement(destination);
						// retrieve the ViewElement object for the navigation flow
						if (child.eContainer() instanceof ViewElement) {
							ve = (ViewElement) child.eContainer();
							vee = (ViewElementEvent) child;
							done = 1;
							break;
						}
					}
				}
			}
			
			// remove other View Elements.
			EList<EObject> removeThis = new BasicEList<EObject>();
			if (ve != null) {
				EList<EObject> extra = mutantWindow.eContents();
				for (EObject obj : extra) {
					if (!(obj.equals(ve))) {
						removeThis.add(obj);
					}
				}
				
				int removeSize = removeThis.size();
				for (int n=0; n<removeSize; n++) {
					EcoreUtil.delete(removeThis.get(n));
				}
			}
			
			// remove other View Element Event, i.e. buttons and links in the existing ViewElement
			EList<EObject> removeVee = new BasicEList<EObject>();
			if (vee != null) {
				EList<EObject> extraVee = ve.eContents();
				for (EObject obj : extraVee) {
					if (!(obj.equals(vee))) {
						removeVee.add(obj);
					}
				}
				
				int removeSize = removeVee.size();
				for (int n=0; n<removeSize; n++) {
					EcoreUtil.delete(removeVee.get(n));
				}
			}
		}
		
		return theOffspring;
	}
	
	/**
	 * Removes excessive outFlows if the mutant gene is the last gene in the offspring. <!-- begin-user-doc -->
	 * @param theOffspring the offspring
	 * @return the updated offspring
	 */
	public static EObject removeOutflows(EObject theOffspring, int thePrevWinIndex) {
		int mutantIndex = thePrevWinIndex+1;
		
		EObject mutantGene = ((IFMLModel)theOffspring).getInteractionFlowModel().getInteractionFlowModelElements().get(mutantIndex);
		((IFMLWindow)mutantGene).getViewElements().clear();
		
		return theOffspring;
	}
	
	private static boolean targetFlowIsCorrect(String target, String destination) {
		if (target.contains(destination) || target.compareToIgnoreCase(destination) == 0 || target.compareToIgnoreCase(destination) > 0) {
			return true;
		}
		else {
			return false;
		}
	}
}